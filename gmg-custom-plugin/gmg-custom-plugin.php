<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://goodmarketinggroup.com/
 * @since             1.0.0
 * @package           Gmg_Custom_Plugin
 *
 * @wordpress-plugin
 * Plugin Name:       GMG Custom Plugin
 * Plugin URI:        https://goodmarketinggroup.com/
 * Description:       Updated Custom Plugin created by Good Marketing Group. Includes a bunch of different elements that are part of our normal website.
 * Version:           1.2.0
 * Author:            Good Group LLC
 * Author URI:        https://goodmarketinggroup.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gmg-custom-plugin
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'GMG_CUSTOM_PLUGIN_VERSION', '1.2.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gmg-custom-plugin-activator.php
 */
function activate_gmg_custom_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gmg-custom-plugin-activator.php';
	Gmg_Custom_Plugin_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gmg-custom-plugin-deactivator.php
 */
function deactivate_gmg_custom_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gmg-custom-plugin-deactivator.php';
	Gmg_Custom_Plugin_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gmg_custom_plugin' );
register_deactivation_hook( __FILE__, 'deactivate_gmg_custom_plugin' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */

require plugin_dir_path( __FILE__ ) . 'includes/class-gmg-custom-plugin.php';

/**
*
* Function used by couple of modules to transfer files inside plugins
* to specific places.
*
*/

function recurseCopy($src, $dst){
    
//    error_log('Source: ' . $src );
//    error_log('Destination: ' . $dst );
    
    $dir = opendir($src);
    
    if( !file_exists( $dst ) ){
        
        mkdir( $dst );
        
    }
    
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
            if ( is_dir($src . '/' . $file) ) {
                recurseCopy($src . '/' . $file, $dst . '/' . $file);
            } else {
                copy($src . '/' . $file,$dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gmg_custom_plugin() {
    
    if( function_exists( 'get_field' ) ){
    
        require plugin_dir_path( __FILE__ ) . 'acf/gmg-custom-plugin-import-acf.php';
        //require plugin_dir_path( __FILE__ ) . 'parts/gmg-brands/gmg-brands.php';
        require plugin_dir_path( __FILE__ ) . 'parts/gmg-child-boxes/gmg-child-boxes.php';
        //require plugin_dir_path( __FILE__ ) . 'parts/gmg-cta/gmg-cta.php';
        require plugin_dir_path( __FILE__ ) . 'parts/gmg-customs/gmg-customs.php';
        require plugin_dir_path( __FILE__ ) . 'parts/gmg-gallery/gmg-gallery.php';
        require plugin_dir_path( __FILE__ ) . 'parts/gmg-headliner/gmg-headliner.php';
        require plugin_dir_path( __FILE__ ) . 'parts/gmg-mailer/gmg-mailer.php';
        //require plugin_dir_path( __FILE__ ) . 'parts/gmg-slider/gmg-slider.php';
        require plugin_dir_path( __FILE__ ) . 'parts/gmg-sticky/gmg-sticky.php';
        require plugin_dir_path( __FILE__ ) . 'parts/gmg-faqs/gmg-faqs.php';
        require plugin_dir_path( __FILE__ ) . 'parts/gmg-store-info/gmg-store-info.php';
        require plugin_dir_path( __FILE__ ) . 'parts/gmg-woocommerce/gmg-woocommerce.php';

        require plugin_dir_path( __FILE__ ) . 'parts/gmg-customers/gmg-customers.php';
        //require plugin_dir_path( __FILE__ ) . 'parts/gmg-capture/gmg-capture.php';
        require plugin_dir_path( __FILE__ ) . 'parts/gmg-reviews/gmg-reviews.php';

        $plugin = new Gmg_Custom_Plugin();
        $plugin->run();
        
    }

}
run_gmg_custom_plugin();
