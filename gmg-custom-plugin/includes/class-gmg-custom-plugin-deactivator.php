<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://goodmarketinggroup.com/
 * @since      1.0.0
 *
 * @package    Gmg_Custom_Plugin
 * @subpackage Gmg_Custom_Plugin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gmg_Custom_Plugin
 * @subpackage Gmg_Custom_Plugin/includes
 * @author     Good Group LLC <info@goodmarketinggroup.com>
 */
class Gmg_Custom_Plugin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
