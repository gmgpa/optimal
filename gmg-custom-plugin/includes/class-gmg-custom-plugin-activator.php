<?php

/**
 * Fired during plugin activation
 *
 * @link       https://goodmarketinggroup.com/
 * @since      1.0.0
 *
 * @package    Gmg_Custom_Plugin
 * @subpackage Gmg_Custom_Plugin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gmg_Custom_Plugin
 * @subpackage Gmg_Custom_Plugin/includes
 * @author     Good Group LLC <info@goodmarketinggroup.com>
 */
class Gmg_Custom_Plugin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
