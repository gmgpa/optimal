<?php

// Move all "advanced" metaboxes above the default editor
//add_action('edit_form_after_editor', 'gmg_cta_editor');
add_action('edit_form_after_editor', 'gmg_cta_meta_boxes_setup');

//function gmg_cta_editor() {
//	global $post, $wp_meta_boxes;
//	do_meta_boxes(get_current_screen(), 'advanced', $post);
//	unset($wp_meta_boxes[get_post_type($post)]['advanced']);
//}
//
///* Fire our meta box setup function on the recipients editor screen. */
//add_action( 'init', 'mgm_cta_meta_boxes_setup' );

function gmg_cta_meta_boxes_setup(){
    
    global $post;
    do_meta_boxes(get_current_screen(), 'advanced', $post);

	/* Add meta boxes on the 'add_meta_boxes' hook. */
	add_action( 'add_meta_boxes', 'gmg_cta_add_meta_boxes' );
	/* Save post meta on the 'save_post' hook. */
	add_action( 'save_post', 'gmg_save_cta_header', 10, 2);
	add_action( 'save_post', 'gmg_save_cta_quote_meta', 10, 2);
}


/* Create Recipients Custom Meta Boxes */
function gmg_cta_add_meta_boxes(){

	add_meta_box(
		'cta-top',      // Unique ID
		esc_html__( 'CTA Header', 'altitude-pro' ),    // Title
		'gmg_cta_header_box',   // Callback function
		'Page',         // Admin page (or post type)
		'advanced',         // Context
		'default'         // Priority
	);

	add_meta_box(
		'cta-quote',      // Unique ID
		esc_html__( 'CTA Quote', 'altitude-pro' ),    // Title
		'gmg_cta_quote_meta_box',   // Callback function
		'Page',         // Admin page (or post type)
		'advanced',         // Context
		'default'         // Priority
	);
}

/* Display the page recipient meta box. */
function gmg_cta_header_box( $object, $box ) { ?>

    <?php wp_nonce_field( basename( __FILE__ ), 'gmg_cta_header_nonce' ); ?>

    <p>
        <label for="cta-header"><?php _e( "CTA Header:", 'altitude-pro Pro' ); ?></label>
        <br />
        <input class="widefat" type="text" name="cta-header" id="cta-header" value="<?php echo esc_attr( get_post_meta( $object->ID, 'cta_header', true ) ); ?>" size="30" />
    </p>
    <?php }

/* Display the recipient name meta box. */
function gmg_cta_quote_meta_box( $object, $box ) { ?>

    <?php wp_nonce_field( basename( __FILE__ ), 'gmg_cta_quote_nonce' ); ?>

    <p>
        <label for="cta-quote"><?php _e( "CTA Quote:", 'altitude-pro Pro' ); ?></label>
        <br />
        <input class="widefat" type="text" name="cta-quote" id="cta-quote" value="<?php echo esc_attr( get_post_meta( $object->ID, 'cta_quote', true ) ); ?>" size="30" />
    </p>
    <?php }




/* Save the meta box's post metadata. */
function gmg_save_cta_header( $post_id, $post ) {

	/* Verify the nonce before proceeding. */
	if ( !isset( $_POST['gmg_cta_header_nonce'] ) || !wp_verify_nonce( $_POST['gmg_cta_header_nonce'], basename( __FILE__ ) ) )
		return $post_id;

	/* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );

	/* Check if the current user has permission to edit the post. */
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;

	/* Get the posted data and sanitize it for use as an HTML class. */
	$new_meta_value = ( isset( $_POST['cta-header'] ) ? sanitize_text_field( $_POST['cta-header'] ) : '' );

	/* Get the meta key. */
	$meta_key = 'cta_header';

	/* Get the meta value of the custom field key. */
	$meta_value = get_post_meta( $post_id, $meta_key, true );

	/* If a new meta value was added and there was no previous value, add it. */
	if ( $new_meta_value && '' == $meta_value )
		add_post_meta( $post_id, $meta_key, $new_meta_value, true );

	/* If the new meta value does not match the old value, update it. */
	elseif ( $new_meta_value && $new_meta_value != $meta_value )
		update_post_meta( $post_id, $meta_key, $new_meta_value );

	/* If there is no new meta value but an old value exists, delete it. */
	elseif ( '' == $new_meta_value && $meta_value )
		delete_post_meta( $post_id, $meta_key, $meta_value );
}


/* Save the meta box's post metadata. */
function gmg_save_cta_quote_meta( $post_id, $post ) {

	/* Verify the nonce before proceeding. */
	if ( !isset( $_POST['gmg_cta_quote_nonce'] ) || !wp_verify_nonce( $_POST['gmg_cta_quote_nonce'], basename( __FILE__ ) ) )
		return $post_id;

	/* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );

	/* Check if the current user has permission to edit the post. */
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;

	/* Get the posted data and sanitize it for use as an HTML class. */
	$new_meta_value = ( isset( $_POST['cta-quote'] ) ? sanitize_text_field( $_POST['cta-quote'] ) : '' );

	/* Get the meta key. */
	$meta_key = 'cta_quote';

	/* Get the meta value of the custom field key. */
	$meta_value = get_post_meta( $post_id, $meta_key, true );

	/* If a new meta value was added and there was no previous value, add it. */
	if ( $new_meta_value && '' == $meta_value )
		add_post_meta( $post_id, $meta_key, $new_meta_value, true );

	/* If the new meta value does not match the old value, update it. */
	elseif ( $new_meta_value && $new_meta_value != $meta_value )
		update_post_meta( $post_id, $meta_key, $new_meta_value );

	/* If there is no new meta value but an old value exists, delete it. */
	elseif ( '' == $new_meta_value && $meta_value )
		delete_post_meta( $post_id, $meta_key, $meta_value );
}
