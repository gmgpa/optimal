<?php

class GMG_CTA_Meta_Box {

	public function __construct() {

		if ( is_admin() ) {
			add_action( 'load-post.php',     array( $this, 'init_cta_metabox' ) );
			add_action( 'load-post-new.php', array( $this, 'init_cta_metabox' ) );
		}

	}
    
//    add_action( 'edit_form_after_editor', 'init_cta_metabox', 10, 1 );

	public function init_cta_metabox() {

//		add_action( 'add_meta_boxes', array( $this, 'gmg_cta_add_metabox'  )        );
        add_action( 'edit_form_after_editor', array( $this, 'gmg_cta_add_metabox'  ), 5, 1 );
		add_action( 'save_post',      array( $this, 'gmg_cta_save_metabox' ), 10, 2 );

	}

	public function gmg_cta_add_metabox() {
    
        	add_meta_box(
                'cta-top',      // Unique ID
                esc_html__( 'Call To Action Text', 'text_domain' ),    // Title
                array ($this, 'gmg_cta_render_metabox'),   // Callback function
                'Page',         // Admin page (or post type)
                'normal',         // Context
                'high'         // Priority
	);

	}

	public function gmg_cta_render_metabox( $post ) {

		// Add nonce for security and authentication.
		wp_nonce_field( 'cta_nonce_data', 'cta_gmg_nonce' );

		// Retrieve an existing value from the database.
		$cta_header = get_post_meta( $post->ID, 'cta_header', true );
		$cta_quote = get_post_meta( $post->ID, 'cta_quote', true );

		// Set default values.
		if( empty( $cta_header ) ) $cta_header = '';
		if( empty( $cta_quote ) ) $cta_quote = '';

		// Form fields.
		echo '<table class="form-table">';

		echo '	<tr>';
		echo '		<th><label for="cta_header" class="cta_header_label">' . __( 'CTA Header', 'text_domain' ) . '</label></th>';
		echo '		<td>';
		echo '			<input type="text" id="cta_header" name="cta_header" style="width: 99%;" class="cta_input_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $cta_header ) . '">';
		echo '		</td>';
		echo '	</tr>';

		echo '	<tr>';
		echo '		<th><label for="cta_quote" class="cta_quote_label">' . __( 'CTA Quote', 'text_domain' ) . '</label></th>';
		echo '		<td>';
		echo '			<input type="text" id="cta_quote" name="cta_quote" style="width: 99%;" class="cta_input_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $cta_quote ) . '">';
		echo '		</td>';
		echo '	</tr>';

		echo '</table>';

	}

	public function gmg_cta_save_metabox( $post_id, $post ) {

		// Add nonce for security and authentication.
		$cta_nonce_name   = $_POST['cta_gmg_nonce'];
		$cta_nonce_action = 'cta_nonce_data';

		// Check if a nonce is set.
		if ( ! isset( $cta_nonce_name ) )
			return;

		// Check if a nonce is valid.
		if ( ! wp_verify_nonce( $cta_nonce_name, $cta_nonce_action ) )
			return;

		// Check if the user has permissions to save data.
		if ( ! current_user_can( 'edit_post', $post_id ) )
			return;

		// Check if it's not an autosave.
		if ( wp_is_post_autosave( $post_id ) )
			return;

		// Check if it's not a revision.
		if ( wp_is_post_revision( $post_id ) )
			return;

		// Sanitize user input.
		$cta_new_header = isset( $_POST[ 'cta_header' ] ) ? sanitize_text_field( $_POST[ 'cta_header' ] ) : '';
		$cta_new_quote = isset( $_POST[ 'cta_quote' ] ) ? sanitize_text_field( $_POST[ 'cta_quote' ] ) : '';

		// Update the meta field in the database.
		update_post_meta( $post_id, 'cta_header', $cta_new_header );
		update_post_meta( $post_id, 'cta_quote', $cta_new_quote );

	}

}

new GMG_CTA_Meta_Box;
