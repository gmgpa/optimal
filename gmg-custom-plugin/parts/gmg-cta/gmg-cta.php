<?php

/**
 * GMG CTA
 *
 * @package           gmg-cta
 * @author            Good Marketing Group
 * @license           GPL-2.0+
 * @link              http://goodgroupllc.com/
 * @copyright         2017 Good Marketing Group
 *
 * @wordpress-plugin
 * Plugin Name:       GMG CTA
 * Plugin URI:        http://goodgroupllc.com/
 * Description:       Creates a custom 'call to action' box that is inserted after post and page entries.
 * Version:           1.0.2
 * Author:            Good Marketing Group
 * Author URI:        http://goodgroupllc.com/
 * Text Domain:       gmg-cta
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

$cta = plugin_dir_path( __FILE__ );

require_once($cta . 'includes/gmg-cta-meta.php');


add_action( 'genesis_after_entry', 'gmg_cta') ;
             
function gmg_cta(){
    
    if (is_page()){
    
//    '<div>Howdy. There is more now!</div>';
    
        $theID = get_queried_object_id();

        echo '<div id="CTA-Text-Box" >';
        echo '<h2>';

        if( get_post_meta( $theID, 'cta_header', true )) {
            echo get_post_meta( $theID, 'cta_header', true ) . '</h2>';
        } else {
            echo 'Call or Email us.</h2>' ;
        }
        
        echo '<div class="icon-boxes">';

        echo '<div class="icon-box">';
        if( get_post_meta( $theID, 'cta_url', true ) ) {            
            echo '<a href="' . get_post_meta( $theID, 'cta_url', true ) . '"><i class="fa fa-pencil-square-o fa-5x"></i></a>';
        } else {
            echo '<a href="' . site_url('contact-us') . '"><i class="fa fa-pencil-square-o fa-5x"></i></a>';
        }
        echo '</div>';
        
        $stores = new Stores();
        $stores_array = $stores->get_all_stores();
        
        foreach( $stores_array as $stores ){
            
            $store = new Store( $stores[1] );
            $phone = $store->get_store_phone();
            echo '<div class="icon-box">';
            echo '<a href="tel:+1' . $store->get_store_phone() . '"><i class="fa fa-phone fa-5x" aria-hidden="true"></i></a>';
            echo '<h4>' . $store->get_store_state() . '</h4>';
            echo '</div>';
        }
        echo '</div>';
        
        if( get_post_meta( $theID, 'cta_quote', true )) {
            echo '<h3>' . get_post_meta( $theID, 'cta_quote', true ) . '</h3>' ;
        } else{
            echo '<h3>' . get_bloginfo( 'description' ) . '</h3>' ;
        }
    
        echo '</div>';
}
}

function gmg_get_store_ID(){
    
    // WP_Query arguments
    $args = array(
        'post_type'              => array( 'Stores' ),
        'posts_per_page'         => -1,
    );

    // The Query
    $query = new WP_Query( $args );

    // The Loop
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post();
            $theID = get_the_ID();

        endwhile;
    } else {
        // no posts found
    }

    // Restore original Post Data
    wp_reset_postdata();
    
    return $theID;
}