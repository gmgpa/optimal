<?php

function gmg_start_sticky_message_plugin(){

$sm = plugin_dir_path( __FILE__ );
    
//First, let's create the Sticky custom post type.
require_once($sm . '/includes/gmg-sticky-cpt.php');

//First, let's make sure the Sticky Header Page is created.
//require_once($sm . '/includes/gmg-sticky-page-adder.php');

//Let's also bring in the meta boxes and all that jazz.
require_once($sm . '/includes/gmg-sticky-meta.php');

//Let's also bring in the codes to show the message.
require_once($sm . '/includes/gmg-sticky-message-show.php');

//Let's also bring in the dashboard widget
//require_once($sm . '/includes/gmg-store-info-dashboard.php');
    
}

function gmg_sticky_style() {
//    error_log('Put up some style!');
    $plugin_url = plugin_dir_url( __FILE__ );
    
    wp_register_style( 'sticky', $plugin_url . '/css/gmg-sticky-style.css', false );
    wp_enqueue_style( 'sticky' );
}

add_action('wp_enqueue_scripts', 'gmg_sticky_style');

gmg_start_sticky_message_plugin();