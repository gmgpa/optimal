<?php

add_action( 'genesis_before', 'gmg_custom_sticky_message', 1);
function gmg_custom_sticky_message(){
    
    $stickys = new Stickys();
    $sticky_array = $stickys->get_ready_stickys();
    
    if( count( $sticky_array ) > 1 ){
        
        $stickys->show_stickys_stacked( $sticky_array );
        
    } elseif( count( $sticky_array ) == 1 ){
        
        $sticky = new Sticky( $sticky_array[0] );
        $sticky->show_sticky( '0');
    } else{
        
        //No stickies
    }
}
