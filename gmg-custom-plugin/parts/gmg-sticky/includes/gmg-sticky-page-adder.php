<?php

//Let's check to see if we've created a page called 'Sticky Header'

add_action( 'wp_loaded', 'gmg_service_check_if_sticky_page_exists' );
function gmg_service_check_if_sticky_page_exists(){
    
    if( !get_page_by_path('sticky-message/', OBJECT, 'page') ){
        
        $user_id = get_current_user_id();
        
        $args = array(
            'post_type'     => 'page',
            'post_title'    => 'Sticky Message',
            'post_name'     => 'sticky-message',
            'post_content'      => '',
            'post_status'        => 'publish',
            'post_author'       => $user_id,   
            );

            wp_insert_post( $args );        

        }
         
    }