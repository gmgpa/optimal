<?php

//Add Custom Post Type For Stickys
function stickys_init() {
	$rev_labels = array(
        'name'               => _x( 'Stickies', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Sticky', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Stickies', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Sticky', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'slide', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Sticky', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Sticky', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Sticky', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Sticky', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Stickies', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Stickies', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Stickies:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No slides found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No slides found in Trash.', 'your-plugin-textdomain' )
	);
    
	$rev_args = array(
		'label'                 => __( 'Sticky', 'gmg-stickys' ),
		'description'           => __( 'Custom Post Type for Stickys', 'gmg-stickys' ),
		'labels'                => $rev_labels,
		'supports'              => array( 'title' ),
//		'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => 'gmg-custom-plugin',
		'menu_position'         => 6,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'gmg-stickys', $rev_args );

}
add_action( 'init', 'stickys_init' );


// add order column to admin table list of posts
function gmg_stickys_add_new_post_column($cpt_columns) {
	$cpt_columns['menu_order'] = "Order";
	return $cpt_columns;
}
add_action('manage_stickys_posts_columns', 'gmg_stickys_add_new_post_column');

//Show custom order column values for Stickys
function gmg_stickys_posts_show_order_column($name){
	global $post;

	switch ($name) {
		case 'menu_order':
			$order = $post->menu_order;
			echo $order;
			break;
		default:
			break;
	}
}
add_action('manage_stickys_posts_custom_column','gmg_stickys_posts_show_order_column');


//Make Column Sortable for Stickys
function gmg_stickys_order_column_register_sortable($columns){
	$columns['menu_order'] = 'menu_order';
	return $columns;
}
add_filter('manage_edit-Stickys_sortable_columns','gmg_stickys_order_column_register_sortable');
