<?php

$sti = plugin_dir_path( __FILE__ );

function admin_style() {
    
    $plugin_url = plugin_dir_url( __FILE__ );
//    wp_register_style( 'store-info-custom', $plugin_url . 'gmg-admin.css' );
    wp_enqueue_style('si-custom-css', $plugin_url . '/includes/gmg-admin.css' );
}
add_action('admin_enqueue_scripts', 'admin_style');

//Let's bring in the custom post type Stores, which is where we will store (no pun intented) the store info.
require_once($sti . '/includes/gmg-store-cpt.php');

//Let's also bring in the meta boxes and all that jazz.
require_once($sti . '/includes/gmg-store-info-meta.php');

//Let's also bring in the codes of short (shortcodes).
require_once($sti . '/includes/gmg-store-shortcode.php');

//Let's bring in the class Days, which, at this point, controls Store Hours.
require_once($sti . '/includes/gmg-class-days.php');

//Let's bring in the class Days.
require_once($sti . '/includes/gmg-class-store.php');

//Let's also bring in the dashboard widget
//require_once($sti . '/includes/gmg-store-info-dashboard.php');
