<?php

class GMG_Store_Info_Meta_Box {

	public function __construct() {

		if ( is_admin() ) {
			add_action( 'load-post.php',     array( $this, 'init_si_metabox' ) );
			add_action( 'load-post-new.php', array( $this, 'init_si_metabox' ) );
		}

	}

	public function init_si_metabox() {

		add_action( 'add_meta_boxes', array( $this, 'gmg_store_info_add_metabox'  )        );
		add_action( 'save_post',      array( $this, 'gmg_store_info_save_metabox' ), 10, 2 );

	}

	public function gmg_store_info_add_metabox() {
    
        	add_meta_box(
                'store-info-meta',      // Unique ID
                esc_html__( 'Store Info', 'text_domain' ),    // Title
                array ($this, 'gmg_store_info_render_metabox'),   // Callback function
                'Stores',         // Admin page (or post type)
                'advanced',         // Context
                'default'         // Priority
	);

	}

	public function gmg_store_info_render_metabox( $post ) {

		// Add nonce for security and authentication.
		wp_nonce_field( 'store_info_data', 'store_info_nonce' );

		// Retrieve an existing value from the database.
        $store_info_address_location = get_post_meta( $post->ID, 'store_info_address_location', true );
		$store_info_address_top = get_post_meta( $post->ID, 'store_info_address_top', true );
        $store_info_address_bottom = get_post_meta( $post->ID, 'store_info_address_bottom', true );
        $store_info_address_state = get_post_meta( $post->ID, 'store_info_address_state', true );
        $store_info_address_url = get_post_meta( $post->ID, 'store_info_address_url', true );
		$store_info_phone = get_post_meta( $post->ID, 'store_info_phone', true );
        $store_info_email = get_post_meta( $post->ID, 'store_info_email', true );
        
//		$store_info_sun_status = get_post_meta( $post->ID, 'store_info_sun_status', true );
        
        $store_info_sun_check = get_post_meta( $post->ID, 'store_info_sun_check', true );        
        $store_info_mon_check = get_post_meta( $post->ID, 'store_info_mon_check', true );
        $store_info_tues_check = get_post_meta( $post->ID, 'store_info_tues_check', true );
        $store_info_wed_check = get_post_meta( $post->ID, 'store_info_wed_check', true );
        $store_info_thurs_check = get_post_meta( $post->ID, 'store_info_thurs_check', true );
        $store_info_fri_check = get_post_meta( $post->ID, 'store_info_fri_check', true );
        $store_info_sat_check = get_post_meta( $post->ID, 'store_info_sat_check', true );
        
//        $store_info_sun_check = '';       
//        $store_info_mon_check = '';
//        $store_info_tues_check = '';
//        $store_info_wed_check = '';
//        $store_info_thurs_check = '';
//        $store_info_fri_check = '';
//        $store_info_sat_check = '';
        
        $store_info_sun_open = get_post_meta( $post->ID, 'store_info_sun_open', true );
        $store_info_mon_open = get_post_meta( $post->ID, 'store_info_mon_open', true );
        $store_info_tues_open = get_post_meta( $post->ID, 'store_info_tues_open', true );
        $store_info_wed_open = get_post_meta( $post->ID, 'store_info_wed_open', true );
        $store_info_thurs_open = get_post_meta( $post->ID, 'store_info_thurs_open', true );
        $store_info_fri_open = get_post_meta( $post->ID, 'store_info_fri_open', true );
        $store_info_sat_open = get_post_meta( $post->ID, 'store_info_sat_open', true );
        
        $store_info_sun_close = get_post_meta( $post->ID, 'store_info_sun_close', true );
        $store_info_mon_close = get_post_meta( $post->ID, 'store_info_mon_close', true );
        $store_info_tues_close = get_post_meta( $post->ID, 'store_info_tues_close', true );
        $store_info_wed_close = get_post_meta( $post->ID, 'store_info_wed_close', true );
        $store_info_thurs_close = get_post_meta( $post->ID, 'store_info_thurs_close', true );
        $store_info_fri_close = get_post_meta( $post->ID, 'store_info_fri_close', true );
        $store_info_sat_close = get_post_meta( $post->ID, 'store_info_sat_close', true );
        
        $store_info_hours_extra = get_post_meta( $post->ID, 'store_info_hours_extra', true );  
        

		// Set default values.
        if( empty( $store_info_address_location ) ) $store_info_address_location = '';
		if( empty( $store_info_address_top ) ) $store_info_address_top = '';
        if( empty( $store_info_address_bottom ) ) $store_info_address_bottom = '';
        if( empty( $store_info_address_state ) ) $store_info_address_state = '';
        if( empty( $store_info_address_url ) ) $store_info_address_url = '';
		if( empty( $store_info_phone ) ) $store_info_phone = '';
        if( empty( $store_info_email ) ) $store_info_email = '';
        
        if( empty( $store_info_sun_check ) ) $store_info_sun_check = '';        
        if( empty( $store_info_mon_check ) ) $store_info_mon_check = '';
        if( empty( $store_info_tues_check ) ) $store_info_tues_check = '';
        if( empty( $store_info_wed_check ) ) $store_info_wed_check = '';
        if( empty( $store_info_thurs_check ) ) $store_info_thurs_check = '';
        if( empty( $store_info_fri_check ) ) $store_info_fri_check = '';
        if( empty( $store_info_sat_check ) ) $store_info_sat_check = '';
        
        if( empty( $store_info_sun_open ) ) $store_info_sun_open = '';        
        if( empty( $store_info_mon_open ) ) $store_info_mon_open = '';
        if( empty( $store_info_tues_open ) ) $store_info_tues_open = '';
        if( empty( $store_info_wed_open ) ) $store_info_wed_open = '';
        if( empty( $store_info_thurs_open ) ) $store_info_thurs_open = '';
        if( empty( $store_info_fri_open ) ) $store_info_fri_open = '';
        if( empty( $store_info_sat_open ) ) $store_info_sat_open = '';
        
        if( empty( $store_info_sun_close ) ) $store_info_sun_close = '';
        if( empty( $store_info_mon_close ) ) $store_info_mon_close = '';
        if( empty( $store_info_tues_close ) ) $store_info_tues_close = '';
        if( empty( $store_info_wed_close ) ) $store_info_wed_close = '';
        if( empty( $store_info_thurs_close ) ) $store_info_thurs_close = '';
        if( empty( $store_info_fri_close ) ) $store_info_fri_close = '';
        if( empty( $store_info_sat_close ) ) $store_info_sat_close = '';
        
        if( empty( $store_info_hours_extra ) ) $store_info_hours_extra = '';

//    Form fields
        
//    $store_info_meta = [];
//    
//    array_push($store_info,
               
               echo '<table class="form-table">';
        
        //Address
        echo '<tr>';
        echo '<td<label for="store_info_address_location" class="store_info_address_location_label">' . __( 'Store Location', 'text_domain' ) . '<input type="text" id="store_info_address_location" name="store_info_address_location" class="store_info_address_location_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr( $store_info_address_location ) . '"></label></td>';     
        echo '</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<th><label for="store_info_address_top" class="store_info_address_top_label">' . __( 'Address Line 1', 'text_domain' ) . '</label></th>';     
        echo '<td>';
        echo '<input type="text" id="store_info_address_top" name="store_info_address_top" class="store_info_address_top_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr( $store_info_address_top ) . '">';
        echo '</td>';
         echo '</tr>';
         echo '<tr>';
        echo '<th><label for="store_info_address_bottom" class="store_info_address_bottom_label">' . __( 'Address Line 2', 'text_domain' ) . '</label></th>';   
        echo '<td>';
        echo '<input type="text" id="store_info_address_bottom" name="store_info_address_bottom" class="store_info_address_bottom_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr( $store_info_address_bottom ) . '">';
        echo '</td>';
        echo '</tr>';
        
        echo '<tr>';
        echo '<th><label for="store_info_address_state" class="store_info_address_state_label">' . __( 'State', 'text_domain' ) . '</label></th>';   
        echo '<td>';
        echo '<input type="text" id="store_info_address_state" name="store_info_address_state" class="store_info_address_state_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr( $store_info_address_state ) . '">';
        echo '</td>';
        echo '</tr>';
        
        echo '<tr>';
        echo '<td>';
        echo '<a href="' . esc_attr__( $store_info_address_url ) . '" target="_blank" >Google Directions URL</a>';
        echo '</td>';
        echo '</tr>';
        
//        Telephone        
        echo '<tr>';
        echo '<th><label for="store_info_phone" class="store_info_phone_label">' . __( 'Phone', 'text_domain' ) . '</label></th>';
        echo '<td>';
        echo '<input type="tel" id="store_info_phone" name="store_info_phone" class="store_info_phone_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_phone ) . '">';
        echo '</td>';
        echo '</tr>';
        
//      Email        
        echo '<tr>';
        echo '<th><label for="store_info_email" class="store_info_email_label">' . __( 'Email (General email)', 'text_domain' ) . '</label></th>';
        echo '<td>';
        echo '<input type="email" id="store_info_email" name="store_info_email" class="store_info_email_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_email ) . '">';
        echo '</td>';
        echo '</tr>';
        
//     Intro
        echo '<tr>';
        echo '<td><h2>Store Hours</h2></td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td><p>If you are closed, just leave empty.</p></td>';
//        echo '<td><h2>Store Hours</h2></td>';
        echo '</tr>';
        
//      Sunday   
        echo '<tr>';
        echo '<th><label class="">' . __( 'Sunday:', 'text_domain' ) . '</label></th><td></td>';
        
        echo '<td>';
        echo '<label for="store_info_sun_open" class="">' . __( 'Open', 'text_domain' ) . '</label>';
        echo '<input type="time" id="store_info_sun_open" name="store_info_sun_open" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_sun_open ) . '">';
//        $store_info_sun_open= new DateTime ( get_post_meta( $post->ID, 'store_info_sun_open', true ));
//        echo $store_info_sun_open->format('g:i A');
        echo '</td>';
        
        echo '<td>';
        echo '<label for="store_info_sun_close" class="">' . __( 'Close', 'text_domain' ) . '</label>';
        echo '<input type="time" id="store_info_sun_close" name="store_info_sun_close" class=" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_sun_close ) . '">';
//        $store_info_sun_close = new DateTime ( get_post_meta( $post->ID, 'store_info_sun_close', true ) );
//        echo $store_info_sun_close->format('g:i A');
        echo '</td>';    
        echo '</tr>';
        
//      Monday        
        echo '<tr>';
        echo '<th><label>' . __( 'Monday', 'text_domain' ) . '</label></th>';
        
        echo '<td>';
        echo '<label for="store_info_mon_check"  class=""> Same as above? </label>';
        echo '<input type="checkbox" id="store_info_mon_check"  name="store_info_mon_check"  ' . esc_attr__( $store_info_mon_check ) . ' value="Same">';
        echo '</td>';
        
        echo '<td>';
        echo '<label for="store_info_mon_open" class="">' . __( 'Open', 'text_domain' ) . '</label>';
        echo '<input type="time" id="store_info_mon_open" name="store_info_mon_open" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_mon_open ) . '">';
        echo '</td>';
        
        echo '<td>';
        echo '<label for="store_info_mon_close" class="">' . __( 'Close', 'text_domain' ) . '</label>';
        echo '<input type="time" id="store_info_mon_close" name="store_info_mon_close" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_mon_close ) . '">';
        echo '</td>';    
        
        echo '</tr>';
        
//      Tuesday
        echo '<tr>';
        echo '<th><label>' . __( 'Tuesday', 'text_domain' ) . '</label></th>';
        
        echo '<td>';
        echo '<label for="store_info_tues_check"  class=""> Same as above? </label>';
        echo '<input type="checkbox" id="store_info_tues_check"  name="store_info_tues_check" ' . esc_attr__( $store_info_tues_check ) .  ' value="Same">';
        echo '</td>';
        
        echo '<td>';
        echo '<label for="store_info_tues_open" class="">' . __( 'Open', 'text_domain' ) . '</label>';
        echo '<input type="time" id="store_info_tues_open" name="store_info_tues_open" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_tues_open ) . '">';
        echo '</td>';
        
        
        echo '<td>';
        echo '<label for="store_info_tues_close" class="">' . __( 'Close', 'text_domain' ) . '</label>';
        echo '<input type="time" id="store_info_tues_close" name="store_info_tues_close" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_tues_close ) . '">';
        echo '</td>';
        
        echo '</tr>';
        
//      Wednesday        
        echo '<tr>';
        echo '<th><label>' . __( 'Wednesday', 'text_domain' ) . '</label></th>';
        
        echo '<td>';
        echo '<label for="store_info_wed_check"  class=""> Same as above? </label>';
        echo '<input type="checkbox" id="store_info_wed_check"  name="store_info_wed_check" ' . esc_attr__( $store_info_wed_check ) .  ' value="Same">';
        echo '</td>';
        
        echo '<td>';
        echo '<label for="store_info_wed_open" class="">' . __( 'Open', 'text_domain' ) . '</label>';
        echo '<input type="time" id="store_info_wed_open" name="store_info_wed_open" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_wed_open ) . '">';
        echo '</td>';
        
        echo '<td>';
        echo '<label for="store_info_wed_close" class="">' . __( 'Close', 'text_domain' ) . '</label>';
        echo '<input type="time" id="store_info_wed_close" name="store_info_wed_close" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_wed_close ) . '">';
        echo '</td>';
        
        echo '</tr>';
        
//      Thursday        
        echo '<tr>';
        echo '<th><label>' . __( 'Thursday', 'text_domain' ) . '</label></th>';
        
        echo '<td>';
        echo '<label for="store_info_thurs_check"  class=""> Same as above? </label>';
        echo '<input type="checkbox" id="store_info_thurs_check"  name="store_info_thurs_check" ' . esc_attr__( $store_info_thurs_check ) .  ' value="Same">';
        echo '</td>';
        
        echo '<td>';
        echo '<label for="store_info_thurs_open" class="">' . __( 'Open', 'text_domain' ) . '</label>';
        echo '<input type="time" id="store_info_thurs_open" name="store_info_thurs_open" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_thurs_open ) . '">';
        echo '</td>';
        
        echo '<td>';
        echo '<label for="store_info_thurs_close" class="">' . __( 'Close', 'text_domain' ) . '</label>';
        echo '<input type="time" id="store_info_thurs_close" name="store_info_thurs_close" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_thurs_close ) . '">';
        echo '</td>';
        
        echo '</tr>';
        
//      Friday        
        echo '<tr>';
        echo '<th><label>' . __( 'Friday', 'text_domain' ) . '</label></th>';
        
        echo '<td>';
        echo '<label for="store_info_fri_check"  class=""> Same as above? </label>';
        echo '<input type="checkbox" id="store_info_fri_check"  name="store_info_fri_check" ' . esc_attr__( $store_info_fri_check ) .  ' value="Same">';
        echo '</td>';
        
        echo '<td>';
        echo '<label for="store_info_fri_open" class="">' . __( 'Open', 'text_domain' ) . '</label>';
        echo '<input type="time" id="store_info_fri_open" name="store_info_fri_open" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_fri_open ) . '">';
        echo '</td>';
        
        echo '<td>';
        echo '<label for="store_info_fri_close" class="">' . __( 'Close', 'text_domain' ) . '</label>';
        echo '<input type="time" id="store_info_fri_close" name="store_info_fri_close" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_fri_close ) . '">';
        echo '</td>';
        
        echo '</tr>';
        
//      Saturday        
        echo '<tr>';
        echo '<th><label>' . __( 'Saturday', 'text_domain' ) . '</label></th>';
        
        echo '<td>';
        echo '<label for="store_info_sat_check"  class=""> Same as above? </label>';
        echo '<input type="checkbox" id="store_info_sat_check"  name="store_info_sat_check" ' . esc_attr__( $store_info_sat_check ) .  ' value="Same">';
        echo '</td>';
        
        echo '<td>';
        echo '<label for="store_info_sat_open" class="">' . __( 'Open', 'text_domain' ) . '</label>';
        echo '<input type="time" id="store_info_sat_open" name="store_info_sat_open" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_sat_open ) . '">';
        echo '</td>';
        
        echo '<td>';
        echo '<label for="store_info_sat_close" class="">' . __( 'Close', 'text_domain' ) . '</label>';
        echo '<input type="time" id="store_info_sat_close" name="store_info_sat_close" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_sat_close ) . '">';
        echo '</td>';
        
        echo '</tr>';
        
        echo '<tr>';        
        echo '<th><label for="store_info_hours_extra" class="">' . __( 'Any hours-related text?', 'text_domain' ) . '</label></th>';
        echo '<td>';        
        echo '<input type="text" id="store_info_hours_extra" name="store_info_hours_extra" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_hours_extra ) . '">';
        echo '</td>';    
        echo '</tr>';
        
        echo '</table>';
               
//        return implode( $store_info_meta );

	}

	public function gmg_store_info_save_metabox( $post_id, $post ) {
        
        if( get_post_type( $post_id ) == 'stores'){

            // Add nonce for security and authentication.
            $si_nonce_name   = $_POST['store_info_nonce'];
            $si_nonce_action = 'store_info_data';

            // Check if a nonce is set.
            if ( ! isset( $si_nonce_name ) )
                return;

            // Check if a nonce is valid.
            if ( ! wp_verify_nonce( $si_nonce_name, $si_nonce_action ) )
                return;

            // Check if the user has permissions to save data.
            if ( ! current_user_can( 'edit_post', $post_id ) )
                return;

            // Check if it's not an autosave.
            if ( wp_is_post_autosave( $post_id ) )
                return;

            // Check if it's not a revision.
            if ( wp_is_post_revision( $post_id ) )
                return;

            // Sanitize user input.
    //        $store_info_new_address = isset( $_POST[ 'store_info_address' ] ) ? sanitize_text_field( $_POST[ 'store_info_address' ] ) : '';
    //        $store_info_new_address = isset( $_POST[ 'store_info_address' ] ) ? wp_kses_post( $_POST[ 'store_info_address' ] ) : '';
            $store_info_new_address_location = isset( $_POST[ 'store_info_address_location' ] ) ? sanitize_text_field( $_POST[ 'store_info_address_location' ] ) : '';
            $store_info_new_address_top = isset( $_POST[ 'store_info_address_top' ] ) ? sanitize_text_field( $_POST[ 'store_info_address_top' ] ) : '';
            $store_info_new_address_bottom = isset( $_POST[ 'store_info_address_bottom' ] ) ? sanitize_text_field( $_POST[ 'store_info_address_bottom' ] ) : '';
            $store_info_new_address_state = isset( $_POST[ 'store_info_address_state' ] ) ? sanitize_text_field( $_POST[ 'store_info_address_state' ] ) : '';
    //		$store_info_new_phone = isset( $_POST[ 'store_info_phone' ] ) ? sanitize_text_field( $_POST[ 'store_info_phone' ] ) : '';
            $store_info_new_email = isset( $_POST[ 'store_info_email' ] ) ? sanitize_text_field( $_POST[ 'store_info_email' ] ) : '';

            if( empty( $_POST[ 'store_info_sun_check' ] ) ) {
                $new_store_info_sun_check = '';
            } else {
                $new_store_info_sun_check = 'checked';
            }

            if( empty( $_POST[ 'store_info_mon_check' ] ) ) {
                $new_store_info_mon_check = '';         
            } else {             
                $new_store_info_mon_check = 'checked';
            }

            if( empty( $_POST[ 'store_info_tues_check' ] ) ) {
                $new_store_info_tues_check = '';         
            } else {             
                $new_store_info_tues_check = 'checked';
            }

            if( empty( $_POST[ 'store_info_wed_check' ] ) ) {
                $new_store_info_wed_check = '';         
            } else {             
                $new_store_info_wed_check = 'checked';
            }

            if( empty( $_POST[ 'store_info_thurs_check' ] ) ) {
                $new_store_info_thurs_check = '';         
            } else {             
                $new_store_info_thurs_check = 'checked';
            }

            if( empty( $_POST[ 'store_info_fri_check' ] ) ) {
                $new_store_info_fri_check = '';         
            } else {             
                $new_store_info_fri_check = 'checked';
            }

            if( empty( $_POST[ 'store_info_sat_check' ] ) ) {
                $new_store_info_sat_check = '';         
            } else {             
                $new_store_info_sat_check = 'checked';
            }

            $store_info_new_sun_open = isset( $_POST[ 'store_info_sun_open' ] ) ? sanitize_text_field( $_POST['store_info_sun_open' ] ) : '';        
            $store_info_new_mon_open = isset( $_POST[ 'store_info_mon_open' ] ) ? sanitize_text_field( $_POST['store_info_mon_open' ] ) : '';
            $store_info_new_tues_open = isset( $_POST[ 'store_info_tues_open' ] ) ? sanitize_text_field( $_POST[ 'store_info_tues_open' ] ) : '';
            $store_info_new_wed_open = isset( $_POST[ 'store_info_wed_open' ] ) ? sanitize_text_field( $_POST['store_info_wed_open' ] ) : '';
            $store_info_new_thurs_open = isset( $_POST[ 'store_info_thurs_open' ] ) ? sanitize_text_field( $_POST['store_info_thurs_open' ] ) : '';
            $store_info_new_fri_open = isset( $_POST[ 'store_info_fri_open' ] ) ? sanitize_text_field( $_POST['store_info_fri_open' ] ) : '';
            $store_info_new_sat_open = isset( $_POST[ 'store_info_sat_open' ] ) ? sanitize_text_field( $_POST['store_info_sat_open' ] ) : '';

            $store_info_new_sun_close = isset( $_POST[ 'store_info_sun_close' ] ) ? sanitize_text_field( $_POST[ 'store_info_sun_close'  ] ) : '';
            $store_info_new_mon_close = isset( $_POST[ 'store_info_mon_close' ] ) ? sanitize_text_field( $_POST[ 'store_info_mon_close'  ] ) : '';
            $store_info_new_tues_close = isset( $_POST[ 'store_info_tues_close' ] ) ? sanitize_text_field( $_POST[ 'store_info_tues_close'  ] ) : '';
            $store_info_new_wed_close = isset( $_POST[ 'store_info_wed_close' ] ) ? sanitize_text_field( $_POST[ 'store_info_wed_close'  ] ) : '';
            $store_info_new_thurs_close = isset( $_POST[ 'store_info_thurs_close' ] ) ? sanitize_text_field( $_POST[ 'store_info_thurs_close'  ] ) : '';
            $store_info_new_fri_close = isset( $_POST[ 'store_info_fri_close' ] ) ? sanitize_text_field( $_POST[ 'store_info_fri_close'  ] ) : '';
            $store_info_new_sat_close = isset( $_POST[ 'store_info_sat_close' ] ) ? sanitize_text_field( $_POST[ 'store_info_sat_close'  ] ) : '';

            $store_info_new_extra = isset( $_POST[ 'store_info_hours_extra' ] ) ? sanitize_text_field( $_POST[ 'store_info_hours_extra' ] ) : '';

            // Update the meta field in the database.
            update_post_meta( $post_id, 'store_info_address_location', $store_info_new_address_location );
            update_post_meta( $post_id, 'store_info_address_top', $store_info_new_address_top );
            update_post_meta( $post_id, 'store_info_address_bottom', $store_info_new_address_bottom );
            update_post_meta( $post_id, 'store_info_address_state', $store_info_new_address_state );
    //        update_post_meta( $post_id, 'store_info_phone', $store_info_new_phone );
            update_post_meta( $post_id, 'store_info_email', $store_info_new_email );

            update_post_meta( $post_id, 'store_info_sun_check', $new_store_info_sun_check );
            update_post_meta( $post_id, 'store_info_mon_check', $new_store_info_mon_check );
            update_post_meta( $post_id, 'store_info_tues_check', $new_store_info_tues_check );
            update_post_meta( $post_id, 'store_info_wed_check', $new_store_info_wed_check );
            update_post_meta( $post_id, 'store_info_thurs_check', $new_store_info_thurs_check );
            update_post_meta( $post_id, 'store_info_fri_check', $new_store_info_fri_check );
            update_post_meta( $post_id, 'store_info_sat_check', $new_store_info_sat_check );

            update_post_meta( $post_id, 'store_info_sun_open', $store_info_new_sun_open );
            update_post_meta( $post_id, 'store_info_mon_open', $store_info_new_mon_open );
            update_post_meta( $post_id, 'store_info_tues_open', $store_info_new_tues_open );
            update_post_meta( $post_id, 'store_info_wed_open', $store_info_new_wed_open );
            update_post_meta( $post_id, 'store_info_thurs_open', $store_info_new_thurs_open );
            update_post_meta( $post_id, 'store_info_fri_open', $store_info_new_fri_open );
            update_post_meta( $post_id, 'store_info_sat_open', $store_info_new_sat_open );

            update_post_meta( $post_id, 'store_info_sun_close' , $store_info_new_sun_close );
            update_post_meta( $post_id, 'store_info_mon_close' , $store_info_new_mon_close );
            update_post_meta( $post_id, 'store_info_tues_close' , $store_info_new_tues_close );
            update_post_meta( $post_id, 'store_info_wed_close' , $store_info_new_wed_close );
            update_post_meta( $post_id, 'store_info_thurs_close' , $store_info_new_thurs_close );
            update_post_meta( $post_id, 'store_info_fri_close' , $store_info_new_fri_close );
            update_post_meta( $post_id, 'store_info_sat_close' , $store_info_new_sat_close );

            update_post_meta( $post_id, 'store_info_hours_extra' , $store_info_new_extra );

            $store_info_new_phone = isset( $_POST[ 'store_info_phone' ] ) ? sanitize_text_field( $_POST[ 'store_info_phone' ] ) : '';

            $phone_numbs = preg_replace("/[^0-9]/", "", $store_info_new_phone);
            if ( strlen($phone_numbs) == 10 ){
                preg_match( "/([0-9]{3})([0-9]{3})([0-9]{4})/", $phone_numbs, $phone_pieces );
                $store_info_new_phone = $phone_pieces[1] . '-' . $phone_pieces[2] . '-' . $phone_pieces[3];

            } else {
                preg_match( "/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", $phone_numbs, $phone_pieces );
                $store_info_new_phone = $phone_pieces[1] . '-' . $phone_pieces[2] . '-' . $phone_pieces[3] . '-' . $phone_pieces[4];
            }
    //        
    //        
    //                
    //
    //        
    //
    //        if(preg_match("/[-_.]/", $store_info_new_phone, $output_array)){
    //            $phone_pieces = preg_split( "/[-_.]/", $store_info_new_phone );
    //            $store_info_new_phone = implode('-', $phone_pieces );
    //        } elseif( preg_match( "/[()]/", $store_info_new_phone, $output_array ) ) {
    //            
    //        } else {
    //            preg_match( "/([0-9]{3})([0-9]{3})([0-9]{4})/", $store_info_new_phone, $phone_pieces );
    //            $store_info_new_phone = $phone_pieces[1] . '-' . $phone_pieces[2] . '-' . $phone_pieces[3];
    //        }
    //        update_post_meta( $post_id, 'store_info_phone', $phone_numbs );
    //        update_post_meta( $post_id, 'store_info_phone', strlen($phone_numbs) );
            update_post_meta( $post_id, 'store_info_phone', $store_info_new_phone );

            $url_pieces = explode(" ", $store_info_new_address_top . ' ' . $store_info_new_address_bottom);
            $new_url = 'https://maps.google.com?daddr=' . implode('+', $url_pieces);
    //        echo $new_url;
            update_post_meta( $post_id, 'store_info_address_url', $new_url );

    //        foreach ($url_pieces as $piece) {
    //            $url_start = $url_start . '+' . $piece
    //        }
    //        
    //        https://maps.google.com?daddr=760+West+Genesee+Street+Syracuse+NY+13204

        }
    }

}

new GMG_Store_Info_Meta_Box;
