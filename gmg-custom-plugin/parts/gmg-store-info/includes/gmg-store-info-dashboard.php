<?php

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */


function example_add_dashboard_widgets() {
 	wp_add_dashboard_widget( 'gmg_store_info_dashboard_widget',
                            'Store Info', 'gmg_store_info_dashboard_widget_function', 'gmg_store_info_save_db_metabox' );
 	
 	// Globalize the metaboxes array, this holds all the widgets for wp-admin
 
 	global $wp_meta_boxes;
 	
 	// Get the regular dashboard widgets array 
 	// (which has our new widget already but at the end)
 
 	$normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core'];
 	
 	// Backup and delete our new dashboard widget from the end of the array
 
 	$example_widget_backup = array( 'gmg_store_info_dashboard_widget' => $normal_dashboard['gmg_store_info_dashboard_widget'] );
 	unset( $normal_dashboard['gmg_store_info_dashboard_widget'] );
 
 	// Merge the two arrays together so our widget is at the beginning
 
 	$sorted_dashboard = array_merge( $example_widget_backup, $normal_dashboard );
 
 	// Save the sorted array back into the original metaboxes 
 
 	$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
}

add_action( 'wp_dashboard_setup', 'example_add_dashboard_widgets' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function gmg_store_info_dashboard_widget_function() {
    
    
        // WP_Query arguments
$args = array(
	'post_type'              => array( 'Stores' ),
    'posts_per_page'         => -1,
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) : $query->the_post();
		$theID = get_the_ID();

	// Display whatever it is you want to show.
//	echo "Hello World, I'm a great Dashboard Widget and I do more";

		// Add nonce for security and authentication.
		wp_nonce_field( 'store_info_data', 'store_info_nonce' );

		// Retrieve an existing value from the database.
		$store_info_address = get_post_meta( $theID, 'store_info_address', true );
//        $store_info_address_url = get_post_meta( $theID, 'store_info_address_url', true );
		$store_info_phone = get_post_meta( $theID, 'store_info_phone', true );
        $store_info_email = get_post_meta( $theID, 'store_info_email', true );
    
    $store_info_id = $theID;
        
//		$store_info_sun_status = get_post_meta( $theID, 'store_info_sun_status', true );
        $store_info_sun_open = get_post_meta( $theID, 'store_info_sun_open', true );
        $store_info_sun_close = get_post_meta( $theID, 'store_info_sun_close', true );
        
        $store_info_mon_open = get_post_meta( $theID, 'store_info_mon_open', true );
        $store_info_tues_open = get_post_meta( $theID, 'store_info_tues_open', true );
        $store_info_wed_open = get_post_meta( $theID, 'store_info_wed_open', true );
        $store_info_thurs_open = get_post_meta( $theID, 'store_info_thurs_open', true );
        $store_info_fri_open = get_post_meta( $theID, 'store_info_fri_open', true );
        $store_info_sat_open = get_post_meta( $theID, 'store_info_sat_open', true );
        
        $store_info_mon_close = get_post_meta( $theID, 'store_info_mon_close', true );
        $store_info_tues_close = get_post_meta( $theID, 'store_info_tues_close', true );
        $store_info_wed_close = get_post_meta( $theID, 'store_info_wed_close', true );
        $store_info_thurs_close = get_post_meta( $theID, 'store_info_thurs_close', true );
        $store_info_fri_close = get_post_meta( $theID, 'store_info_fri_close', true );
        $store_info_sat_close = get_post_meta( $theID, 'store_info_sat_close', true );  
        

		// Set default values.
		if( empty( $store_info_address ) ) $store_info_address = '';
        if( empty( $store_info_address_url ) ) $store_info_address_url = '';
		if( empty( $store_info_phone ) ) $store_info_phone = '';
        if( empty( $store_info_email ) ) $store_info_email = '';
        
        if( empty( $store_info_sun_open ) ) $store_info_sun_open = '';
        if( empty( $store_info_sun_close ) ) $store_info_sun_close = '';
        
        if( empty( $store_info_mon_open ) ) $store_info_mon_open = '';
        if( empty( $store_info_tues_open ) ) $store_info_tues_open = '';
        if( empty( $store_info_wed_open ) ) $store_info_wed_open = '';
        if( empty( $store_info_thurs_open ) ) $store_info_thurs_open = '';
        if( empty( $store_info_fri_open ) ) $store_info_fri_open = '';
        if( empty( $store_info_sat_open ) ) $store_info_sat_open = '';
        
        if( empty( $store_info_mon_close ) ) $store_info_mon_close = '';
        if( empty( $store_info_tues_close ) ) $store_info_tues_close = '';
        if( empty( $store_info_wed_close ) ) $store_info_wed_close = '';
        if( empty( $store_info_thurs_close ) ) $store_info_thurs_close = '';
        if( empty( $store_info_fri_close ) ) $store_info_fri_close = '';
        if( empty( $store_info_sat_close ) ) $store_info_sat_close = '';
    
//    Form fields
        
//        echo 'Hello';
        echo ' <form class="intitial-form">';
    
    echo '<input type="hidden" id="store_info_id" name="store_info_id" class="store_info_id" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_id ) . '">'; 
        
        //Address
        echo '<div class="textarea-wrap">';
        echo '<label for="store_info_address" class="store_info_address_label">' . __( 'Address', 'text_domain' ) . '</label>';
//        echo '<textarea rows="2" cols="50" id="store_info_address" name="store_info_address" class="store_info_address_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_address ) . '"></textarea>';
        
        echo '<textarea rows="2" cols="50" id="store_info_address" name="store_info_address" class="store_info_address_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '">' . esc_textarea( $store_info_address ) . '</textarea>';
    echo '</div>';
       
        
//        Telephone
        
        echo '<div class="input-text-wrap">';
        echo '<label for="store_info_phone" class="store_info_phone_label">' . __( 'Phone', 'text_domain' ) . '</label>';
        
        echo ' <input type="tel" id="store_info_phone" name="store_info_phone" class="store_info_phone_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_phone ) . '">';
    echo '</div>';
        
        
        
//      Email        
        echo '<div class="input-text-wrap">';
        echo '<label for="store_info_email" class="store_info_email_label">' . __( 'Email (General email)', 'text_domain' ) . '</label>';
        
        echo ' <input type="email" id="store_info_email" name="store_info_email" class="store_info_email_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_email ) . '">';
        echo '</div>';
        
        
//     Intro
        
//        echo '<h2>Store Hours. (If you are closed, just leave empty.)</h2>';
        echo '<h2>Store Hours</h2>';    
        echo '<p>If you are closed, just leave empty.</p>';
//        echo '<h2>Store Hours</h2>';
        
        
//      Sunday   
        
    echo '<div class="meta-left">';
        echo '<label for="store_info_sun_open" class="">' . __( 'Sunday: Open', 'text_domain' ) . '</label>';        
        echo ' <input type="time" id="store_info_sun_open" name="store_info_sun_open" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_sun_open ) . '">';
        echo '</div>';
        
    echo '<div class="meta-right">';
        echo '<label for="store_info_sun_close" class="">' . __( 'Close', 'text_domain' ) . '</label>';
        
        echo ' <input type="time" id="store_info_sun_close" name="store_info_sun_close" class=" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_sun_close ) . '">';
            echo '</div>';
        
        
//      Monday        
        
        echo '<div id="mon-open">';
        echo '<label for="store_info_mon_open" class="">' . __( 'Monday: Open', 'text_domain' ) . '</label>';
        echo ' <input type="time" id="store_info_mon_open" name="store_info_mon_open" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_mon_open ) . '">';
                echo '</div>';
        
    echo '<div id="mon-close">';
        
        echo '<label for="store_info_mon_close" class="">' . __( 'Close', 'text_domain' ) . '</label>';
        
        echo ' <input type="time" id="store_info_mon_close" name="store_info_mon_close" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_mon_close ) . '">';
            
        echo '</div>';
        
        
//      Tuesday        
        
        echo '<div class="meta-left">';
        echo '<label for="store_info_tues_open" class="">' . __( 'Tuesday: Open', 'text_domain' ) . '</label>';
                
        echo ' <input type="time" id="store_info_tues_open" name="store_info_tues_open" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_tues_open ) . '">';
                echo '</div>';
        
    echo '<div class="meta-right">';
        
        echo '<label for="store_info_tues_close" class="">' . __( 'Close', 'text_domain' ) . '</label>';
        
        echo ' <input type="time" id="store_info_tues_close" name="store_info_tues_close" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_tues_close ) . '">';
        echo '</div>';
        
        
        
//      Wednesday        
        
        echo '<div class="meta-left">';
        echo '<label for="store_info_wed_open" class="">' . __( 'Wednesday: Open', 'text_domain' ) . '</label>';
                
        echo ' <input type="time" id="store_info_wed_open" name="store_info_wed_open" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_wed_open ) . '">';
                echo '</div>';
        
    echo '<div class="meta-right">';
        
        echo '<label for="store_info_wed_close" class="">' . __( 'Close', 'text_domain' ) . '</label>';
        
        echo ' <input type="time" id="store_info_wed_close" name="store_info_wed_close" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_wed_close ) . '">';
        
        echo '</div>';
        
        
//      Thursday        
        
        echo '<div class="meta-left">';
        echo '<label for="store_info_thurs_open" class="">' . __( 'Thursday: Open', 'text_domain' ) . '</label>';
                
        echo ' <input type="time" id="store_info_thurs_open" name="store_info_thurs_open" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_thurs_open ) . '">';
                echo '</div>';
        
    echo '<div class="meta-right">';
        
        echo '<label for="store_info_thurs_close" class="">' . __( 'Close', 'text_domain' ) . '</label>';
        
        echo ' <input type="time" id="store_info_thurs_close" name="store_info_thurs_close" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_thurs_close ) . '">';
        echo '</div>';
        
        
        
//      Friday        
        
        echo '<div class="meta-left">';
        echo '<label for="store_info_fri_open" class="">' . __( 'Friday: Open', 'text_domain' ) . '</label>';
                
        echo ' <input type="time" id="store_info_fri_open" name="store_info_fri_open" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_fri_open ) . '">';
                echo '</div>';
        
    echo '<div class="meta-right">';
        
        echo '<label for="store_info_fri_close" class="">' . __( 'Close', 'text_domain' ) . '</label>';
        
        echo ' <input type="time" id="store_info_fri_close" name="store_info_fri_close" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_fri_close ) . '">';
        echo '</div>';
        
        
        
//      Saturday        
        
        echo '<div class="meta-left">';
        echo '<label for="store_info_sat_open" class="">' . __( 'Saturday: Open', 'text_domain' ) . '</label>';
                
        echo ' <input type="time" id="store_info_sat_open" name="store_info_sat_open" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_sat_open ) . '">';
                echo '</div>';
        
    echo '<div class="meta-right">';
        
        echo '<label for="store_info_sat_close" class="">' . __( 'Close', 'text_domain' ) . '</label>';
        
        echo ' <input type="time" id="store_info_sat_close" name="store_info_sat_close" class="" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $store_info_sat_close ) . '">';
        echo '</div>';
        
        echo '<input type="submit">';
        
        echo ' </form>';

    	endwhile;
} else {
	// no posts found
}

// Restore original Post Data
wp_reset_postdata();

	}
//


	function gmg_store_info_save_db_metabox( $new_id, $post ) {

//		// Add nonce for security and authentication.
//		$si_nonce_name   = $_POST['store_info_nonce'];
//		$si_nonce_action = 'store_info_data';
//
//		// Check if a nonce is set.
//		if ( ! isset( $si_nonce_name ) )
//			return;
//
//		// Check if a nonce is valid.
//		if ( ! wp_verify_nonce( $si_nonce_name, $si_nonce_action ) )
//			return;

//		// Check if the user has permissions to save data.
//		if ( ! current_user_can( 'edit_post', $new_id ) )
//			return;
//
//		// Check if it's not an autosave.
//		if ( wp_is_post_autosave( $new_id ) )
//			return;
//
//		// Check if it's not a revision.
//		if ( wp_is_post_revision( $new_id ) )
//			return;

		// Sanitize user input.
//        $store_info_new_address = isset( $_POST[ 'store_info_address' ] ) ? sanitize_text_field( $_POST[ 'store_info_address' ] ) : '';
        $store_info_new_address = isset( $_POST[ 'store_info_address' ] ) ? wp_kses_post( $_POST[ 'store_info_address' ] ) : '';
		$store_info_new_phone = isset( $_POST[ 'store_info_phone' ] ) ? sanitize_text_field( $_POST[ 'store_info_phone' ] ) : '';
        $store_info_new_email = isset( $_POST[ 'store_info_email' ] ) ? sanitize_text_field( $_POST[ 'store_info_email' ] ) : '';
 $new_id = isset( $_POST[ 'store_info_id' ] ) ? sanitize_text_field( $_POST[ 'store_info_id' ] ) : '';
        
        $store_info_new_sun_open = isset( $_POST[ 'store_info_sun_open' ] ) ? sanitize_text_field( $_POST['store_info_sun_open' ] ) : '';        
        $store_info_new_mon_open = isset( $_POST[ 'store_info_mon_open' ] ) ? sanitize_text_field( $_POST['store_info_mon_open' ] ) : '';
        $store_info_new_tues_open = isset( $_POST[ 'store_info_tues_open' ] ) ? sanitize_text_field( $_POST[ 'store_info_tues_open' ] ) : '';
        $store_info_new_wed_open = isset( $_POST[ 'store_info_wed_open' ] ) ? sanitize_text_field( $_POST['store_info_wed_open' ] ) : '';
        $store_info_new_thurs_open = isset( $_POST[ 'store_info_thurs_open' ] ) ? sanitize_text_field( $_POST['store_info_thurs_open' ] ) : '';
        $store_info_new_fri_open = isset( $_POST[ 'store_info_fri_open' ] ) ? sanitize_text_field( $_POST['store_info_fri_open' ] ) : '';
        $store_info_new_sat_open = isset( $_POST[ 'store_info_sat_open' ] ) ? sanitize_text_field( $_POST['store_info_sat_open' ] ) : '';
        
        $store_info_new_sun_close = isset( $_POST[ 'store_info_sun_close' ] ) ? sanitize_text_field( $_POST[ 'store_info_sun_close'  ] ) : '';
        $store_info_new_mon_close = isset( $_POST[ 'store_info_mon_close' ] ) ? sanitize_text_field( $_POST[ 'store_info_mon_close'  ] ) : '';
        $store_info_new_tues_close = isset( $_POST[ 'store_info_tues_close' ] ) ? sanitize_text_field( $_POST[ 'store_info_tues_close'  ] ) : '';
        $store_info_new_wed_close = isset( $_POST[ 'store_info_wed_close' ] ) ? sanitize_text_field( $_POST[ 'store_info_wed_close'  ] ) : '';
        $store_info_new_thurs_close = isset( $_POST[ 'store_info_thurs_close' ] ) ? sanitize_text_field( $_POST[ 'store_info_thurs_close'  ] ) : '';
        $store_info_new_fri_close = isset( $_POST[ 'store_info_fri_close' ] ) ? sanitize_text_field( $_POST[ 'store_info_fri_close'  ] ) : '';
        $store_info_new_sat_close = isset( $_POST[ 'store_info_sat_close' ] ) ? sanitize_text_field( $_POST[ 'store_info_sat_close'  ] ) : '';
        
        // Update the meta field in the database.
		update_post_meta( $new_id, 'store_info_address', $store_info_new_address );
        update_post_meta( $new_id, 'store_info_phone', $store_info_new_phone );
        update_post_meta( $new_id, 'store_info_email', $store_info_new_email );
        
        update_post_meta( $new_id, 'store_info_sun_open', $store_info_new_sun_open );
        update_post_meta( $new_id, 'store_info_mon_open', $store_info_new_mon_open );
        update_post_meta( $new_id, 'store_info_tues_open', $store_info_new_tues_open );
        update_post_meta( $new_id, 'store_info_wed_open', $store_info_new_wed_open );
        update_post_meta( $new_id, 'store_info_thurs_open', $store_info_new_thurs_open );
        update_post_meta( $new_id, 'store_info_fri_open', $store_info_new_fri_open );
        update_post_meta( $new_id, 'store_info_sat_open', $store_info_new_sat_open );
        
        update_post_meta( $new_id, 'store_info_sun_close' , $store_info_new_sun_close );
        update_post_meta( $new_id, 'store_info_mon_close' , $store_info_new_mon_close );
        update_post_meta( $new_id, 'store_info_tues_close' , $store_info_new_tues_close );
        update_post_meta( $new_id, 'store_info_wed_close' , $store_info_new_wed_close );
        update_post_meta( $new_id, 'store_info_thurs_close' , $store_info_new_thurs_close );
        update_post_meta( $new_id, 'store_info_fri_close' , $store_info_new_fri_close );
        update_post_meta( $new_id, 'store_info_sat_close' , $store_info_new_sat_close );
        
        $url_pieces = explode(" ", $store_info_new_address);
        $new_url = 'https://maps.google.com?daddr=' . implode('+', $url_pieces);
//        echo $new_url;
        update_post_meta( $new_id, 'store_info_address_url', $new_url );
        
	}

//}

//new GMG_Store_Info_Dashboard_Meta_Box;
