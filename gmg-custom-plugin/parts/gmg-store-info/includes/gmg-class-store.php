<?php

class Store {
    
    public $id;

	public function __construct( $id ) {        
        $this->id = $id;
	}
    
    public function get_store_id(){
        return $this->id;
    }
    
    public function get_store_phone(){
        return get_post_meta( $this->id, 'store_info_phone', true );
    }
    
    public function get_store_state(){
        return get_post_meta( $this->id, 'store_info_address_state', true );
    }
    
}

class Stores{    
    
    public function __construct() {
        
	}
    
    public function get_all_stores(){
    
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'Stores' ),
            'posts_per_page'         => -1,
            'orderby' => 'menu_order',
            'order'   => 'ASC',
        );

        $store_array = array();

        // The Query
        $query = new WP_Query( $args );

        // The Loop
        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) : $query->the_post();
            array_push( $store_array, array(get_the_title(), get_the_ID()) );

            endwhile;
        } else {
            // no posts found
        }

        // Restore original Post Data
        wp_reset_postdata();

        return $store_array;
        
    }
    
}
