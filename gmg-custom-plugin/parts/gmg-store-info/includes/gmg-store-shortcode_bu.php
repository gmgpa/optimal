<?php

//Shortcode for showing Address
function store_info_address() {
    
    // WP_Query arguments
$args = array(
	'post_type'              => array( 'Stores' ),
    'posts_per_page'         => -1,
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) : $query->the_post();
		$theID = get_the_ID();
//        
//        $store_info_address = htmlspecialchars_decode(get_post_meta( $theID, 'store_info_address', true ));
        $store_info_address_top = get_post_meta( $theID, 'store_info_address_top', true );
        $store_info_address_bottom = get_post_meta( $theID, 'store_info_address_bottom', true );
        $store_info_address_url = get_post_meta( $theID, 'store_info_address_url', true );
//		$store_info_phone = get_post_meta( $theID, 'store_info_phone', true );
    
    $addy_phone = array(
        '<div class="header-info">',
                '<ul>',
//                '<li><a href="tel:+' . esc_attr__( $store_info_phone ) . '" target="_blank" rel="noopener">' . esc_attr__( $store_info_phone ) . '</a></li>',

                '<a href="' . esc_attr__( $store_info_address_url ) . '" target="_blank" > <li>' . esc_attr__( $store_info_address_top )  . '</li>',
        '<li>' . esc_attr__( $store_info_address_bottom )  . '</li></a>',
        
                '</ul>',
                '</div>' );
    
	endwhile;
} else {
	// no posts found
}

// Restore original Post Data
wp_reset_postdata();
    
    return implode( $addy_phone );
    
}

add_shortcode( 'address', 'store_info_address' );


//Shortcode for showing Address and Phone Number
function store_info_address_phone() {
    
    // WP_Query arguments
$args = array(
	'post_type'              => array( 'Stores' ),
    'posts_per_page'         => -1,
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) : $query->the_post();
		$theID = get_the_ID();
//        
//        $store_info_address = htmlspecialchars_decode(get_post_meta( $theID, 'store_info_address', true ));
            $store_info_address_top = get_post_meta( $theID, 'store_info_address_top', true );
        $store_info_address_bottom = get_post_meta( $theID, 'store_info_address_bottom', true );
        $store_info_address_url = get_post_meta( $theID, 'store_info_address_url', true );
		$store_info_phone = get_post_meta( $theID, 'store_info_phone', true );
    
    $addy_phone = array(
        '<div class="header-info">',
                '<ul>',
                '<li><a href="tel:+' . esc_attr__( $store_info_phone ) . '" target="_blank" rel="noopener">' . esc_attr__( $store_info_phone ) . '</a></li>',
        
        '<a href="' . esc_attr__( $store_info_address_url ) . '" target="_blank" > <li>' . esc_attr__( $store_info_address_top )  . '</li>',
        '<li>' . esc_attr__( $store_info_address_bottom )  . '</li></a>',
        
//        '<li><a href="' . esc_attr__( $store_info_address_url ) . '" target="_blank" >' . esc_html__( $store_info_address_top . '</br>' . $store_info_address_bottom ) . '</a></li>',
//                '<li><a href="' . esc_attr__( $store_info_address_url ) . '" target="_blank" >' . esc_attr__( $store_info_address ) . '</a></li>',
                '</ul>',
                '</div>' );
    
	endwhile;
} else {
	// no posts found
}

// Restore original Post Data
wp_reset_postdata();
    
    return implode( $addy_phone );
    
}

add_shortcode( 'address-phone', 'store_info_address_phone' );

//Shortcode for showing the whole shebang (address, phone & hours)
function store_info_address_phone_hours() {
    
    // WP_Query arguments
$args = array(
	'post_type'              => array( 'Stores' ),
    'posts_per_page'         => -1,
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) : $query->the_post();
		$theID = get_the_ID();
    
    $store_info = [];
    
//    $store_info_address = htmlspecialchars_decode( get_post_meta( $theID, 'store_info_address', true ) );    
    $store_info_address = get_post_meta( $theID, 'store_info_address', true );
    
    array_push( $store_info,
        '<div class="address-phone-hours">',
        '<ul>',
        '<li><a href="tel:+' . esc_attr__( $store_info_phone ) . '" target="_blank" rel="noopener">' . esc_attr__( $store_info_phone ) . '</a></li>',
               
        '<a href="' . esc_attr__( $store_info_address_url ) . '" target="_blank" > <li>' . esc_attr__( $store_info_address_top )  . '</li>',
        '<li>' . esc_attr__( $store_info_address_bottom )  . '</li></a>'
               
//        '<li><a href="' . esc_attr__( $store_info_address_url ) . '" target="_blank" >' . esc_html__( $store_info_address_top . '</br>' . $store_info_address_bottom ) . '</a></li>',
//        '<li><a href="' . esc_attr__( $store_info_address_url ) . '" target="_blank" >' . esc_attr__( $store_info_address ) . ' </a></li>'
              );
    
    
        $hours_array = do_hours($theID);
    foreach( $hours_array as $hours){
        array_push($store_info, $hours);
    }
    
    array_push($store_info,
    '</ul>',
    '</div>'
              );
    
	endwhile;
} else {
	// no posts found
}

// Restore original Post Data
wp_reset_postdata();
    
    return implode( $store_info );
    
}

add_shortcode( 'address-phone-hours', 'store_info_address_phone_hours' );


//Shortcode for showing only phone & hours
function store_info_phone_hours() {
    
    // WP_Query arguments
$args = array(
	'post_type'              => array( 'Stores' ),
    'posts_per_page'         => -1,
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) : $query->the_post();
		$theID = get_the_ID();

    $store_info_phone = get_post_meta( $theID, 'store_info_phone', true );
    
    $store_info = [];
    
    array_push($store_info,
        '<div class="address-phone-hours">',
        '<ul>',
        '<li><a href="tel:+' . esc_attr__( $store_info_phone ) . '" target="_blank" rel="noopener">' . esc_attr__( $store_info_phone ) . '</a></li>');
    
    $hours_array = do_hours($theID);
    foreach( $hours_array as $hours){
        array_push($store_info, $hours);
    }
    
    array_push($store_info,
    '</ul>',
    '</div>');
    
	endwhile;
} else {
	// no posts found
}

// Restore original Post Data
wp_reset_postdata();
    
    return implode( $store_info );
    
}

add_shortcode( 'phone-hours', 'store_info_phone_hours' );

function do_hours($theID){
    
    //Open
    $store_info_sun_open = get_post_meta( $theID, 'store_info_sun_open', true );
    if( !empty( $store_info_sun_open ) ) $store_info_sun_open = new DateTime ( $store_info_sun_open );
    
    $store_info_mon_open = get_post_meta( $theID, 'store_info_mon_open', true );
    if( !empty( $store_info_mon_open ) ) $store_info_mon_open = new DateTime ( $store_info_mon_open );
    
    $store_info_tues_open = get_post_meta( $theID, 'store_info_tues_open', true );
    if( !empty( $store_info_tues_open ) ) $store_info_tues_open = new DateTime ( $store_info_tues_open );
    
    $store_info_wed_open = get_post_meta( $theID, 'store_info_wed_open', true );
    if( !empty( $store_info_wed_open ) ) $store_info_wed_open = new DateTime ( $store_info_wed_open );
    
    $store_info_thurs_open = get_post_meta( $theID, 'store_info_thurs_open', true );
    if( !empty( $store_info_thurs_open ) ) $store_info_thurs_open = new DateTime ( $store_info_thurs_open );
    
    $store_info_fri_open = get_post_meta( $theID, 'store_info_fri_open', true );
    if( !empty( $store_info_fri_open ) ) $store_info_fri_open = new DateTime ( $store_info_fri_open );
    
    $store_info_sat_open = get_post_meta( $theID, 'store_info_sat_open', true );
    if( !empty( $store_info_sat_open ) ) $store_info_sat_open = new DateTime ( $store_info_sat_open );
    
    //Close    
    $store_info_sun_close = get_post_meta( $theID, 'store_info_sun_close', true );
    if( !empty( $store_info_sun_close ) ) $store_info_sun_close = new DateTime ( $store_info_sun_close );
    
    $store_info_mon_close = get_post_meta( $theID, 'store_info_mon_close', true );
    if( !empty( $store_info_mon_close ) ) $store_info_mon_close = new DateTime ( $store_info_mon_close );
    
    $store_info_tues_close = get_post_meta( $theID, 'store_info_tues_close', true );
    if( !empty( $store_info_tues_close ) ) $store_info_tues_close = new DateTime ( $store_info_tues_close );
    
    $store_info_wed_close = get_post_meta( $theID, 'store_info_wed_close', true );
    if( !empty( $store_info_wed_close ) ) $store_info_wed_close = new DateTime ( $store_info_wed_close );
    
    $store_info_thurs_close = get_post_meta( $theID, 'store_info_thurs_close', true );
    if( !empty( $store_info_thurs_close ) ) $store_info_thurs_close = new DateTime ( $store_info_thurs_close );
    
    $store_info_fri_close = get_post_meta( $theID, 'store_info_fri_close', true );
    if( !empty( $store_info_fri_close ) ) $store_info_fri_close = new DateTime ( $store_info_fri_close );
    
    $store_info_sat_close = get_post_meta( $theID, 'store_info_sat_close', true );
    if( !empty( $store_info_sat_close ) ) $store_info_sat_close = new DateTime ( $store_info_sat_close );
    
    $store_info_extra = get_post_meta( $theID, 'store_info_hours_extra', true );
       
    $store_info = [];
    
    array_push($store_info, '<li><h4>Business Hours:</h4></li>');
    
    if( empty( $store_info_mon_open ) && (empty( $store_info_mon_close ) )){                
        array_push($store_info, '<li><p>MON: CLOSED</p></li>');
    } else {
//                array_push($store_info, '<li><p>MON: ' . esc_attr__( $store_info_mon_open ) . ' - ' . esc_attr__( $store_info_mon_close ) . '</p></li>');
        array_push($store_info, '<li><p>MON: ' . esc_attr__( $store_info_mon_open->format('g:i A') ) . ' - ' . esc_attr__( $store_info_mon_close->format('g:i A') ) . '</p></li>');
    }

    if( empty( $store_info_tues_open ) && empty( $store_info_tues_close ) ){                
        array_push($store_info, '<li><p>tues: CLOSED</p></li>');
    } else {
        array_push($store_info, '<li><p>tues: ' . esc_attr__( $store_info_tues_open->format('g:i A') ) . ' - ' . esc_attr__( $store_info_tues_close->format('g:i A') ) . '</p></li>');
    }

        if( empty( $store_info_wed_open ) && empty( $store_info_wed_close ) ){                
        array_push($store_info, '<li><p>wed: CLOSED</p></li>');
    } else {
        array_push($store_info, '<li><p>wed: ' . esc_attr__( $store_info_wed_open->format('g:i A')) . ' - ' . esc_attr__( $store_info_wed_close->format('g:i A') ) . '</p></li>');
    }

        if( empty( $store_info_thurs_open ) && empty( $store_info_thurs_close ) ){                
        array_push($store_info, '<li><p>thurs: CLOSED</p></li>');
    } else {
        array_push($store_info, '<li><p>thurs: ' . esc_attr__( $store_info_thurs_open->format('g:i A')) . ' - ' . esc_attr__( $store_info_thurs_close->format('g:i A') ) . '</p></li>');
    }

        if( empty( $store_info_fri_open ) && empty( $store_info_fri_close ) ){                
        array_push($store_info, '<li><p>fri: CLOSED</p></li>');
    } else {
        array_push($store_info, '<li><p>fri: ' . esc_attr__( $store_info_fri_open->format('g:i A')) . ' - ' . esc_attr__( $store_info_fri_close->format('g:i A') ) . '</p></li>');
    }

        if( empty( $store_info_sat_open ) && empty( $store_info_sat_close ) ){                
        array_push($store_info, '<li><p>sat: CLOSED</p></li>');
    } else {
        array_push($store_info, '<li><p>sat: ' . esc_attr__( $store_info_sat_open->format('g:i A')) . ' - ' . esc_attr__( $store_info_sat_close->format('g:i A') ) . '</p></li>');
    }

    if( empty( $store_info_sun_open ) && empty( $store_info_sun_close ) ){                
        array_push($store_info, '<li><p>SUN: CLOSED</p></li>');
    } else {
        array_push($store_info, '<li><p>SUN: ' . esc_attr__( $store_info_sun_open->format('g:i A')) . ' - ' . esc_attr__( $store_info_sun_close->format('g:i A') ) . '</p></li>');
    }

    if( !empty( $store_info_extra ) ){
    array_push( $store_info, '<li><p>' . esc_attr__( $store_info_extra ) . ' </p></li>');
    }
    
    return $store_info;
    
}

