<?php

function gmg_get_hours( $theID ){
    
    //This is the array that we will write the HTML to.      
    $open_close = [];
    
    //This is the array that we will iterate through to generate the hours.
    $days = array(        
        0 => 'mon',
        1 => 'tues',
        2 => 'wed',
        3 => 'thurs',
        4 => 'fri',
        5 => 'sat',
        6 => 'sun'
    );
    
    $same_days = [];
    
    $store = 'store_info_';
    $s_extra = $store . 'hours_extra';
    
    //Let's start        
    foreach($days as $day){
        
//        echo var_dump( $open_close );
        
        //Each of these variables are created fresh each time to be looking at specific saved fields.
        $s_open = $store . $day . '_open';
        $s_close = $store . $day . '_close';
        $s_check = $store . $day . '_check';        
        
        
        $that_day = [];
        
        //First thing, let's check to see if the day was checked as "same as above."
        
        if ( empty( get_post_meta( $theID, $s_check, true ) ) ){
            
            //It wasn't checked!
            
            //First thing we need to know is whether there are were any Days entered before. We should check and, if so, write that first.
                
            if( isset( $class_days ) ){
                
                //We need to pop the last entry of open_close because it was actuallu covered by Days.
                array_pop ( $open_close );

                //Class Days has already been created so we need to pass that to Open_Close array.
                array_push( $open_close, '<span>' . $class_days->get_start_day() . ' - ' . $class_days->get_end_day() . ': ' . $class_days->get_start_time() . ' - ' . $class_days->get_end_time() . '</span>' );
                
                unset( $class_days );

            }
            
            //Next, we need to know if there were any hours set for that day because that will determine if they are open or closed that day.
            
            //Let's check if hours have been entered.
            if ( get_post_meta( $theID, $s_open, true ) && get_post_meta( $theID, $s_close, true ) ){
                
                //Hours have been entered.
                
                //We need:
                
                //Open Time
                $that_open = new DateTime ( get_post_meta( $theID, $s_open, true ) );                
                
                //Close Time
                $that_close = new DateTime ( get_post_meta( $theID, $s_close, true ) );
                
                $open_time = gmg_check_if_half_hour( $that_open );                
                $close_time = gmg_check_if_half_hour( $that_close );                
                
                //Publish the entry.                
                array_push($open_close, '<span>' . $day . ': ' . $open_time . ' - ' . $close_time . '</span>');
                
            } else {
                
                //Hours haven't been entered therefore they are closed that day.
                array_push($open_close, "<span> $day: CLOSED</span>" );
            }
            
        } else {
            
            //It was checked.
            
            //First, let's look to see if there were any other days checked already. How we do that is see if class Days has been created.
            
            if( isset( $class_days ) ){
                
                //Class Days has already been created so we can go ahead and rewrite part of it.
                
                $class_days->set_end_day( $day );
                
            } else {
                
                //It hasn't so we're going to have to create a new class Day variable.  To do that, we're going to need:
                
                //Index of the current day
                $position = array_search($day, $days);
                
                //Using that, I can get the day before.
                $previous_day = $days[ $position - 1 ];
                
                //Open Time
                $s_open = $store . $previous_day . '_open';
                $that_open = new DateTime (get_post_meta( $theID, $s_open, true ) );
                
                //Close Time
                $s_close = $store . $previous_day . '_close';
                $that_close = new DateTime (get_post_meta( $theID, $s_close, true ) );
                
                $open_time = gmg_check_if_half_hour( $that_open );                
                $close_time = gmg_check_if_half_hour( $that_close );
                
                //Instantiate the class.
                $class_days = new Days( $previous_day, $day, $open_time, $close_time );
                
            }
            
        }
        
    }
    
    if( !empty( get_post_meta( $theID, $s_extra, true ) ) ){
        
        $store_extra = get_post_meta( $theID, $s_extra, true );
        array_push( $open_close, '<span>' . esc_attr__( $store_extra ) . ' </span>');
    }
    
    return $open_close;
    
}

function gmg_check_if_half_hour( $time ){
    
    if( $time->format('i') > 0 ){
        return $time->format('g:i A');
    } else {
       return $time->format('g A'); 
    }
}

function gmg_get_email( $theID ){
    
    $email = array(
        '<a class="button email" href="mailto:' . esc_attr__( get_post_meta( $theID, 'store_info_email', true ) ) . '" target="_blank">',
        'Email',
        '</a>');
    
    return $email;
    
}

function gmg_get_address($theID){
    
    $address = array(
        '<a href="' . esc_attr__( get_post_meta( $theID, 'store_info_address_url', true ) ) . '" target="_blank">',
//        '<ul>',
        '<i class="fa fa-map-marker" aria-hidden="true"></i> ' . esc_attr__( get_post_meta( $theID, 'store_info_address_top', true ) )  . ', ',
        esc_attr__( get_post_meta( $theID, 'store_info_address_bottom', true ) ),
//        '</ul>',
        '</a>'
    );
    
    return $address;
    
}

function gmg_get_stacked_address($theID){
    
    $address = array(
        '<a href="' . esc_attr__( get_post_meta( $theID, 'store_info_address_url', true ) ) . '" target="_blank">',
//        '<ul>',
        '<span><i class="fa fa-map-marker" aria-hidden="true"></i> ' . esc_attr__( get_post_meta( $theID, 'store_info_address_top', true ) )  . '</span><span>',
        esc_attr__( get_post_meta( $theID, 'store_info_address_bottom', true ) )  . '</span>',
//        '</ul>',
        '</a>'
    );
    
    return $address;
    
}

function gmg_get_store_state($theID){
    
    return esc_attr__( get_post_meta( $theID, 'store_info_address_state', true )  );
}

function gmg_get_store_name($theID){
    
    return esc_attr__( get_the_title( $theID ) );
}

function gmg_get_store_location($theID){
    $location = get_post_meta( $theID, 'store_info_address_location', true );
    
    return $location;    
}

function gmg_get_store_name_and_location($theID){
    
    $name = get_the_title( $theID );
    $location = get_post_meta( $theID, 'store_info_address_location', true );
    
    return $name . ' <span class="location-name">' . $location . '</span>';    
}

function gmg_get_phone($theID){
    
    return '<a href="tel:+1-' . esc_attr__( get_post_meta( $theID, 'store_info_phone', true ) ) . '" target="_blank" rel="noopener"><i class="fa fa-phone" aria-hidden="true"></i> ' . esc_attr__( get_post_meta( $theID, 'store_info_phone', true ) ) . '</a>';
}

function gmg_get_phone_and_state($theID){
    
    return '<a href="tel:+1-' . esc_attr__( get_post_meta( $theID, 'store_info_phone', true ) ) . '" target="_blank" rel="noopener"><i class="fa fa-phone" aria-hidden="true"></i> ' . esc_attr__( get_post_meta( $theID, 'store_info_phone', true ) ) . ' (' . gmg_get_store_state($theID) . ')</a>';
}

function gmg_get_the_IDs(){
    
    // WP_Query arguments
    $args = array(
        'post_type'              => array( 'Stores' ),
        'posts_per_page'         => -1,
    );

    // The Query
    $query = new WP_Query( $args );
    
    $ids = array();

    // The Loop
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post();
        
            array_push( $ids, get_the_ID() );

        endwhile;
    } else {
        // no posts found
    }

    // Restore original Post Data
    wp_reset_postdata();
    
    if( count( $ids ) > 0 ){
        
        return $ids;
    } else {
        
        return false;
    }
}

function gmg_get_the_ID(){
    
    // WP_Query arguments
    $args = array(
        'post_type'              => array( 'Stores' ),
        'posts_per_page'         => -1,
    );

    // The Query
    $query = new WP_Query( $args );

    // The Loop
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post();
            $theID = get_the_ID();

        endwhile;
    } else {
        // no posts found
    }

    // Restore original Post Data
    wp_reset_postdata();
    
    return $theID;
}

//Shortcode for showing Address Section
function store_info_address_section( $id ) {
    
    $addy = array(
        '<div class="store-info gmg-address">',
        implode( gmg_get_address( $id ) ),
        '</div>');
    
    return $addy;
    
}

//Shortcode for showing a button for email
function store_info_email() {
    
    $email = array(
        '<div class="store-info gmg-email">',
        implode( gmg_get_email( gmg_get_the_ID() ) ),
        '</div>');
    
    return implode( $email );
    
}
add_shortcode( 'email', 'store_info_email' );

//Shortcode for showing Address
function store_info_address() {
    
    $addy = array(
        '<div class="store-info gmg-address">',
        implode( gmg_get_address( gmg_get_the_ID() ) ),
        '</div>');
    
    return implode( $addy );
    
}
add_shortcode( 'address', 'store_info_address' );

//Shortcode for showing Address
function store_info_address_1() {
        
    $addy = array(
        '<div class="store-info gmg-address">',
        implode( gmg_get_address( gmg_get_the_IDs()[0] ) ),
        '</div>');
    
    return implode( $addy );
    
}
add_shortcode( 'address_1', 'store_info_address_1' );

//Shortcode for showing Address
function store_info_address_2() {
    
    $addy = array(
        '<div class="store-info gmg-address">',
        implode( gmg_get_address( gmg_get_the_IDs()[1] ) ),
        '</div>');
    
    return implode( $addy );
    
}
add_shortcode( 'address_2', 'store_info_address_2' );

//Shortcode for showing Address stacked
function store_info_stacked_address() {
    
    $addy = array(
        '<div class="store-info gmg-address">',
        implode( gmg_get_stacked_address( gmg_get_the_ID() ) ),
        '</div>');
    
    return implode( $addy );
    
}
add_shortcode( 'address-stacked', 'store_info_stacked_address' );

//Shortcode for showing the phone number section
function store_info_phone_section( $id ) {
    
    $phone = array(
        '<div class="store-info gmg-phone">',
        gmg_get_phone(  $id  ),
        '</div>');
    
    return $phone;
    
}


//Shortcode for showing the phone number
function store_info_phone() {
    
    $phone = array(
        '<div class="store-info gmg-phone">',
        gmg_get_phone( gmg_get_the_ID() ),
        '</div>');
    
    return implode( $phone );
    
}
add_shortcode( 'phone', 'store_info_phone' );
                      
//Shortcode for showing the phone numbers of all stores and store names
function store_info_phones_stores() {
    
    $stores = gmg_get_the_stores();
    $stores_html = array( '<div class="phone-box">');
    
    foreach( $stores as $store ){
        
        array_push( $stores_html , '<div class="phone-' . $store . '">' );
        array_push( $stores_html , '<ul>' );
        array_push( $stores_html , gmg_get_phone_and_state( $store ) );
        array_push( $stores_html , '</ul>' );
        array_push( $stores_html , '</div>' );
        
    }
    
    array_push( $stores_html , '</div>' );
    return implode( $stores_html );        
    
}
add_shortcode( 'phones', 'store_info_phones_stores' );

//Shortcode for showing only hours
function store_info_hours_section( $id ) {
    
    $hours = array(
        '<div class="store-info gmg-hours">',
        implode( gmg_get_hours($id) ),
        '</div>');
    
    return $hours;    
}

//Shortcode for showing only hours
function store_info_hours() {
    
    $id = gmg_get_the_ID();
    
    $hours = array(
        '<div class="store-info gmg-hours">',
        implode( gmg_get_hours ($id) ),
        '</div>');
    
    return implode( $hours );    
}
add_shortcode( 'hours', 'store_info_hours' );


//Shortcode for showing Address and Phone Number
function store_info_address_phone() {
    
    $id = gmg_get_the_ID();
    
    $addy_phone = array(
        '<div class="store-info">',
        gmg_get_phone( $id ),
        implode( gmg_get_address( $id  ) ),
        '</div>');
    
    return implode( $addy_phone );
}
add_shortcode( 'address-phone', 'store_info_address_phone' );


//Shortcode for showing only phone & hours
function store_info_phone_hours() {
    
    $id = gmg_get_the_ID();
    
    $phone_hours = array(
        '<div class="store-info gmg-phone-hours">',
        gmg_get_phone( $id ),
        '<span><h4>Business Hours:</h4></span>',
        implode( gmg_get_hours ($id) ),
        '</div>');
    
    return implode( $phone_hours );    
}
add_shortcode( 'phone-hours', 'store_info_phone_hours' );


//Shortcode for showing the whole shebang (address, phone & hours)
function store_info_address_phone_hours() {
    
    $id = gmg_get_the_ID();
    
    $addy_phone_hours = array(
        '<div class="store-info">',
        implode( gmg_get_address( $id  ) ),
        gmg_get_phone( $id ),
        '<span><h4>Business Hours:</h4></span>',
        implode( gmg_get_hours ($id) ),
        '</div>');
    
    return implode( $addy_phone_hours );
}
add_shortcode( 'address-phone-hours', 'store_info_address_phone_hours' );

function gmg_get_the_stores(){
    
    // WP_Query arguments
    $args = array(
        'post_type'              => array( 'Stores' ),
        'posts_per_page'         => -1,
        'orderby' => 'menu_order',
        'order'   => 'ASC',
    );
    
    $store_array = array();

    // The Query
    $query = new WP_Query( $args );

    // The Loop
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post();
        array_push( $store_array, get_the_ID() );

        endwhile;
    } else {
        // no posts found
    }

    // Restore original Post Data
    wp_reset_postdata();
    
    return $store_array;
}

//Shortcode for showing all the stores' phones and locations
function store_info_stores_phones_locations() {
    
    $stores = gmg_get_the_stores();
    $stores_html = array( '<div class="stores">');
    
    foreach( $stores as $store ){          
        
        array_push( $stores_html , '<div class="store" id="store_' . $store . '">' );
        array_push( $stores_html , '<strong>' . gmg_get_store_name_and_location( $store ) . '</strong>');
        array_push( $stores_html , implode( store_info_phone_section( $store  ) ) );
        array_push( $stores_html , implode( store_info_address_section( $store  ) ) );
        array_push( $stores_html , '</div>' );
    }
    
    array_push( $stores_html , '</div>' );
    
    return implode( $stores_html );
}
add_shortcode( 'stores-phones-locations', 'store_info_stores_phones_locations' );

//Shortcode for showing all the stores' info
function store_info_stores_names_hours() {
    
    $stores = gmg_get_the_stores();
    $stores_html = array( '<div class="stores">');
    
    foreach( $stores as $store ){
        
//        echo var_dump( $store );
        
        array_push( $stores_html , '<div class="store" id="store_' . $store . '">' );
        array_push( $stores_html , '<strong>' . gmg_get_store_name_and_location($store) . '</strong>');
        array_push( $stores_html , implode( store_info_hours_section( $store ) ) );
        array_push( $stores_html , '</div>' );
    }
    
    array_push( $stores_html , '</div>' );
    
    return implode( $stores_html );
}
add_shortcode( 'stores-names-hours', 'store_info_stores_names_hours' );

//Shortcode for showing all the stores
function store_info_stores() {
    
    $stores = gmg_get_the_stores();
    $stores_html = array( '<div class="stores">');
    
    foreach( $stores as $store ){
        
        array_push( $stores_html , '<div class="store" id="store_' . $store . '">' );
        array_push( $stores_html , '<div class="image-box">' );
        array_push( $stores_html , '<a href="/">' );
        array_push( $stores_html , get_the_post_thumbnail( $store, 'medium', array( 'class' => 'aligncenter' ) ) );
        array_push( $stores_html , '</a>' );
        array_push( $stores_html , '</div>');
        array_push( $stores_html , '<div class="info-box">' );
        array_push( $stores_html , '<ul>' );
        array_push( $stores_html , gmg_get_phone( $store  ) );
        array_push( $stores_html , implode( gmg_get_address( $store  ) ) );
        array_push( $stores_html , '</ul>' );
        array_push( $stores_html , '</div>' );
        array_push( $stores_html , '</div>' );
    }
    array_push( $stores_html , '<div class="hours-link-section"><a href="#custom_html-18" class="button hours">Hours</a></div>' );
    
    array_push( $stores_html , '</div>' );
    
    return implode( $stores_html );
}
add_shortcode( 'stores-info', 'store_info_stores_info' );

//Shortcode for showing all the stores in Header Right
function store_info_header_stores() {
    
    $stores = gmg_get_the_stores();
    $stores_html = array( '<div class="stores">');
    
    foreach( $stores as $store ){
        
        array_push( $stores_html , '<div class="store" id="store_' . $store . '">' );
        array_push( $stores_html , '<strong>' . gmg_get_store_location( $store ) . '</strong>');
        array_push( $stores_html , '<ul>' );
        array_push( $stores_html , implode( gmg_get_address( $store  ) ) );
        array_push( $stores_html , gmg_get_phone( $store  ) );        
        array_push( $stores_html , '</ul>' );
        array_push( $stores_html , '</div>' );
    }
    
    array_push( $stores_html , '</div>' );
    
    return implode( $stores_html );
}
add_shortcode( 'header_stores', 'store_info_header_stores' );

//Shortcode for showing all the stores on Front Page
function store_info_front_stores() {
    
    $stores = gmg_get_the_stores();
    $stores_html = array( '<div class="stores">');
    
    foreach( $stores as $store ){
        
        array_push( $stores_html , '<div class="store" id="store_' . $store . '">' );
        array_push( $stores_html , '<strong>' . gmg_get_store_name_and_location( $store ) . '</strong>');
        array_push( $stores_html , '<ul>' );
        array_push( $stores_html , '<li>' . implode( gmg_get_address( $store  ) ) . '</li>' );
        array_push( $stores_html , '<li>' . gmg_get_phone( $store  ) . '</li>' );        
        array_push( $stores_html , '</ul>' );
        array_push( $stores_html , '</div>' );
    }
    
    array_push( $stores_html , '</div>' );
    
    return implode( $stores_html );
}
add_shortcode( 'front_stores', 'store_info_front_stores' );

//Shortcode for showing all the stores, hours, and special code
function store_info_stores_hours() {
    
    $stores = gmg_get_the_stores();
    $stores_html = array( '<div class="stores">');
    
    foreach( $stores as $store ){
        
        array_push( $stores_html , '<div class="store" id="store_' . $store . '">' );
        array_push( $stores_html , '<li><h4>' . $value[0] . '</h4></li>' );
        array_push( $stores_html , '<div class="info-box">' );
        array_push( $stores_html , '<ul>' );
        array_push( $stores_html , gmg_get_phone( $store  ) );
        array_push( $stores_html , implode( gmg_get_address_gps( $store  ) ) ); 
        array_push( $stores_html , '</ul>' );
        array_push( $stores_html , '</div>' );
        array_push( $stores_html , '</div>' );
    }
    
    array_push( $stores_html , '<div class="info-hours">');
    array_push( $stores_html , '<li><strong>Hours of Operation</strong></li>' );
    array_push( $stores_html , implode( gmg_get_hours( $store  ) ) );
    array_push( $stores_html , '</div>' );
    
    
    array_push( $stores_html , '</div>' );
    
    return implode( $stores_html );
}
add_shortcode( 'stores-hours', 'store_info_stores_hours' );

//Shortcode for showing all the stores
function store_info_stores_footer() {
    
    $stores = gmg_get_the_stores();
    $stores_html = array( '<div class="stores-stacked">');
    
    foreach( $stores as $store ){
        
        array_push( $stores_html , '<div class="store-box box-' . $store . '">' );
        array_push( $stores_html , '<img src="' . get_the_post_thumbnail_url( $store, 'medium') . '" width="150" />' );
        array_push( $stores_html , '<div class="info-box">' );
        array_push( $stores_html , '<ul>' );
        array_push( $stores_html , gmg_get_phone( $store  ) );
        array_push( $stores_html , implode( gmg_get_address( $store  ) ) ); 
        array_push( $stores_html , '</ul>' );
        array_push( $stores_html , '</div>' );
        array_push( $stores_html , '</div>' );
    }
    
    array_push( $stores_html , '</div>' );
    
    return implode( $stores_html );
}

add_shortcode( 'stores-footer', 'store_info_stores_footer' );

//Shortcode for showing all the store names
function store_info_store_name() {
    
    $stores_class = new Stores();
    $stores = $stores_class->get_all_stores();
    
    $stores_html = array( '<div class="store-name">');
    
    foreach( $stores as $key => $value ){
        array_push( $stores_html , '<h2>' . get_the_title( $value[1] ) . '</h2>' );
    }
    
    array_push( $stores_html , '</div>' );
    
    return implode( $stores_html );
}

add_shortcode( 'store-name', 'store_info_store_name' );
