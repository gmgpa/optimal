<?php

add_filter( 'widget_text', 'do_shortcode' );

add_filter('genesis_pre_get_option_footer_text', 'gmg_footer_creds_filter');
function gmg_footer_creds_filter( $creds ) {
    
	$creds = '<div id="footer-footer"><div id="footer-left">[footer_copyright first="<p>" after="</p>"] &middot; <a href="' . site_url() . '">' . get_bloginfo( 'name' ) . '</a><p>All Rights Reserved</div> <div id="footer-right"><a href="https://goodmarketinggroup.com" target="_blank"><img src="https://hearth.goodmarketinggroup.com/wp-content/uploads/2018/12/WebDesign-GMG-H-WHT.png"/></a></div></div>';
	return $creds;
}

function gmg_login_css_created(){
    
    $url = get_theme_mod( 'custom_logo' );
        
    if( $url ){
        
        $image = wp_get_attachment_image_src( $url , 'medium' )[0];

        $text = "#login h1 a {background-image: url('$image') !important;background-position: center;height: 270px;width: 270px;background-size: 230px;}";

//        error_log( $text );
        
        $dir = plugin_dir_path( __FILE__ );
            
        $file = fopen( $dir . 'css/gmg-login.css', "w" );
        if( $file ){

//            error_log( 'File opened!' );

            fwrite( $file, $text );
            fclose($file);

            recurseCopy( $dir . 'css', get_stylesheet_directory() . '/css' );
        }
    }
}

add_action('login_enqueue_scripts', 'gmg_login_css');
// Custom WordPress Login Logo
function gmg_login_css() {
    
    gmg_login_css_created();
    
    $dir = plugin_dir_path( __FILE__ );
    
    if( file_exists( $dir . 'css/gmg-login.css') && !file_exists( get_stylesheet_directory() . '/css/gmg-login.css' ) ){
        
        recurseCopy( $dir . 'css', get_stylesheet_directory() . '/css' );
    }
    
	wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/css/gmg-login.css', array() , NULL, 'all' );
//    wp_enqueue_style( 'custom-login', get_stylesheet_directory() . '/css/gmg-login.css');
//    error_log( 'CSS!');
}


function gmg_add_read_more() {
    // if this is a singular page, abort.
    if ( is_singular() ) {
        return;
    }

    printf( '<div class="fpa-more-link"><a href="%s" class="gmg-more-link button">%s</a></div>', get_permalink(), esc_html__( 'Read More' ) );
}
add_action( 'genesis_entry_content', 'gmg_add_read_more', 15 );


/*
*
* Used by Mace Energy and Clean Water America
*
* Add description to menu items
*
*/

//* Add description to menu items
//add_filter( 'walker_nav_menu_start_el', 'gmg_add_description', 10, 2 );
function gmg_add_description( $item_output, $item ) {
    $description = $item->post_content;
    if (' ' !== $description ) {
        return preg_replace( '/(<a.*)</', '$1' . '<span class="menu-description">' . $description . '</span><', $item_output) ;
    }
    else {
        return $item_output;
    };
}