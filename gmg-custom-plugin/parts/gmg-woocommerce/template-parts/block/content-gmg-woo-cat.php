<?php
/**
 * Block Name: GMG Form
 *
 * This is the template that displays the gmg-form block.
 */

$woo_cat = get_field( 'woocommerce_category' );

$index = 1;
$row = 0;
        
$args = array(
   'post_type' => 'product',
   'orderby'   => 'title',
   'tax_query' => array(
       array(
            'taxonomy'  => 'product_cat',
            'field'     => 'id',
            'terms'     => $woo_cat
        ),
   ),
    'posts_per_page' => -1
);
        
// The Query
$query = new WP_Query( $args );
        
// The Loop
if ( $query->have_posts() ) {
    
    $div_class = "archive tax-product_cat woocommerce woocommerce-page woocommerce-js";
//    $div_class = "wc-block-grid wp-block-product-category wc-block-product-category has-4-columns has-multiple-rows has-aligned-buttons";
    
//    $ul_class = "wc-block-grid__products";
    $ul_class = "products columns-4";
    
    ?>

    <div class="<?php echo $div_class; ?>">
        <ul class="<?php echo $ul_class; ?>">
            
            <?php

    while ( $query->have_posts() ) {
        
        $row_eq = 4 * intval( $row );
//        error_log('Index is ' . $index );
//        error_log('Row is ' . $row );
//        error_log('Row equation ' . $row_eq );
//        error_log('Row position is ' . bcsub(intval( $index ), intval( $row_eq )) );
        
        if( $index == 1 ){
            
            $li_class = "entry has-post-thumbnail product type-product first";
            
        } elseif( $index % 4 == 0 ){
            
            $li_class = "entry has-post-thumbnail product type-product last";
            
            $row++;
            
        }elseif( bcsub(intval( $index ), intval( $row_eq )) == 1 ){
            
            $li_class = "entry has-post-thumbnail product type-product first";
            
        } else {
        
        //    $li_class = "wc-block-grid__product";
        $li_class = "entry has-post-thumbnail product type-product";
            
        }
        
        ?>
            
        <li class="<?php echo $li_class; ?>">

            <?php $query->the_post(); ?>

            <a href="<?php echo get_the_permalink(); ?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                <?php $thumb = get_the_post_thumbnail_url( get_the_ID(), 'woocommerce_thumbnail' ); ?>
                <img src="<?php echo $thumb; ?>" alt="<?php echo get_the_title(); ?>" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" >
                <h2 class="woocommerce-loop-product__title"><?php echo get_the_title(); ?></h2>
            </a>
            <a href="<?php echo get_the_permalink(); ?>" aria-label="Learn more about “<?php echo get_the_title(); ?>”" data-quantity="1" data-product_id="<?php echo get_the_id(); ?>" rel="nofollow" class="button">Learn More</a>
        </li>
            
            <?php
        
        $index++;
                    
                }
    
    ?>
            
            </ul>
        </div>
    
<?php

} else {
    //No posts
}
    
// Restore original Post Data
wp_reset_postdata();

