<?php

/**
 * GMG WooCommerce
 *
 * @package           gmg-woocommerce
 * @author            Greg Cowell & Raymond M Rose
 * @license           GPL-2.0+
 * @link              http://goodgroupllc.com/
 * @copyright         2017 Good Marketing Group
 *
 * @wordpress-plugin
 * Plugin Name:       GMG WooCommerce
 * Plugin URI:        http://goodgroupllc.com/
 * Description:       Implements various changes in WooCommerce including custom fields & turning the 'Add to Cart Button' into a 'Request for Info.'
 * Version:           1.0.2
 * Author:            Greg Cowell & Raymond M Rose
 * Author URI:        http://goodgroupllc.com/
 * Text Domain:       gmg-slider
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

$dir = plugin_dir_path( __FILE__ );

require_once($dir . 'custom-fields/gmg-woo-custom-fields.php');
require_once($dir . 'request-info/gmg-woocommerce-request-info.php');
require_once($dir . 'woo-cat/gmg-woo-cat.php');

add_action( 'after_setup_theme', 'gmg_woo_theme_setup' ); 
function gmg_woo_theme_setup() {
//    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}

gmg_woo_install_block_files();

function gmg_woo_install_block_files(){
    
    $dir = plugin_dir_path( __FILE__ );
    
//    error_log( 'Install block files from ' . $dir . 'template-parts/block/content-gmg-woo-cat.php' );

    if( file_exists( $dir . 'template-parts/block/content-gmg-woo-cat.php') && !file_exists( get_stylesheet_directory() . '/template-parts/block/content-gmg-woo-cat.php' ) ){
            
//            error_log( 'File does not exist in theme' );
            
            recurseCopy( $dir . 'template-parts', get_stylesheet_directory() . '/template-parts' );

//            copy( $dir . 'template-parts/block/', get_template_directory() . 'template-parts/block/' ) );

    }
}