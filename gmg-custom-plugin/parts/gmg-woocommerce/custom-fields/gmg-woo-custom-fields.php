<?php

// Display WooCommerce Custom Fields
add_action( 'woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields' );

// To Create WooCommerce Custom Fields
function woo_add_custom_general_fields() {
    
    $brand_names = [];
    
    $args = array(
        'post_type'              => array( 'brands' ),
        'posts_per_page'         => '-1',
    );

    // The Query
    $query = new WP_Query( $args );

    // The Loop
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) {
            $query->the_post();
            
            $brand_names[get_the_title()] = get_the_title();
//            $brand_names[get_the_title()] = "__( '" . get_the_title() . '') ;
        }
    }
    
    $ordered_brands = gmg_custom_sort($brand_names);
    
    // Restore original Post Data
    wp_reset_postdata();

  global $woocommerce, $post;
  
  echo '<div class="options_group">';
    
    // Select   
    woocommerce_wp_select(
        array(
            'id'                => '_brand',
            'label'             => __( 'Brand information', 'woocommerce' ),
            'options'           => $ordered_brands,
            'description'       => __( 'Enter the Brand name.', 'woocommerce' )
        )
    );
    
    woocommerce_wp_text_input(
        array(
            'id'                => '_video_url',
            'label'             => __( 'Video Link', 'woocommerce' ),
            'placeholder'       => '',
            'description'       => __( 'If exists, enter a link to product video', 'woocommerce' ),
            'type'              => 'text'
        )
    );
    
    woocommerce_wp_text_input(
        array(
            'id'                => '_pdf_url',
            'label'             => __( 'PDF Link', 'woocommerce' ),
            'placeholder'       => '',
            'description'       => __( 'If exists, enter a link to product brochure', 'woocommerce' ),
            'type'              => 'text'
        )
    );
    
    woocommerce_wp_text_input(
        array(
            'id'                => '_byo_url',
            'label'             => __( 'Build Your Own Fireplace Link', 'woocommerce' ),
            'placeholder'       => '',
            'description'       => __( 'If exists, enter a link to Build Your Own Fireplace (only certain mfrs)', 'woocommerce' ),
            'type'              => 'text'
        )
    );
    
  echo '</div>';
	
}

function gmg_custom_sort($array){
    
    asort($array);
    $new_array = [];
    
    return $array;
}

// Save WooCommerce Custom Fields
add_action( 'woocommerce_process_product_meta', 'woo_add_custom_general_fields_save' );

// To Save WooCommerce Custom Fields
function woo_add_custom_general_fields_save( $post_id ){
    
	// Number Field
	$woocommerce_brand_field = $_POST['_brand'];
	if( !empty( $woocommerce_brand_field ) )
		update_post_meta( $post_id, '_brand', esc_attr( $woocommerce_brand_field ) ); 
    
    $woocommerce_video_url = $_POST['_video_url'];
	if( !empty( $woocommerce_video_url ) )
		update_post_meta( $post_id, '_video_url', esc_attr( $woocommerce_video_url ) );
    
    $woocommerce_pdf_url = $_POST['_pdf_url'];
	if( !empty( $woocommerce_pdf_url ) )
		update_post_meta( $post_id, '_pdf_url', esc_attr( $woocommerce_pdf_url ) );
    
    $woocommerce_pdf_url = $_POST['_byo_url'];
	if( !empty( $woocommerce_pdf_url ) )
		update_post_meta( $post_id, '_byo_url', esc_attr( $woocommerce_pdf_url ) );
}

add_filter( 'woocommerce_catalog_orderby', 'gmg_update_sorting_name' );
add_filter( 'woocommerce_default_catalog_orderby_options', 'gmg_update_sorting_name' );

// Removing all price-related sorting options
function gmg_update_sorting_name( $orderby ) {
    unset($orderby["price"]);
    unset($orderby["price-desc"]);
    
    return $orderby;

}

add_action('woocommerce_single_product_summary', 'woo_display_custom_general_fields_values', 35);
function woo_display_custom_general_fields_values() {
    global $product;
    
     // WP_Query arguments
    $args = array(
        'post_type'              => array( 'Stores' ),
        'posts_per_page'         => -1,
    );

    // The Query
    $query = new WP_Query( $args );

    // The Loop
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post();
            $theID = get_the_ID();

		$store_info_phone = get_post_meta( $theID, 'store_info_phone', true );
        
        endwhile;
        
        } else {
            // no posts found
        }

        // Restore original Post Data
        wp_reset_postdata();
        
    echo '<div class="woo-custom">';
        
    echo '<a href="tel:+' . $store_info_phone . '" target="_blank" ><p class="with-icon"><i class="fa fa-phone-square" aria-hidden="true"></i>' . $store_info_phone . '</p></a>';
        
    echo '<div class="brand-name">';
    
    echo '</div>';
    
    echo '</div>';
}



add_filter( 'woocommerce_default_catalog_orderby_options', 'gmg_woocommerce_catalog_orderby', 10 );
add_filter( 'woocommerce_catalog_orderby', 'gmg_woocommerce_catalog_orderby', 20 );

function gmg_woocommerce_catalog_orderby( $sortby ) {
	$sortby['product_cat'] = __( 'Sort by Category', 'woocommerce' );
	return $sortby;
}


add_filter( 'woocommerce_get_catalog_ordering_args', 'gmg_woocommerce_get_catalog_ordering_args', 20 );
function gmg_woocommerce_get_catalog_ordering_args( $args ) {
	$orderby_value = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
	if ( 'product_cat' == $orderby_value ) {
//		$args['orderby'] = array( 'term_id' => 'ASC', 'title' => 'ASC' );
$args['orderby'] = 'term_id meta_value title';
$args['order'] = 'ASC';
$args['meta_key'] = '_sku';
//				$args['meta_key'] = '';
}
//		$args['orderby'] = 'title';
//		$args['order']   = 'ASC';

//	}
	return $args;
}

add_filter( 'woocommerce_product_tabs', 'gmg_woo_rename_tabs', 98, 1 );
function gmg_woo_rename_tabs( $tabs ) {

	$tabs['description']['title'] = __( 'Features' );		// Rename the description tab
	$tabs['reviews']['title'] = __( 'More Information' );				// Rename the reviews tab

	return $tabs;
}


function woo_related_products_limit() {
  global $product;
	
	$args['posts_per_page'] = 6;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
  function jk_related_products_args( $args ) {

	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 3; // arranged in 2 columns
	return $args;
}

/*---------------------------------------------------------*/

/*
*
* These were created for Masters Chimney
*
*/

//add_action( 'woocommerce_product_options_pricing', 'gmg_woo_add_start_and_end_pricing' );
function gmg_woo_add_start_and_end_pricing(){
    
    woocommerce_wp_text_input(
        array(
            'id'                => '_start_price',
            'label'             => __( 'Start Price ($)', 'woocommerce' ),
            'placeholder'       => '',
            'description'       => __( '', 'woocommerce' ),
            'type'              => 'text'
        )
    );
    
    woocommerce_wp_text_input(
        array(
            'id'                => '_end_price',
            'label'             => __( 'End Price ($)', 'woocommerce' ),
            'placeholder'       => '',
            'description'       => __( '', 'woocommerce' ),
            'type'              => 'text'
        )
    );
    
}

// Save WooCommerce Custom Fields
//add_action( 'woocommerce_process_product_meta', 'gmg_woo_save_custom_pricing' );
function gmg_woo_save_custom_pricing( $post_id ){
    
	$woocommerce_start_field = $_POST['_start_price'];
    $woocommerce_end_field = $_POST['_end_price'];
    
    if( !empty( $woocommerce_start_field ) )
		update_post_meta( $post_id, '_start_price', esc_attr( $woocommerce_start_field ) );

	if( !empty( $woocommerce_end_field ) )
		update_post_meta( $post_id, '_end_price', esc_attr( $woocommerce_end_field ) );
}

//add_filter( 'woocommerce_product_tabs', 'woo_custom_description_tab', 98 );
function woo_custom_description_tab( $tabs ) {
    $tabs['description']['title'] = __( 'Information' );		// Rename the description tab
    
    //grab global product
    global $product;
    
    //get the ID
    $post_id = $product->get_id();
    
     error_log( 'Product ID is ' . $post_id );
    
    //get the images
    $install_images = get_field('product_installation_images_gallery', $post_id );
    
     //If there are more than one image, then we  want a slideshow and we need to call those images
    if( count( $install_images ) > 0 ){
        
        error_log( 'Found images!');
        
        $tabs['gmg-installation'] = array(
            'title' => __( 'Installation Images' ),
            'callback'  => 'gmg_woo_custom_field_installation_tab',
            'priority'  => '15',
        );
        
    }
    
    unset( $tabs['reviews'] );

    return $tabs;
}

function gmg_woo_custom_field_installation_tab() {
    
    //get the images
    $install_images = get_field('product_installation_images_gallery');
    
    echo '<div class="entry-content product-installation-images">';
				
	echo '<h2>Installation Images</h2>';
    
     //If there are more than one image, then we  want a slideshow and we need to call those images
    if( count( $install_images ) > 1 ){

    ?>
        <div id="slider" class="flexslider">
            <ul class="slides">
                <?php foreach( $install_images as $image ): ?>
                    <li>
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php
        
    } else {
        
        $shortcode = sprintf( '[' . 'gallery ids="%s" columns="1" link="none" size="large"]', esc_attr($install_images[0]['ID']) );
        echo do_shortcode( $shortcode );
        
    }
}

/*---------------------------------------------------------*/


/*
*
* These were created for Mace Energy
*
*/

add_action( 'woocommerce_before_single_product' , 'gmg_woo_custom_description', 10 );
function gmg_woo_custom_description(){
    
    global $post;    
    $text = get_post_meta( $post->ID, 'product_short_description' );
    
    if( is_array( $text ) && $text[0] != '' ){
        
        remove_action( 'woocommerce_single_product_summary' , 'woocommerce_template_single_excerpt', 20 );
        add_action( 'woocommerce_single_product_summary' , 'gmg_woo_custom_show', 20 );
    }
}

function gmg_woo_custom_show(){
    
    global $post;
    
    echo '<div class="woocommerce-product-details__short-description">';
    echo get_post_meta( $post->ID, 'product_short_description' )[0];
    echo '</div>';
    
}

//add_action('genesis_before_site-inner_wrap', 'gmg_woo_menu_bar', 15 );
function gmg_woo_menu_bar(){
    
    if(class_exists('WooCommerce') ){
        
        if( is_product_category() || is_shop() ){
            
            if( is_shop() ){
                
                ?>

                <div class="menu-products-container">
                    <ul id="menu-products" class="menu">
                        
                        <li id="menu-item-000" class="menu-item menu-item-type-taxonomy menu-item-object-product current-menu-item menu-item-000"><a href="/products/" aria-current="page" itemprop="url" class="product-link-active">All</a></li>
                        <li id="menu-item-33" class="menu-item menu-item-type-taxonomy menu-item-object-product-type menu-item-33"><a href="/product-category/stoves/" itemprop="url">Stoves</a></li>
                        <li id="menu-item-78" class="menu-item menu-item-type-taxonomy menu-item-object-product-type menu-item-78"><a href="/product-category/inserts/" itemprop="url">Inserts</a></li>
                        <li id="menu-item-141" class="menu-item menu-item-type-taxonomy menu-item-object-product-type menu-item menu-item-141"><a href="/product-category/fireplaces/" aria-current="page" itemprop="url" class="product-link-active">Fireplaces</a></li>                        
                        <li id="menu-item-43" class="menu-item menu-item-type-taxonomy menu-item-object-product-type menu-item-43"><a href="/product-category/outdoors/" itemprop="url">Outdoors</a></li>
                    
                    </ul>
                </div>

                <?php
                
            } else {
            
                $cate = get_queried_object();
                $catID = $cate->term_id;
                $cat_parent = get_ancestors( $catID, 'product_cat', 'taxonomy' );
                $cat_children = get_term_children( $catID, 'product_cat' );

                if( empty( $cat_parent ) && !empty( $cat_children ) ){

    //                echo var_dump( $cat_children );

                    $parent_term = get_term_by( 'id', $catID, 'product_cat' );

                    ?>

                    <div class="menu-products-container">
                        <ul id="menu-products" class="menu">

                            <li id="menu-item-000" class="menu-item menu-item-type-taxonomy menu-item-object-product menu-item menu-item-000"><a href="/products/" aria-current="page" itemprop="url" class="product-link-active">All</a></li>
                            <li id="menu-item-<?php echo $catID; ?>" class="menu-item menu-item-type-taxonomy menu-item-object-product-type current-menu-item menu-item-<?php echo $catID; ?>"><a href="<?php echo get_term_link( $catID, 'product_cat' ); ?>" aria-current="page" itemprop="url" class="product-link-active"><?php echo $parent_term->name; ?></a></li>

                            <?php $cats_array = array(); ?>

                            <?php foreach( $cat_children as $cat_child ): ?>

                                <?php $cats_array[ get_term_meta( $cat_child, 'order' )[0] ] = $cat_child; ?>

                            <?php endforeach; ?>

                            <?php ksort( $cats_array ); ?>

                            <?php foreach( $cats_array as $cat_child ): ?>

                                <?php $term = get_term_by( 'id', $cat_child, 'product_cat' ); ?>

                                <li id="menu-item-<?php echo $cat_child; ?>" class="menu-item menu-item-type-taxonomy menu-item-object-product-type menu-item menu-item-<?php echo $cat_child; ?>"><a href="<?php echo get_term_link( $cat_child, 'product_cat' ); ?>" aria-current="page" itemprop="url" class="product-link-active"><?php echo $term->name; ?></a></li>

                            <?php endforeach; ?>

                        </ul>
                    </div>

                        <?php

                /*} elseif( is_product_category('wood-fireplaces') ){

                    ?>

                    <div class="menu-products-container">
                        <ul id="menu-products" class="menu">

                            <li id="menu-item-000" class="menu-item menu-item-type-taxonomy menu-item-object-product menu-item menu-item-000"><a href="/products/" aria-current="page" itemprop="url" class="product-link-active">All</a></li>
                            <li id="menu-item-33" class="menu-item menu-item-type-taxonomy menu-item-object-product-type menu-item-33"><a href="/product-category/stoves/" itemprop="url">Stoves</a></li>
                            <li id="menu-item-78" class="menu-item menu-item-type-taxonomy menu-item-object-product-type menu-item-78"><a href="/product-category/inserts/" itemprop="url">Inserts</a></li>
                            <li id="menu-item-141" class="menu-item menu-item-type-taxonomy menu-item-object-product-type current-menu-item menu-item-141"><a href="/product-category/fireplaces/" aria-current="page" itemprop="url" class="product-link-active">Fireplaces</a></li>
                            <li id="menu-item-43" class="menu-item menu-item-type-taxonomy menu-item-object-product-type menu-item-43"><a href="/product-category/outdoors/" itemprop="url">Outdoors</a></li>

                        </ul>
                    </div>

                    <?php
                    */
                } else {

                    $parent_term = get_term_by( 'id', $cat_parent[0], 'product_cat' );
                    $cat_children = get_term_children( $cat_parent[0], 'product_cat' );

                    ?>

                    <div class="menu-products-container">
                        <ul id="menu-products" class="menu">

                            <li id="menu-item-000" class="menu-item menu-item-type-taxonomy menu-item-object-product menu-item menu-item-000"><a href="/products/" aria-current="page" itemprop="url" class="product-link-active">All</a></li>

                            <li id="menu-item-<?php echo $catID; ?>" class="menu-item menu-item-type-taxonomy menu-item-object-product-type menu-item menu-item-<?php echo $catID; ?>"><a href="<?php echo get_term_link( $cat_parent[0], 'product_cat' ); ?>" aria-current="page" itemprop="url" class="product-link-active"><?php echo $parent_term->name; ?></a></li>

                            <?php foreach( $cat_children as $cat_child ): ?>

                                <?php $cats_array[ get_term_meta( $cat_child, 'order' )[0] ] = $cat_child; ?>

                            <?php endforeach; ?>

                            <?php ksort( $cats_array ); ?>

                            <?php foreach( $cats_array as $cat_child ): ?>

                                <?php $term = get_term_by( 'id', $cat_child, 'product_cat' ); ?>

                                <?php if( strcasecmp( $term->name , $cate->name ) == 0 ): ?>

                                    <?php $class = 'current-menu-item'; ?>

                                <?php else: ?>

                                    <?php $class = 'menu-item'; ?>

                                <?php endif; ?>

                                    <li id="menu-item-<?php echo $cat_child; ?>" class="menu-item menu-item-type-taxonomy menu-item-object-product <?php echo $class; ?> menu-item-<?php echo $cat_child; ?>"><a href="<?php echo get_term_link( $cat_child, 'product_cat' ); ?>" aria-current="page" itemprop="url" class="product-link-active"><?php echo $term->name; ?></a></li>

                            <?php endforeach; ?>

                        </ul>
                    </div>

                    <?php
                }
            }
        }
    }
}

/*---------------------------------------------------------*/