<?php

/**
 * GMG WooCommerce Request Info
 *
 * @package           gmg-woocommerce-request-info
 * @author            Greg Cowell & Raymond M Rose
 * @license           GPL-2.0+
 * @link              http://goodgroupllc.com/
 * @copyright         2017 Good Marketing Group
 *
 * @wordpress-plugin
 * Plugin Name:       GMG WooCommerce Request Info
 * Plugin URI:        http://goodgroupllc.com/
 * Description:       Turns the 'Add to Cart Button' into a 'Request for Info,' then opens a form when the button is pressed.
 * Version:           1.0.2
 * Author:            Greg Cowell & Raymond M Rose
 * Author URI:        http://goodgroupllc.com/
 * Text Domain:       gmg-slider
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */


//Create paths to plug-in
if ( ! defined( 'GMG_BASE_FILE' ) )
    define( 'GMG_BASE_FILE', __FILE__ );
if ( ! defined( 'GMG_BASE_DIR' ) )
    define( 'GMG_BASE_DIR', dirname( GMG_BASE_FILE ) );
if ( ! defined( 'GMG_PLUGIN_URL' ) )
    define( 'GMG_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

add_action( 'wp_enqueue_scripts', 'my_scripts_loader' );
function my_scripts_loader() {
    
    wp_enqueue_script('requester', GMG_PLUGIN_URL . '/assets/contact-form.js', array(
                      'jquery'), true, true );
    wp_localize_script( 'requester', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    
    wp_enqueue_script('flexslider', 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.2/jquery.flexslider.js', array(
                      'jquery'), true, true );
    
    wp_enqueue_style( 'flexslider-css' , 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.2/flexslider.css' );
}

//replace add to cart with view product
add_filter( 'woocommerce_loop_add_to_cart_link', 'add_product_link' );
function add_product_link( $link ) {
    global $product;
    echo '<a href="'.$product->get_permalink( $product->get_id() ).'" class="button">' . __('View Product', 'woocommerce') . '</a>';
}

//add_filter( 'woocommerce_simple_add_to_cart', 'add_product_link' );
//function add_product_link( $link ) {
//    global $product;
//    echo '<a href="'.$product->get_permalink( $product->id ).'" class="button">' . __('View Product', 'woocommerce') . '</a>';
//}

add_action('woocommerce_simple_add_to_cart','gmg_custom_update_contact_form');
function gmg_custom_update_contact_form(){
    
    $product = wc_get_product();
//    $button_text = 'Check Availability';
    
//    echo '<button id="order_form_button" class="button alt">' . $button_text . '</button>';
    gmgGetNewForm( $product );
//    echo do_shortcode( '[request-quote]' );
    
    remove_action('woocommerce_simple_add_to_cart','woocommerce_simple_add_to_cart', 30);
}

function gmgGetNewForm( $product ){
    
    ?>

    <div class="zip-code-finder" id="zip_code_finder">
        
        <label>Please Enter Your Zip Code to Check Availibility:</label>
        
        <input type="text" name="zip-code" id="zip_code_input" value="" class="zip-code-checker" aria-required="true" aria-invalid="false">
        <br>
        <button id="zip_code_checker" class="btn btn-primary" type="submit" value="submit">Check Availibility</button>
        
    </div>

    <div class="zip-code-response" id="zip_code_response">
        
        <h3>Unfortunately, you are outside our service area.</h3>
        
        <p>We would be more than happy to speak to you about the products that you are interested in so please call us.</p>
        
    </div>
    
    <?php
    
    echo '<div class="shop_contact" id="shop_contact">';
    
    echo do_shortcode( '[contact-form-7 id="31379" title="Product Request a Quote Form"]' );
    
    echo '</div>';
}

//add_action( 'woocommerce_shortcode_before_featured_products_loop', 'gmg_woo_remove_featured_products_links' );
function gmg_woo_remove_featured_products_links(){
    
    remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
    remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 10 );
    remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
    remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);
    
    add_action( 'woocommerce_after_shop_loop_item', 'gmg_woo_add_featured_product_links' );
 
}

function gmg_woo_add_featured_product_links(){
    
    global $product;
    
    $install_image = get_field( 'product_installation_featured_image' );
    
    if( is_array( $install_image ) ){
        
        echo '<figure class="wp-block-image size-large same-height"><img src="' . $install_image['url'] . '" alt="" class="wp-image-' . $product->get_image_id() . '"></figure>';
        
    } else {
    
        $image = wp_get_attachment_image_src( $product->get_image_id(), 'full' );

        echo '<figure class="wp-block-image size-large same-height"><img src="' . $image[0] . '" alt="" class="wp-image-' . $product->get_image_id() . '"></figure>';
        
    }
    
    echo '<h3>' . $product->get_name() . '</h3>';
    
    echo '<div class="featured-products-block">';
    
    echo '<p class="is-underlined dark-grey">';
    
    echo '<a href="'.$product->get_permalink( $product->get_id() ).'">' . __('View Product Pricing >', 'woocommerce') . '</a>';
    
    echo '</p>';
    
//    echo var_dump( $product->get_category_ids() );
    
    foreach( $product->get_category_ids() as $each_cat_id ){
        
//        echo $each_cat_id;
        
        $each_cat = get_term_by('id', $each_cat_id, 'product_cat' );
        
        if( is_object( $each_cat ) ){
            
            echo '<p class="has-small-font-size red not-underlined"><span style="color: #e90000;" class="ugb-highlight"><strong>';
            
            echo '<a href="'.get_category_link( $each_cat ).'">' . __('> View More ' . $each_cat->name, 'woocommerce') . '</a>';
            
            echo '</strong></span></p>';
            
        }
        
    }
    
    echo '</div>';
}

//add_action( 'woocommerce_before_single_product', 'gmg_woo_removed_product_price_and_image' );
function gmg_woo_removed_product_price_and_image(){
    
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
    add_action( 'woocommerce_single_product_summary' , 'gmg_woo_price', 10 );
    
    $install_images = get_field( 'product_installation_images_gallery' );
    
    if( is_array( $install_images) && count( $install_images ) >= 1 ){
        
        remove_action( 'woocommerce_before_single_product_summary' , 'woocommerce_show_product_images', 20 );
        add_action( 'woocommerce_before_single_product_summary' , 'gmg_show_install_images', 20 );
    }
    
}

function gmg_show_install_images(){
    
    //get the images
    $install_images = get_field('product_installation_images_gallery');
    
    echo '<div class="entry-content product-installation-images">';
				
	echo '<h2>Installation Images</h2>';
    
     //If there are more than one image, then we  want a slideshow and we need to call those images
    if( count( $install_images ) > 1 ){

    ?>
        <div id="slider" class="flexslider">
            <ul class="slides">
                <?php foreach( $install_images as $image ): ?>
                    <li>
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php
        
    } else {
        
        $shortcode = sprintf( '[' . 'gallery ids="%s" columns="1" link="none" size="large"]', esc_attr($install_images[0]['ID']) );
        echo do_shortcode( $shortcode );
        
    }    
    
	echo '<h2>Manufacturer Images</h2>';
    
    woocommerce_show_product_images();
    
    echo '</div>';
    
}

function gmg_woo_price(){
    
    global $product;
    
    $start_price = get_post_meta( $product->get_id(), '_start_price')[0];
    $end_price = get_post_meta( $product->get_id(), '_end_price')[0];
    
    //If, at the very least, you have the start price, then go aheand and let them in.
    if( $start_price ){
        
//        $fmt = new NumberFormatter( 'en_US', NumberFormatter::CURRENCY );
        
        echo '<div class="woo-price-box">';
    
        //If you only have the start price, show specific text.
        if( $start_price && !$end_price ){
            
            echo '<h3 class="woo-price">Starting at $' . number_format($start_price ) . '</h3>';

        //However, if you have both, show it this way.    
        }elseif( $start_price && $end_price ){
            
            echo '<h3 class="woo-price">Starting at $' . number_format($start_price) . ' - $' . number_format($end_price ) . '</h3>';

    //    if( $product->get_price() ){
    //    if( is_array( $price ) && !empty( $price ) ){
            
        }
        
        echo '<ul>';
        echo '<li>Price includes installation within 30 miles of Bowie, MD. <a href="#zip_code_finder">Click here</a> to find out if you are outside of this area.</li>';
        echo '</ul>';

        echo '</div>';
    }
}