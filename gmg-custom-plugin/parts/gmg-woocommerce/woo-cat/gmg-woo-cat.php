<?php

add_action('acf/init', 'my_acf_gmg_woo_init');
function my_acf_gmg_woo_init() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// register a gmg-form block
		acf_register_block(array(
			'name'				=> 'gmg-woo-cat',
			'title'				=> __('GMG Woo Cat'),
			'description'		=> __('A block for showing WooCommerce Category Products.'),
			'render_callback'	=> 'gmg_woo_cat_acf_block_render_callback',
			'category'			=> 'formatting',
			'icon'				=> 'editor-table',
			'keywords'			=> array( 'GMG', 'Woo Cat' ),
		));
	}
}

function gmg_woo_cat_acf_block_render_callback( $block ) {
	
	// convert name ("acf/gmg-form") into path friendly slug ("gmg-form")
	$slug = str_replace('acf/', '', $block['name']);
	
	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/block/content-{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/block/content-{$slug}.php") );
	}
}