jQuery(document).ready(function ($) {
    "use strict";
    var form = $('#gmg-contact-form');
//    var button = $('#webdev_form_button');
//    form.change( function(e) {
//        alert( 'Selected!' );
//        e.preventDefault();
//    });
    form.on('submit', function (e) {
//        alert( 'Selected!' );
        e.preventDefault();
        
        var form_addressee = $("#webdev_form_addressee" ).val();
        var form_subject = $("#webdev_form_addressee  option:selected").text();
//        console.log( form_addressee );
        var form_name = $("#webdev_form_name").val();
        var form_last_name = $("#webdev_form_last_name").val();
        var form_company_name = $("#webdev_form_company_name").val();
        var form_email = $("#webdev_form_email").val();
        var form_zip = $("#webdev_form_zip").val();
        var form_number = $("#webdev_form_number").val();
        var form_make = $("#webdev_form_make").val();
        var form_message = $("#webdev_form_message").val();
        var form_title = $("#webdev_form_title").val();
//        var form_captcha = $(".g-recaptcha").val();
//        console.log( form_captcha );
        
        var selecteddays = [];
        var selected;
        
        $(".days:checked").each(function() {
            selecteddays.push($(this).val());
        });
        
        if ( selecteddays ){
            selected = selecteddays.join(',') ;
        }
        
//        console.log( 'Selected Days are ' + selected );
        
        $.ajax({
            url : ajax_objecty.ajaxurl,
            type : 'post',
            data : {
                action : 'gmg_contact_mail',
                addressee : form_addressee,
                subject : form_subject,
                name : form_name,
                last : form_last_name,
                company : form_company_name,
                days : selected,
                zip : form_zip,
                email : form_email,
                number : form_number,
                title: form_title,
                make : form_make,
                message : form_message,
                captcha : grecaptcha.getResponse()
            },
            success:function( response ){
                if(response['foo']){
//                    alert( 'Success' + response['foo']);
                    $('#webdev_form_response').html( response['foo'] );
                    $('#webdev_form_response').css("visibility","visible");
                    window.location.href = "/thank-you/";
                } else{
//                    alert( 'Problems' + response['errors']);
                    $('#webdev_form_response').html( response['errors'] );
                    $('#webdev_form_response').css("visibility","visible");
                }
                
            },
            error: function(errorThrown){
                alert('Error' + errorThrown);
            } 
//            success : function( response ) {
//                console.log(response);
//                $('.gmg_contents').html(response);
//            }
        });       
	});
});
