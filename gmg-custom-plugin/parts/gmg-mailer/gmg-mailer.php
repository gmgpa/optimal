<?php

/**
 * GMG Mailer
 *
 * @package           gmg-mailer
 * @author            Greg Cowell & Raymond M Rose
 * @license           GPL-2.0+
 * @link              http://goodgroupllc.com/
 * @copyright         2017 Good Marketing Group
 *
 * @wordpress-plugin
 * Plugin Name:       GMG Mailer
 * Plugin URI:        http://goodgroupllc.com/
 * Description:       Creates A Receipient Custom Post Type, custom templates with forms, and allows specific forms to go to specific people.
 * Version:           1.0.2
 * Author:            Greg Cowell & Raymond M Rose
 * Author URI:        http://goodgroupllc.com/
 * Text Domain:       gmg-mailer
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

// Move all "advanced" metaboxes above the default editor
add_action('edit_form_after_title', function() {
	global $post, $wp_meta_boxes;
	do_meta_boxes(get_current_screen(), 'advanced', $post);
	unset($wp_meta_boxes[get_post_type($post)]['advanced']);
});

//Add Custom Post Type For Recipients
function recipients_init() {
	$labels = array(
		'name'               => _x( 'Recipients', 'post type general name', 'gmg_c121' ),
		'singular_name'      => _x( 'Recipient', 'post type singular name', 'gmg_c121' ),
		'menu_name'          => _x( 'Recipients', 'admin menu', 'gmg_c121' ),
		'name_admin_bar'     => _x( 'Recipient', 'add new on admin bar', 'gmg_c121' ),
		'add_new'            => _x( 'Add New', 'recipient', 'gmg_c121' ),
		'add_new_item'       => __( 'Add New Recipient', 'gmg_c121' ),
		'new_item'           => __( 'New Recipient', 'gmg_c121' ),
		'edit_item'          => __( 'Edit Recipient', 'gmg_c121' ),
		'view_item'          => __( 'View Recipient', 'gmg_c121' ),
		'all_items'          => __( 'All Recipients', 'gmg_c121' ),
		'search_items'       => __( 'Search Recipients', 'gmg_c121' ),
		'parent_item_colon'  => __( 'Parent Recipients:', 'gmg_c121' ),
		'not_found'          => __( 'No recipients found.', 'gmg_c121' ),
		'not_found_in_trash' => __( 'No recipients found in Trash.', 'gmg_c121' )
	);

	$args = array(
		'label'                 => __( 'Recipients', 'gmg_c121' ),
		'description'           => __( 'Custom Post Type for Recipients', 'gmg_c121' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'page-attributes'),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => 'gmg-custom-plugin',
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-awards',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'recipients', $args );

}
add_action( 'init', 'recipients_init' );

// add order column to admin table list of posts
function gmg_mailer_add_new_post_column($cpt_columns) {
	$cpt_columns['menu_order'] = "Order";
	return $cpt_columns;
}
add_action('manage_recipients_posts_columns', 'gmg_mailer_add_new_post_column');

//Show custom order column values for Recipients
function gmg_mailer_show_order_column($name){
	global $post;

	switch ($name) {
		case 'menu_order':
			$order = $post->menu_order;
			echo $order;
			break;
		default:
			break;
	}
}
add_action('manage_recipients_posts_custom_column','gmg_mailer_show_order_column');


//Make Column Sortable for Recipients
function gmg_mailer_order_column_register_sortable($columns){
	$columns['menu_order'] = 'menu_order';
	return $columns;
}
add_filter('manage_edit-recipients_sortable_columns','gmg_mailer_order_column_register_sortable');


/* Fire our meta box setup function on the recipients editor screen. */
add_action( 'init', 'gmg_recipients_meta_boxes_setup' );

function gmg_recipients_meta_boxes_setup(){

	/* Add meta boxes on the 'add_meta_boxes' hook. */
	add_action( 'add_meta_boxes', 'gmg_mailer_add_recipient_meta_boxes' );
	/* Save post meta on the 'save_post' hook. */
	add_action( 'save_post', 'gmg_save_recipient_name_meta', 10, 2);
	add_action( 'save_post', 'gmg_save_recipient_email_meta', 10, 2);
}

/* Create Recipients Custom Meta Boxes */
function gmg_mailer_add_recipient_meta_boxes(){

	add_meta_box(
		'recipient-name',      // Unique ID
		esc_html__( 'Recipient Name', 'outreach' ),    // Title
		'gmg_recipient_name_meta_box',   // Callback function
		'Recipients',         // Admin page (or post type)
		'advanced',         // Context
		'default'         // Priority
	);

	add_meta_box(
		'recipient-email',      // Unique ID
		esc_html__( 'Recipient Email Address', 'outreach' ),    // Title
		'gmg_recipient_email_meta_box',   // Callback function
		'Recipients',         // Admin page (or post type)
		'advanced',         // Context
		'default'         // Priority
	);
}

/* Display the page recipient meta box. */
function gmg_recipient_name_meta_box( $object, $box ) { ?>

    <?php wp_nonce_field( basename( __FILE__ ), 'gmg_recipient_name_nonce' ); ?>

    <p>
        <label for="recipient-name"><?php _e( "Recipient Name:", 'Outreach Pro' ); ?></label>
        <br />
        <input class="widefat" type="text" name="recipient-name" id="recipient-name" value="<?php echo esc_attr( get_post_meta( $object->ID, 'recipient_name', true ) ); ?>" size="30" />
    </p>
    <?php }

/* Display the recipient name meta box. */
function gmg_recipient_email_meta_box( $object, $box ) { ?>

    <?php wp_nonce_field( basename( __FILE__ ), 'gmg_recipient_email_nonce' ); ?>

    <p>
        <label for="recipient-email"><?php _e( "Recipient Email:", 'Outreach Pro' ); ?></label>
        <br />
        <input class="widefat" type="text" name="recipient-email" id="recipient-email" value="<?php echo esc_attr( get_post_meta( $object->ID, 'recipient_email', true ) ); ?>" size="30" />
    </p>
    <?php }


/* Save the meta box's post metadata. */
function gmg_save_recipient_name_meta( $post_id, $post ) {

	/* Verify the nonce before proceeding. */
	if ( !isset( $_POST['gmg_recipient_name_nonce'] ) || !wp_verify_nonce( $_POST['gmg_recipient_name_nonce'], basename( __FILE__ ) ) )
		return $post_id;

	/* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );

	/* Check if the current user has permission to edit the post. */
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;

	/* Get the posted data and sanitize it for use as an HTML class. */
	$new_meta_value = ( isset( $_POST['recipient-name'] ) ? sanitize_text_field( $_POST['recipient-name'] ) : '' );

	/* Get the meta key. */
	$meta_key = 'recipient_name';

	/* Get the meta value of the custom field key. */
	$meta_value = get_post_meta( $post_id, $meta_key, true );

	/* If a new meta value was added and there was no previous value, add it. */
	if ( $new_meta_value && '' == $meta_value )
		add_post_meta( $post_id, $meta_key, $new_meta_value, true );

	/* If the new meta value does not match the old value, update it. */
	elseif ( $new_meta_value && $new_meta_value != $meta_value )
		update_post_meta( $post_id, $meta_key, $new_meta_value );

	/* If there is no new meta value but an old value exists, delete it. */
	elseif ( '' == $new_meta_value && $meta_value )
		delete_post_meta( $post_id, $meta_key, $meta_value );
}


/* Save the meta box's post metadata. */
function gmg_save_recipient_email_meta( $post_id, $post ) {

	/* Verify the nonce before proceeding. */
	if ( !isset( $_POST['gmg_recipient_email_nonce'] ) || !wp_verify_nonce( $_POST['gmg_recipient_email_nonce'], basename( __FILE__ ) ) )
		return $post_id;

	/* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );

	/* Check if the current user has permission to edit the post. */
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;

	/* Get the posted data and sanitize it for use as an HTML class. */
	$new_meta_value = ( isset( $_POST['recipient-email'] ) ? sanitize_text_field( $_POST['recipient-email'] ) : '' );

	/* Get the meta key. */
	$meta_key = 'recipient_email';

	/* Get the meta value of the custom field key. */
	$meta_value = get_post_meta( $post_id, $meta_key, true );

	/* If a new meta value was added and there was no previous value, add it. */
	if ( $new_meta_value && '' == $meta_value )
		add_post_meta( $post_id, $meta_key, $new_meta_value, true );

	/* If the new meta value does not match the old value, update it. */
	elseif ( $new_meta_value && $new_meta_value != $meta_value )
		update_post_meta( $post_id, $meta_key, $new_meta_value );

	/* If there is no new meta value but an old value exists, delete it. */
	elseif ( '' == $new_meta_value && $meta_value )
		delete_post_meta( $post_id, $meta_key, $meta_value );
}

//Change category to Receives Mail From:
add_action('add_meta_boxes_recipients','add_recipients_meta');
function add_recipients_meta(){
		remove_meta_box( 'categorydiv', 'recipients', 'side' );
		add_meta_box('categorydiv', __('Receives Mail From: '), 'post_categories_meta_box', 'recipients', 'normal', 'high');
}

function gmg_recs_tester(){
    
    echo 'Inside Tester!</br>';
    
    echo '<form action="" method="post">';

    echo '<label for="webdev_form_addressee">Recipient: </label>';

    echo '<select name="webdev_form_addressee">';

    //Get Names and Email Addresses for Connect to St. Paul's form
    $recipient_connect = get_posts(array(
        'post_type'   => 'recipients',
        'post_status' => 'publish',
        'posts_per_page' => -1,
//        'fields' => 'ids',
        //Category with the ID of 1 is the generic contact form recipient category
//        'cat'=> '1',
        'orderby' => 'menu_order',
        'order' => 'ASC'
    )
    );
    
    echo var_dump($recipient_connect);

    //loop over each post
    foreach($recipient_connect as $recipient_post){
//        echo 'Inside!' . var_dump($recipient_connect);
    //get the meta you need form each post
        $recipient_name[] = get_post_meta($recipient_post->ID,"recipient_name", true);
//        echo 'Name: ' . var_dump($recipient_name) . '</br></br>';
        $recipient_email[] = get_post_meta($recipient_post->ID,"recipient_email", true);
//        echo 'Email: ' . var_dump($recipient_email) . '</br></br>';
        
        
        
        echo '<option value="' . end($recipient_email) .'">' . end($recipient_name) . '</option>';
    }
    
    echo '</select>';
    
    echo '</form>';

    
}

add_shortcode('recs', 'gmg_recs_tester' );

class Recipient{
    
    private $id;
    
    public function __construct( $id ) {
        $this->id = $id;
    }
    
    public function get_recipient_name(){
        return get_post_meta( $this->id, 'recipient_name' ,true );
    }
    
    public function get_recipient_email(){
        return get_post_meta( $this->id, 'recipient_email' ,true );
    }    
    
}

class Recipients{
    
    public function __construct() { }
    
    public function get_all_recipients(){
        
        $recipients = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'             => array( 'recipients' ),
            'posts_per_page'        => '-1',
            'orderby'               => 'menu_order',
            'order'                 => 'ASC',
            'post_status'            => 'publish',
            
        );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                
                array_push( $recipients,
                           array( 'name'    => get_post_meta( $id , "recipient_name" , true),
                                 'email'    => get_post_meta( $id , "recipient_email" , true)
                                )
                           );
                
//                array_push( $recipients, get_the_ID() );
            }
        }  else {
            // no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        if( count( $recipients ) > 0 ){
            
            return $recipients;
            
        } else{
            
            return false;
        }
        
    }
}