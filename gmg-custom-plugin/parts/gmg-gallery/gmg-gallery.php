<?php

$gals = plugin_dir_path( __FILE__ );

//Let's bring in Class Gallery and Galleries.
require_once($gals . '/includes/gmg-class-gallery.php');

//Let's bring in the custom post type Gallery, which is where we will store the info.
require_once($gals . '/includes/gmg-gallery-cpt.php');

//Let's also bring in the code to show the right templates.
require_once($gals . '/includes/gmg-gallery-loader.php');

//Let's also bring in the codes of short.
require_once($gals . '/includes/gmg-gallery-shortcodes.php');