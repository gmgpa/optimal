<?php

add_shortcode('project-cats', 'gmg_get_gallery_cats_for_shortcode');
function gmg_get_gallery_cats_for_shortcode(){
    
    $terms = wp_list_categories( array(
        'hide_empty' => false,
        'taxonomy' => 'gallery_category',
        'title_li' => '',
        'echo'      => false
    ) );
    
    return $terms;

}