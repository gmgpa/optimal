<?php

add_filter( 'template_include', 'gmg_gallery_categories_templates', 10 ); 
function gmg_gallery_categories_templates( $original_template ) {
    
//    error_log( 'PT: ' . get_post_type() . " and " . is_single() );
    
//    error_log( 'Gallery Template Loader!' );
    
    //First, check to see if this is an archive
    
    if( is_tax( 'gallery_category' ) ){
//    if( is_archive() ){
        
//        error_log( 'Inside Gallery Category Archive' );
        
        return plugin_dir_path( __FILE__ ) . '/templates/gallery_category.php';
    
    //It's not a promo so return the template.    
    } elseif( is_single() && get_post_type() == 'gallery' ) {
        
        return plugin_dir_path( __FILE__ ) . '/templates/single-gallery.php';
        
    } else {
        
        return $original_template;
    
    }
}