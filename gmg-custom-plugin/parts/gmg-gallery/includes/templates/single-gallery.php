<?php

//Force full width content layout.
add_filter( 'genesis_site_layout', '__genesis_return_content_sidebar' );

add_action('genesis_before_content_sidebar_wrap', 'gmg_gallery_remove_sidebars');

function gmg_gallery_remove_sidebars(){
    remove_action( 'genesis_sidebar', 'genesis_do_sidebar' ); //remove the default genesis sidebar
    remove_action( 'genesis_sidebar', 'gencwooc_ss_do_sidebar', 10 ); //remove the default genesis sidebar
    add_action( 'genesis_sidebar', 'gmg_gallery_do_cat_sidebar' ); //add an action hook to call the function for my custom sidebar
}

function gmg_gallery_do_cat_sidebar() {
    dynamic_sidebar( 'gallery-category-pages-sidebar' );
}

// Run the Genesis loop.
genesis();