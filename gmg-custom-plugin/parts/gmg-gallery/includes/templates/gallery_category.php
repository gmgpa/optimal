<?php

//Force full width content layout.
add_filter( 'genesis_site_layout', '__genesis_return_content_sidebar' );

add_action('genesis_before_content_sidebar_wrap', 'gmg_gallery_cats_remove_sidebars');

function gmg_gallery_cats_remove_sidebars(){
    remove_action( 'genesis_sidebar', 'genesis_do_sidebar' ); //remove the default genesis sidebar
    remove_action( 'genesis_sidebar', 'gencwooc_ss_do_sidebar' ); //remove the default genesis sidebar
    add_action( 'genesis_sidebar', 'gmg_gallery_do_cat_page_sidebar' ); //add an action hook to call the function for my custom sidebar
    
}

function gmg_gallery_do_cat_page_sidebar() {
    dynamic_sidebar( 'gallery-category-pages-sidebar' ); 
}

remove_action ('genesis_loop', 'genesis_do_loop'); // Remove the standard loop
add_action( 'genesis_loop', 'gmg_gallery_category_loop' ); // Add custom loop

function gmg_gallery_category_loop(){
    
    $current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $display = get_field('display_type',$current_term->taxonomy.'_'.$current_term->term_id);
    
//    echo $display;
    
    if( $display == 'cats' ){
        
//        echo 'Gettings Sub Categories!';
        
        $cat_gallery = new Cat_Gallery( $current_term->term_id );
        echo implode( '' , $cat_gallery->print_cat_children() );
        
        
    } else {
        
        $galleries = new Galleries();
        echo implode( '' , $galleries->print_cat_galleries( $current_term->term_id ) );
        
//        echo 'Gettings Galleries!';
           
    }
    
}

// Run the Genesis loop.
genesis();