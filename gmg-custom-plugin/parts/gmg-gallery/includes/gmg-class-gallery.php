<?php

class Gallery{
    
    private $id;
    
    public function __construct( $id ) {
        
        $this->id = $id;
            
        }
    
    public function get_gallery_name(){
        
        return get_the_title( $this->id);
        
    }
    
    public function get_gallery_link(){
        
        return get_permalink( $this->id );
        
    }
    
    public function get_gallery_image( $size ){
        
        return get_the_post_thumbnail_url( $this->id, $size );
        
    }
    
    public function print_the_gallery(){
        
        $new_html_array = array();
        
        array_push( $new_html_array, '<div class="wp-block-column">' );

            array_push( $new_html_array, '<div id="' . $this->id . '" class="category-gallery-card">' );
        
                array_push( $new_html_array, '<a href="' . $this->get_gallery_link() . '">');

                    array_push( $new_html_array, '<div class="category-gallery-image-card">' );

                        array_push( $new_html_array, '<img src="' . $this->get_gallery_image( 'medium' ) . '"/>' );

                    array_push( $new_html_array, '</div>' );
        
                array_push( $new_html_array, '</a>');
        
                array_push( $new_html_array, '<div class="category-gallery-text-card">' );
        
                    array_push( $new_html_array, '<h2 class="entry-title" itemprop="headline">' . $this->get_gallery_name() . '</h2>' );

                array_push( $new_html_array, '</div>' );
        
                array_push( $new_html_array, '<div class="fpa-more-link"><a href="' . $this->get_gallery_link() . '" class="gmg-more-link button">Read More</a></div>');
        
            array_push( $new_html_array, '</div>' );

        array_push( $new_html_array, '</div>' );
        
        return implode( '' , $new_html_array );
    
    }
    
}


class Galleries{
    
    public function __construct( ) {
            
        }
    
    public function get_all_galleries(){
        
        $galleries = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'gallery' ),
            'posts_per_page'         => '-1',
            'post_status'            => 'publish',
            'orderby'                => 'menu_order',
            'order'                  => 'ASC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                
                $query->the_post();
                
//                error_log( 'Thumb is ' .  get_the_post_thumbnail_url(get_the_ID() , 'medium') );
                
                array_push( $galleries, 
                           array(
                               'title'  => get_the_title(),
                               'thumb'  => get_the_post_thumbnail_url(get_the_ID() , 'large'),
                               'link'   => get_permalink() )
                          );                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $galleries;       
        
    }
    
    public function get_all_galleries_for_category( $cat ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'gallery' ),
            'posts_per_page'         => '-1',
            'post_status'            => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'gallery_category',
                    'field'    => 'term_id',
                    'terms'    => $cat
                )
            ),            
            'orderby'                => 'menu_order',
            'order'                  => 'ASC' );
        
        return get_posts( $args );     
        
    }
    
    public function print_cat_galleries( $cat ){
        
        $kids = $this->get_all_galleries_for_category( $cat );
        
//        echo var_dump( $kids );
        
//        $kids = get_term_children( $this->term_id , 'gallery_category' );
        
        $new_html_array = array();
        
        $index = 0;
        
        foreach( $kids as $kid ){
            
            $gallery = new Gallery( $kid->ID );
            
            if( $index == 0 || $index % 3 == 0 ){
                
                array_push( $new_html_array, '<div class="wp-block-columns">' );
                array_push( $new_html_array, $gallery->print_the_gallery() );
                
            } elseif( $index % 4 == 0 ){
                
                array_push( $new_html_array, $gallery->print_the_gallery() );
                array_push( $new_html_array, '</div>');
                
            } else {
                
                array_push( $new_html_array, $gallery->print_the_gallery() );
            }
            
            $index++;
        
        }
        
        
        if( $index < 3 ){
            
            array_push( $new_html_array, '</div>');
            
//            echo var_dump( $new_html_array );
            
        }
        
        return $new_html_array;
    }
    
}

class Cat_Gallery{
    
    private $term_id;
    
    private $combined_id;
    
    private $term;
    
    public function __construct( $id ) {
        
        $this->term_id = $id;
        
        $this->combined_id = 'gallery_category_' . $id;
        
        $this->term = get_term_by( 'term_id', $id, 'gallery_category' );
            
        }
    
    public function get_cat_gallery_name(){
        
        return $this->term->name;
        
    }
    
    public function get_cat_gallery_link(){
        
        return get_term_link( $this->term );
        
    }
    
    public function get_cat_gallery_image( $size ){
        
//        error_log( 'Combo:' . $this->combined_id );
        
//        echo var_dump( get_field('catagory_image', $this->combined_id ) );
        
        return get_field('catagory_image', $this->combined_id )['sizes'][$size];
        
    }
    
    public function print_cat_gallery(){
        
        $new_html_array = array();
        
        array_push( $new_html_array, '<div class="wp-block-column">' );
        
            array_push( $new_html_array, '<div id="' . $this->term_id . '" class="category-gallery-card">' );
        
                array_push( $new_html_array, '<a href="' . $this->get_cat_gallery_link() . '">');

                    array_push( $new_html_array, '<div class="category-gallery-image-card">' );

                        array_push( $new_html_array, '<img src="' . $this->get_cat_gallery_image( 'medium' ) . '"/>' );

                    array_push( $new_html_array, '</div>' );
        
                array_push( $new_html_array, '</a>');

                array_push( $new_html_array, '<div class="category-gallery-text-card">' );

                    array_push( $new_html_array, '<h2 class="entry-title" itemprop="headline">' . $this->get_cat_gallery_name() . '</h2>' );

            /*        array_push( $new_html_array, '<img src="' . get_field('display_type',$current_term->taxonomy.'_'.$current_term->term_id);apply_filters('the_content', $post->post_content ) );*/

                array_push( $new_html_array, '</div>' );
        
                array_push( $new_html_array, '<div class="fpa-more-link"><a href="' . $this->get_cat_gallery_link() . '" class="gmg-more-link button">Read More</a></div>');

            array_push( $new_html_array, '</div>' );
        
        array_push( $new_html_array, '</div>' );
        
        return implode( '' , $new_html_array );
    
    }
    
    public function print_cat_children(){
        
        $kids = get_terms(
            array(
                'taxonomy' => 'gallery_category',
                'parent'   => $this->term_id,
                'hide_empty' => false,
                )
        );
        
//        echo var_dump( $kids );
        
//        $kids = get_term_children( $this->term_id , 'gallery_category' );
        
        $new_html_array = array();
        
        $index = 0;
        
        foreach( $kids as $kid ){
            
            $cat_gallery = new Cat_Gallery( $kid->term_id );
            
            if( $index == 0 || $index % 3 == 0 ){
                
                array_push( $new_html_array, '<div class="wp-block-columns">' );
                array_push( $new_html_array, $cat_gallery->print_cat_gallery() );
                
            } elseif( $index % 4 == 0 ){
                
                array_push( $new_html_array, $cat_gallery->print_cat_gallery() );
                array_push( $new_html_array, '</div>');
                
            } else {
                
                array_push( $new_html_array, $cat_gallery->print_cat_gallery() );
            }
            
            $index++;
        
        }
        
        
        if( $index < 3 ){
            
            array_push( $new_html_array, '</div>');
            
//            echo var_dump( $new_html_array );
            
        }
        
        return $new_html_array;
    }
}

class Cat_Galleries{
    
    public function __construct() {}
    
}
