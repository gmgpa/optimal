<?php

//Add Custom Post Type For Gallery
function gallery_init() {
	$labels = array(
		'name'               => _x( 'Gallery', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Gallery', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( '1:1 Gallery', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Gallery', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'gallery', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Gallery', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Gallery', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Gallery', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Gallery', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Galleries', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Galleries', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Gallery:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No gallery found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No gallery found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'public' => true,
		'labels'  => $labels,
		'has_archive' => true,
		'supports' => array('thumbnail', 'editor', 'title', 'genesis-cpt-archives-settings'),
        'taxonomies'    => array( 'gallery_category' ),
		'menu_icon' => 'dashicons-images-alt',
        'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
        'show_in_rest' => true,
		'query_var' => true,
		'publicly_queryable' => true,
		'capability_type'     => 'page'
	);
    
	register_post_type( 'gallery', $args );
    
    $cat_labels = array(
        'name' => _x( 'Gallery Categorys', 'taxonomy general name' ),
        'singular_name' => _x( 'Gallery Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Gallery Categorys' ),
        'all_items' => __( 'All Gallery Categorys' ),
        'parent_item' => __( 'Parent Gallery Category' ),
        'parent_item_colon' => __( 'Parent Gallery Category:' ),
        'edit_item' => __( 'Edit Gallery Category' ), 
        'update_item' => __( 'Update Gallery Category' ),
        'add_new_item' => __( 'Add New Gallery Category' ),
        'new_item_name' => __( 'New Gallery Category Name' ),
        'menu_name' => __( 'Gallery Categorys' ),
      ); 
    
    register_taxonomy(
        'gallery_category',  // The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'gallery',             // post type name
        array(
            'hierarchical' => true,
            'label' => 'Gallery Cats', // display name
            'query_var' => true,
//            'show_in_menu' => 'gmg-custom-plugin',
            'labels' => $cat_labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_rest'  => true,
            'rewrite' => array(
                'slug' => 'project-categories',    // This controls the base slug that will display before each term
//                'with_front' => false  // Don't display the category base before
            )
        )
    );
}
add_action( 'init', 'gallery_init' );