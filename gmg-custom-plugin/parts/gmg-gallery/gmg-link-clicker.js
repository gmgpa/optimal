    jQuery(document).ready(function ($) {
        
        "use strict";
        
        var link = $('.four-links');
        
        link.on('click', function (c) {
            c.preventDefault();
            
            var this_link = $(this);
            var id = this_link.attr( 'id' );
            
            link.each( function(){
                
                var this_specific_link  = $(this);
                if( this_specific_link.attr( 'id' ) != id ){
                    
//                    console.log( 'Id is ' + this_specific_link.attr( 'id' ) );
                    
                    this_specific_link.removeClass( 'live' );
                    
                } else {
                    
                    this_specific_link.addClass( 'live' );
                    
                }
            });            
            
//            alert( 'clicked on ' + id );
            
            var widgets = $('.widget');            
            widgets.each( function(){
                
                var this_widget = $(this);
                this_widget.removeClass( 'gone' );
                this_widget.removeClass( 'show' );
                
                if( id != 'all' ){
                    
                    if( this_widget.attr( 'id' ) != id ){

                        this_widget.addClass( 'gone' );
                        
                    } else {
                        
                        this_widget.addClass( 'show' );
                        
                    }
                    
                } else {
                    
                    this_widget.removeClass( 'gone' );
                    
                }
            } );

        });
        
    });