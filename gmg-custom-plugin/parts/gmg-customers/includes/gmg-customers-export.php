<?php

add_action( 'restrict_manage_posts', 'gmg_customers_add_export_button' );

function gmg_customers_add_export_button() {
    
//    error_log( 'Inside Export customers');
    
    $screen = get_current_screen();
    
    if (isset($screen->parent_file) && ('edit.php?post_type=customers' == $screen->parent_file)) {
            
//            error_log( 'The screen parent file is ' . $screen->parent_file );
            ?>
    <input type="submit" name="export_customers_posts" id="export_customers_posts" class="button button-primary" value="Export All customers">
    <script type="text/javascript">
        jQuery(function($) {
            $('#export_customers_posts').insertAfter('#post-query-submit');
        });

    </script>

    <?php
    }
}

//add_action('wp_ajax_gmg_export_all_customers', 'gmg_export_all_customers');
////add_action('wp_ajax_nopriv_gmg_contact_mail', 'gmg_contact_mail');
add_action( 'init', 'func_gmg_customers_export_all_customers' );
function func_gmg_customers_export_all_customers() {
    
	if(isset($_GET['export_customers_posts'])) {
        
		$arg = array(
				'post_type' => 'customers',
				'post_status' => 'publish',
				'posts_per_page' => -1,
			);

		global $post;
		$arr_post = get_posts($arg);
		if ($arr_post) {

			header('Content-type: text/csv');
			header('Content-Disposition: attachment; filename="customers.csv"');
			header('Pragma: no-cache');
			header('Expires: 0');

			$file = fopen('php://output', 'w');

			fputcsv($file, array('First', 'Last', 'Email'));

			foreach ($arr_post as $post) {
				setup_postdata($post);
				fputcsv( $file, array( get_field('customer_first_name'),
                                      get_field('customer_last_name'),
                                      get_field('customer_email') 
                                     ) 
                       );
			}

			exit();
		}
	}
}
