<?php

add_action( 'init', 'customers_init' );

//Add Custom Post Type For Customers
function customers_init() {
    $Customer_labels = array(
        'name'                  => _x( 'Customers', 'Post Type General Name', 'wellness_pro' ),
        'singular_name'         => _x( 'Customer', 'Post Type Singular Name', 'wellness_pro' ),
        'menu_name'             => __( 'Customers', 'wellness_pro' ),
        'name_admin_bar'        => __( 'Customers', 'wellness_pro' ),
        'archives'              => __( 'Customer Archives', 'wellness_pro' ),
        'attributes'            => __( 'Customer Attributes', 'wellness_pro' ),
        'parent_Customer_colon'     => __( 'Parent Customer:', 'wellness_pro' ),
        'all_Customers'             => __( 'All Customers', 'wellness_pro' ),
        'add_new_Customer'          => __( 'Add New Customer', 'wellness_pro' ),
        'add_new'               => __( 'Add New', 'wellness_pro' ),
        'new_Customer'              => __( 'New Customer', 'wellness_pro' ),
        'edit_Customer'             => __( 'Edit Customer', 'wellness_pro' ),
        'update_Customer'           => __( 'Update Customer', 'wellness_pro' ),
        'view_Customer'             => __( 'View Customer', 'wellness_pro' ),
        'view_Customers'            => __( 'View Customers', 'wellness_pro' ),
        'search_Customers'          => __( 'Search Customer', 'wellness_pro' ),
        'not_found'             => __( 'Not found', 'wellness_pro' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'wellness_pro' ),
        'featured_image'        => __( 'Featured Image', 'wellness_pro' ),
        'set_featured_image'    => __( 'Set featured image', 'wellness_pro' ),
        'remove_featured_image' => __( 'Remove featured image', 'wellness_pro' ),
        'use_featured_image'    => __( 'Use as featured image', 'wellness_pro' ),
        'insert_into_Customer'      => __( 'Insert into Customer', 'wellness_pro' ),
        'uploaded_to_this_Customer' => __( 'Uploaded to this Customer', 'wellness_pro' ),
        'Customers_list'            => __( 'Customers list', 'wellness_pro' ),
        'Customers_list_navigation' => __( 'Customers list navigation', 'wellness_pro' ),
        'filter_Customers_list'     => __( 'Filter Customers list', 'wellness_pro' ),
    );

    $Customer_args = array(
        'label'                 => __( 'Customer', 'wellness_pro' ),
        'description'           => __( 'Custom Post Type for Customers', 'wellness_pro' ),
        'labels'                => $Customer_labels,
        'supports'              => array( 'title' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'menu_icon'             => 'dashicons-smiley',
        'show_in_admin_bar'     => false,
        'show_in_nav_menus'     => false,
        'can_export'            => true,
        'has_archive'           => false,		
        'exclude_from_search'   => true,
        'publicly_queryable'    => false,
        'capability_type'       => 'page',
    );
    
    $current_user = wp_get_current_user();
    if ( user_can( $current_user, 'administrator' ) ) {
        
        $Customer_args['show_in_menu'] = 'gmg-custom-plugin';
//        $Customer_args['menu_position'] = 5;
    }
    
    register_post_type( 'Customers', $Customer_args );
    }

// add order column to admin table list of posts
function gmg_Customers_add_new_post_column($cpt_columns) {
    
    $updated_columns = array();
    foreach($cpt_columns as $key => $title){
//        error_log( 'The key is ' . $key);
        if( $key == 'title' ){
            $updated_columns[$key] = $title;
            $updated_columns['email'] = "Email";            
        } else {
            $updated_columns[$key] = $title;            
        }
    }
    
	return $updated_columns;
}
add_action('manage_Customers_posts_columns', 'gmg_Customers_add_new_post_column');

//Show custom order column values for Customers
function gmg_Customers_posts_show_order_column($name){
	global $post;

	switch ($name) {
		case 'email':
//            $email = get_post_meta( $post->ID, 'Customer_email', true );
            echo get_field('Customer_email');
//			$order = $post->menu_order;
//			echo $email;
			break;
		default:
			break;
	}
}
add_action('manage_Customers_posts_custom_column','gmg_Customers_posts_show_order_column');


//Make Column Sortable for Customers
function gmg_Customers_order_column_register_sortable($columns){
	$columns['email'] = 'email';
	return $columns;
}
add_filter('manage_edit-Customers_sortable_columns','gmg_Customers_order_column_register_sortable');
