<?php

//Add Custom Post Type For Reviews
function Reviews_init() {
	$rev_labels = array(
		'name'                  => _x( 'Reviews', 'Post Type General Name', 'wellness_pro' ),
		'singular_name'         => _x( 'Review', 'Post Type Singular Name', 'wellness_pro' ),
		'menu_name'             => __( 'Reviews', 'wellness_pro' ),
		'name_admin_bar'        => __( 'Reviews', 'wellness_pro' ),
		'archives'              => __( 'Review Archives', 'wellness_pro' ),
		'attributes'            => __( 'Review Attributes', 'wellness_pro' ),
		'parent_Review_colon'     => __( 'Parent Review:', 'wellness_pro' ),
		'all_Reviews'             => __( 'All Reviews', 'wellness_pro' ),
		'add_new_Review'          => __( 'Add New Review', 'wellness_pro' ),
		'add_new'               => __( 'Add New', 'wellness_pro' ),
		'new_Review'              => __( 'New Review', 'wellness_pro' ),
		'edit_Review'             => __( 'Edit Review', 'wellness_pro' ),
		'update_Review'           => __( 'Update Review', 'wellness_pro' ),
		'view_Review'             => __( 'View Review', 'wellness_pro' ),
		'view_Reviews'            => __( 'View Reviews', 'wellness_pro' ),
		'search_Reviews'          => __( 'Search Review', 'wellness_pro' ),
		'not_found'             => __( 'Not found', 'wellness_pro' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wellness_pro' ),
		'featured_image'        => __( 'Featured Image', 'wellness_pro' ),
		'set_featured_image'    => __( 'Set featured image', 'wellness_pro' ),
		'remove_featured_image' => __( 'Remove featured image', 'wellness_pro' ),
		'use_featured_image'    => __( 'Use as featured image', 'wellness_pro' ),
		'insert_into_Review'      => __( 'Insert into Review', 'wellness_pro' ),
		'uploaded_to_this_Review' => __( 'Uploaded to this Review', 'wellness_pro' ),
		'Reviews_list'            => __( 'Reviews list', 'wellness_pro' ),
		'Reviews_list_navigation' => __( 'Reviews list navigation', 'wellness_pro' ),
		'filter_Reviews_list'     => __( 'Filter Reviews list', 'wellness_pro' ),
	);
    
	$rev_args = array(
		'label'                 => __( 'Review', 'wellness_pro' ),
		'description'           => __( 'Custom Post Type for Reviews', 'wellness_pro' ),
		'labels'                => $rev_labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
//		'show_in_menu'          => 'gmg-contact-121',
//		'menu_position'         => 6,
		'menu_icon'             => 'dashicons-thumbs-up',
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'capability_type'       => 'page',
	);
    
    $current_user = wp_get_current_user();
    if ( user_can( $current_user, 'manage_options' ) ) {
        $rev_args['show_in_menu'] = 'gmg-custom-plugin';
        $rev_args['menu_position'] = 6;        
    }
    
	register_post_type( 'Reviews', $rev_args );

}
add_action( 'init', 'Reviews_init' );


// add order column to admin table list of posts
function gmg_Reviews_add_new_post_column($cpt_columns) {
	$cpt_columns['menu_order'] = "Order";
	return $cpt_columns;
}
add_action('manage_Reviews_posts_columns', 'gmg_Reviews_add_new_post_column');

//Show custom order column values for Reviews
function gmg_reviews_posts_show_order_column($name){
	global $post;

	switch ($name) {
		case 'menu_order':
			$order = $post->menu_order;
			echo $order;
			break;
		default:
			break;
	}
}
add_action('manage_Reviews_posts_custom_column','gmg_reviews_posts_show_order_column');


//Make Column Sortable for Reviews
function gmg_Reviews_order_column_register_sortable($columns){
	$columns['menu_order'] = 'menu_order';
	return $columns;
}
add_filter('manage_edit-Reviews_sortable_columns','gmg_Reviews_order_column_register_sortable');
