<?php

add_shortcode( 'review-sidebar' , 'gmg_get_review_sidebar' );

function gmg_get_review_sidebar(){
    
    $html_array = array();
    
    array_push( $html_array, '<div class="review-form">' );
    array_push( $html_array, '<form id="gmg-review-form" action="" method="post">');
    
    array_push( $html_array, '<div class="review-form-box">');
    
    array_push( $html_array, '<input type="hidden" name="g-recaptcha-response" id="CaptureRecaptcha" value="' . get_field( 'recaptcha_site', 'overall_settings' ) . '" >');
    array_push( $html_array, '<label for="webdev_form_fname">*First Name: </label>');
    array_push( $html_array, '<input class="form-control" type="text" name="webdev_form_fname" placeholder="First Name" id="webdev_form_fname" value="">');
    array_push( $html_array, '</div>');

    array_push( $html_array, '<div class="review-form-box">');
    array_push( $html_array, '<label for="webdev_form_lname">*Last Name: </label>');
    array_push( $html_array, '<input class="form-control" type="text" name="webdev_form_lname" placeholder="Last Name" id="webdev_form_lname" value="">');
    array_push( $html_array, '</div>');
    
    array_push( $html_array, '<div class="review-form-box">');
    array_push( $html_array, '<label for="webdev_form_email">*Email: </label>');
    array_push( $html_array, '<input class="form-control" type="text" name="webdev_form_email" placeholder="Email Address" id="webdev_form_email" value="">');
    array_push( $html_array, '</div>');
    
    array_push( $html_array, '<div class="review-form-box">');
    array_push( $html_array, '<label for="webdev_form_town">Town: </label>');
    array_push( $html_array, '<input class="form-control" type="text" name="webdev_form_town" placeholder="Town" id="webdev_form_town">');
    array_push( $html_array, '</div>');
    
    array_push( $html_array, '<div class="review-form-box">');
    array_push( $html_array, '<label for="webdev_form_category">Subject: </label>');
//    array_push( $html_array, '<select name="webdev_form_category" id="webdev_form_category">');
    
    $category = get_category_by_slug( 'reviews' );
    
    $gmg_categories = get_term_children( $category->term_id, 'category' );

    if( $gmg_categories ){

        foreach( $gmg_categories as $gmg_category ){

            array_push( $html_array, '<input type="radio" name="review-category" id="review-category" value="' . get_term( $gmg_category, 'category' )->term_id .'" /> ' . get_term( $gmg_category, 'category' )->name . '<br />' );
        }
    }
    
//    array_push( $html_array, '</select>');
    array_push( $html_array, '</div>');
    
    array_push( $html_array, '<div class="review-form-box">');
    array_push( $html_array, '<label for="webdev_form_stars">How Many Stars Would You Rate Us? </label><br />' );
    array_push( $html_array, '<i class="fa fa-star" aria-hidden="true"></i><input type="radio" class="stars" name="webdev_form_stars" id="webdev_form_stars_1" value="1"><br />' );
    array_push( $html_array, '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><input type="radio" class="stars" name="webdev_form_stars" id="webdev_form_stars_2" value="2"><br />' );
    array_push( $html_array, '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><input type="radio" class="stars" name="webdev_form_stars" id="webdev_form_stars_3" value="3"><br />' );
    array_push( $html_array, '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><input type="radio" class="stars" name="webdev_form_stars" id="webdev_form_stars_4" value="4"><br />' );
    array_push( $html_array, '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><input type="radio" class="stars" name="webdev_form_stars" id="webdev_form_stars_5" value="5">');
    array_push( $html_array, '</div>');
    
    array_push( $html_array, '<div class="review-form-box">');
    array_push( $html_array, '<label for="webdev_form_review">Leave a Review </label>');
    array_push( $html_array, '<textarea class="form-control" name="webdev_form_review" id="webdev_form_review" required></textarea>');
    array_push( $html_array, '</div>');
    
    array_push( $html_array, '<input class="form-control" type="text" name="webdev_form_from" style="visibility: hidden" id="webdev_form_from" value="sidebar">');
    
    array_push( $html_array, '<p class="form-response" name="webdev_form_response" id="webdev_form_response" style="visibility: hidden"></p>');
    
    array_push( $html_array, '<button id="webdev_form_submit" class="btn btn-primary" type="submit" value="submit">Submit</button>');
    
    array_push( $html_array, '</form>');
    array_push( $html_array, '</div>');
    
    return implode( '' , $html_array );

}

add_shortcode( 'our-reviews' , 'gmg_get_reviews_shortcode' );
function gmg_get_reviews_shortcode(){
    
    //HTML Array
    $html_array = array();
    
    //Call Reviews class
    $reviews = new Reviews();
    
    //Call past 5 reviews
    $reviews_array = $reviews->get_all_reviews_by_qty( 5, 1 );
    
    //Iterate through reviews
    foreach( $reviews_array as $each_review ){
        
        //call that review
        $review = new Review( $each_review );
        
        //add review to return HTML
        array_push( $html_array,  $review->show_review() );
    
    }
    
    //return the HTML review
    return implode( '' , $html_array );

}