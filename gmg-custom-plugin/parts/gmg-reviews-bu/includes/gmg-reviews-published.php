<?php

//add_action( 'transition_post_status', 'gmg_review_approved', 10, 3 );

function gmg_review_approved( $new_status, $old_status, $post ) {
    
//    error_log( 'Inside CM API call function!' );
//    error_log( 'Old status is ' . $old_status  );
//    error_log( 'New status is ' . $new_status  );
    
    if( $post->post_type == 'reviews' ){
        
//        error_log( 'They are a review!' );
        
        //gets List ID details from the database.
        $part_details = get_option( 'gmg-contact-121-pval' );
        $list_id = $part_details['rm_list_id'];
    
        if ( $old_status == 'pending'  &&  $new_status == 'publish' ) {

//            error_log( 'In GMG CM API Call '. $new_status . ' !' );
    
//            global $post;

            $theID = $post->ID;      
            $fields = get_fields( $theID );
            $review = new Review( $theID );
            if( is_array( $review->get_subject() ) ){
                
                $subject = $review->get_subject()[0];
                
            } else {
                
                $subject = $review->get_subject();
                
            }
            
//            $info = array(
//                'name'      => $fields['reviewer_fname'] . ' ' . $fields['reviewer_lname'],
//                'email'     => $fields['reviewer_email'],
//                'subject'   => $subject,
//                'status'    => 'Yes'
//            );
            
//            $campaign = new ReviewCM();
//            $result = $campaign->update_reviewer( $info );
//            
//            if($result->was_successful()) {
//                error_log( "Subscribed with code ".$result->http_status_code);
//            } else {
//                error_log( 'Failed with code '.$result->http_status_code );
//            }
            
        }
        
        elseif( $old_status == 'pending'  &&  $new_status == 'trash' ) {

            error_log( 'The review was trashed!' );
            
            $theID = $post->ID;            
            $fields = get_fields( $theID );
            
            $info = array(
                'name'      => $fields['reviewer_fname'] . ' ' . $fields['reviewer_lname'],
                'email'     => $fields['reviewer_email'],
                'subject'   => $fields['subject'],
                'status'    => 'Rejected'
            );
            
            $campaign = new ReviewCM();
            $result = $campaign->update_reviewer( $info );
            
            if($result->was_successful()) {
                error_log( "Subscribed with code ".$result->http_status_code);
            } else {
                error_log( 'Failed with code '.$result->http_status_code );
            }
            
        }
       
    }
}

//add_shortcode( 'get_dude', 'gmg_get_dude');

function gmg_get_dude(){
    
    $result = $wrap->get('Email address');

echo "Result of GET /api/v3.1/subscribers/{list id}.{format}?email={email}\n<br />";
if($result->was_successful()) {
    echo "Got subscriber <pre>";
    var_dump($result->response);
} else {
    echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
    var_dump($result->response);
}
echo '</pre>';
    
}