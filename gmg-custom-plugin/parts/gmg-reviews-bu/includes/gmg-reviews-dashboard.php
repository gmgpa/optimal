<?php

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function gmg_reviewer_add_dashboard_widgets() {

	wp_add_dashboard_widget(
        'gmg_reviewer_dashboard_widget',         // Widget slug.
        'Add Reputation Management',         // Title.
        'gmg_reviewer_dashboard_widget_function' // Display function.   
        );	
}
//add_action( 'wp_dashboard_setup', 'gmg_reviewer_add_dashboard_widgets' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function gmg_reviewer_dashboard_widget_function() {
    
    ?>
	<p><strong>Enter the name and email of customer.</strong></p>    

    <form name="gmg_review_form" method="post" action="" id="gmg_review_form">
        
        <label for="fieldFName">Name</label><br />
        <table>
            <tr>
                <td><input id="fieldFName" name="fieldFName" placeholder="First" type="text" required /> </td>
                <td><input id="fieldLName" name="fieldLName" placeholder="Last" type="text" required/></td>
            </tr>        
        </table>
        <p>
            <label for="fieldEmail">Email</label><br />
            <input id="fieldEmail" name="fieldEmail" type="email" required/>
        </p>
        <input type="hidden" name="gmg_review_action" value="gmg_review_submit" />
        <p>
            <input type="submit" name="gmg_reviewer_dashboard_button" value="Add Customer">
        </p>
    </form>
    <form name="gmg_review_form" method="post" action="" enctype="multipart/form-data">
        <p>If you are going to upload a CSV, make row First Name, Last Name, Email Address.</p>
        <p>If you need a template, <a href="https://enchantedfireside.com/wp-content/uploads/2019/04/reviews.csv" target="_blank">click here</a>.</p>
        <p>
            <label for="import_reviews_csv">Choose file to upload:</label>
            <input type="file" name="import_reviews_csv" id="import_reviews_csv" accept=".csv">
            <input type="hidden" name="gmg_review_import_action" value="gmg_review_submit" />
            <input type="submit" name="gmg_reviewer_import_button" value="Import">
        </p>

        
        <?php $reviews = new ReviewCustomers(); ?>
        <?php if( $reviews->get_last_used() ): ?>
            <p>Last used: <?php echo $reviews->get_last_used(); ?></p>
        <?php endif; ?>
        <?php if( $reviews->check_upload_file( 'goodUpload.txt' ) ): ?>
            <p class="good-upload">File Uploaded Correctly!</p>
            <?php $reviews->delete_upload_file( 'goodUpload.txt' ); ?>
        <?php endif; ?>
        <?php if( $reviews->check_upload_file( 'badUpload.txt' ) ): ?>
            <p class="bad-upload">Errors in Upload!</p>
            <?php $reviews->delete_upload_file( 'badUpload.txt' ); ?>
        <?php endif; ?>
        
    </form>

    <?php
}

add_action( 'init', 'func_gmg_reviews_send_contact' );
function func_gmg_reviews_send_contact() {
    
//    error_log( 'Called gmg review send' );
    
	if(isset($_POST['gmg_review_action'])) {
        
//        error_log( 'Inside Not Import!' );

            //gets List ID details from the database.
    //        $part_details = get_option( 'gmg-contact-121-pval' );
    //        $list_id = $part_details['rm_list_id'];

            $customers = new Customers();
            $cust_info = array(
                'fname'          => $_POST['fieldFName'],
                'lname'          => $_POST['fieldLName'],
                'email'         => $_POST['fieldEmail']
                );

    //        error_log( 'Water is ' . $_POST['fieldWater'] );

            if( !$customers->check_if_customer_exists_by_email( $_POST['fieldEmail'] ) ){

//                error_log( 'New Cust!' );

                $customers->create_new_customer( $cust_info );
                $c_id = $customers->get_customer_by_email( $_POST['fieldEmail'] );

            } else {

//                error_log( 'Old Cust!' );

                $c_id = $customers->get_customer_by_email( $_POST['fieldEmail'] );            
                $customer = new Customer( $c_id );

            }

            $campaign = new ReviewCM();
            $result = $campaign->add_reviewer( $cust_info , $c_id );

            if($result->was_successful()) {
                error_log( "Subscribed with code ". $result->http_status_code);
                $reviews = new ReviewCustomers();
                $reviews->set_last_used();
            } else {
                error_log( 'Failed with code '. $result->http_status_code );
            }

            wp_redirect( $_SERVER['HTTP_REFERER'] );

            exit();
        
        } elseif( isset( $_POST['gmg_review_import_action']) ){
        
            error_log( 'Import ' . $_FILES['import_reviews_csv']['name'] );

            if( $_FILES['import_reviews_csv'] && $_FILES['import_reviews_csv']['name'] != '' ) {
    //            
                $filename = explode('.', $_FILES['import_reviews_csv']['name']);

                if( $filename[1] === 'csv' ){
    //                
                    $csv_array = array_map('str_getcsv', file( $_FILES['import_reviews_csv']['tmp_name'] ));
                    error_log( 'Files count is ' . count( $csv_array ) );
                    
                    $index = 1;
                    $errors = array();
                    $good = array();

                    foreach( $csv_array as $csv_line ){
                        
//                        error_log( 'Line is ' . $index );

                        if( $csv_line[0] != '' && $csv_line[0] != ' ' && $csv_line[1] != '' && $csv_line[1] != ' ' && $csv_line[2] != '' && $csv_line[2] != ' ' ){
                            
                            $first = strtolower( ltrim( rtrim( $csv_line[0] ) ) );
                            if( is_utf8( $first ) ){
                                $first = mb_convert_encoding( $first, 'ASCII');
                            }
                            
                            $compare = 'first';
                            
//                            error_log( 'Firsty column is ' . $first . ' with encoding ' . mb_detect_encoding($first) . 'to be compared to encoding ' . mb_detect_encoding( $compare ) );
                            
//                            if( strtolower( ltrim( rtrim( $csv_line[0] ) ) ) === 'first' ){
                            if( strcasecmp( $first , $compare ) == 0 ){
                                
//                                error_log( 'Matches!');
                                
                            }else{

//                                error_log( 'Inside!');

                                $cust_info = array(
                                    'fname'          => ltrim( rtrim( $csv_line[0] ) ),
                                    'lname'          => ltrim( rtrim( $csv_line[1] ) ),
                                    'email'         => ltrim( rtrim( $csv_line[2] ) )
                                );

                                $customers = new Customers();

                                if( !$customers->check_if_customer_exists_by_email( $cust_info['email'] ) ){

//                                    error_log( 'New Cust!' );

                                    $customers->create_new_customer( $cust_info );
                                    $c_id = $customers->get_customer_by_email( $cust_info['email'] );

                                } else {

//                                    error_log( 'Old Cust!' );

                                    $c_id = $customers->get_customer_by_email( $cust_info['email'] );            
                                    $customer = new Customer( $c_id );

                        //            $customer->update_customer( $cust_info );
                        //
                        //            $customer->update_title();

                                }

                                $campaign = new ReviewCM();
                                $result = $campaign->add_reviewer( $cust_info , $c_id );

                                if($result->was_successful()) {
                                    
                                    error_log( "Subscribed with code ". $result->http_status_code);
                                    $reviews = new ReviewCustomers();
                                    $reviews->set_last_used();
                                    array_push( $good , $cust_info['fname'] . ' ' . $cust_info['lname'] . '(' . $cust_info['email'] . ') submitted.' );
                                } else {                                    
                                    error_log( 'Failed with code '. $result->http_status_code );
                                    array_push( $errors , $cust_info['fname'] . ' ' . $cust_info['lname'] . '(' . $cust_info['email'] . ') was imported incorrectly.' );                                
                                    
                                }
                                
                            }
                            $index++;

                        } else {
                            
                            array_push( $errors , $csv_line[0] . ' ' . $csv_line[1] . '(' . $csv_line[2] . ') was imported incorrectly.' );
                            
                        }
                    }
                    
                    if( count( $errors ) > 0 ){                        
                        $reviews = new ReviewCustomers();
                        $reviews->set_bad_upload_file( $errors );
                    }
                    
                    if( count( $good ) > 0 ){
                        $reviews = new ReviewCustomers();
                        $reviews->set_good_upload_file( $good );                        
                    }
                }
    //            
            }
        
        
        wp_redirect( $_SERVER['HTTP_REFERER'] );
        exit();
    }
}

// Returns true if $string is valid UTF-8 and false otherwise.
function is_utf8($string) {

    // From http://w3.org/International/questions/qa-forms-utf-8.html
    return preg_match('%^(?:
          [\x09\x0A\x0D\x20-\x7E]            # ASCII
        | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
        |  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
        | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
        |  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
        |  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
        | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
        |  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
    )*$%xs', $string);

} // function is_utf8