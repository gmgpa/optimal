<?php

add_action('genesis_loop', 'gmg_reviews_loop_checker');
function gmg_reviews_loop_checker() {
    
    if ( is_page( 'testimonials' ) ) {
        
//        error_log( 'Testimonials Loaded!' );
            
            global $post;
            
            $content = get_the_content( $post );
            
            if( $content ){
                
                remove_action( 'genesis_entry_content', 'genesis_do_post_content', 10 );
                
            }
    }
    
}

add_action('genesis_entry_content', 'gmg_reviews_loader');
function gmg_reviews_loader() {
//    error_log( 'Page Loader!' );
    
    if ( is_page( 'leave-a-review' ) ) {
        
        echo do_shortcode('[contact-form-7 id="31380" title="Leave a Review Form"]');
        
    }
    
        if ( is_page( 'testimonials' ) ) {
        
//        error_log( 'Testimonials Loaded!' );
            
            global $post;
            
            $content = get_the_content( $post );
            
            if( $content ){
                
                echo apply_filters( 'the_content', $content );
                
            }
                
        
//        echo '<h1 class="headline">Testimonials</h1>';
        
        $reviews = new Reviews();
        $qty = get_field( 'number_of_reviews' , 'rm_options' );
//        error_log('First Quantity is ' . $qty );
        if( $qty == null ){
            $qty = '0';
        }
        
        //If quantity is greater than 0, then we're going to call reviews in chunks.
        if( $qty > 0 ){
        
            $no_reviews = $reviews->get_number_of_reviews();
            
            if( $no_reviews > 0 ){
        
                $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

                $reviews_array = $reviews->get_all_reviews_by_qty( $qty, $paged );
                $no_reviews_left = $no_reviews - ( $qty * ( $paged -1 ) );
//                echo '<p>Reviews Left is ' . $no_reviews_left . '</p>';
                
                foreach( $reviews_array as $each_review ){
                    
                    $review = new Review( $each_review );
                    echo $review->show_review();
                
                }

                echo '<div class="nav-previous alignleft">' . get_previous_posts_link( 'Previous Reviews', $qty ) . '</div>';
                
                if( $no_reviews_left >= $qty ){
                    
                    echo '<div class="nav-next alignright">' . get_next_posts_link( 'Next Reviews', $qty ) . '</div>';
                    
                }
            
            } else {
                
                echo '<p>There are no testimonials at this time. Check back later.</p>';
            }
            
        } else{
            
            //Quantity is all so call everything.
            
            $reviews_array = $reviews->get_all_reviews_ids();
            $review_count = count( $reviews_array );

            if( $review_count > 0 ){
                
                foreach( $reviews_array as $each_review ){
                
//                    error_log('Key is ' . $each_review );
                    $review = new Review( $each_review );
                    echo $review->show_review();
                }
            
            } else {
            // no posts found

            echo '<p>There are no testimonials at this time. Check back later.</p>';
            }
        }
    }
    
    if ( is_page( 'thank-you' ) ) {
        
//        echo "<h2>Thanks for reaching out to us</h2><p>We'll reach out to you soon.</p>";
    }
}