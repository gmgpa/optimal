<?php

function register_gmg_reviews_menu_page(){
    
//    error_log( 'Inside Reviews menu page ' );
    
    $plugin = new Gmg_Custom_Plugin();
    $options_name = $plugin->get_plugin_name();
    
    acf_add_options_sub_page(
        array(
            'page_title' 	=> 'Reputation Management Settings',
            'parent_slug'	=> $options_name,
            'menu_title'	=> 'Reviews Settings',
            'capability'  => 'manage_options',
            'post_id'       => 'rm_options',
	));
    
}

add_action( 'wp_enqueue_scripts', 'register_reviews_scripts' );
function register_reviews_scripts(){
    
/*    wp_register_script( 'review' , plugin_dir_url( __FILE__ ) . 'js/gmg-rm-leave-a-review.js', array( 'jquery' ), false, true );
    wp_enqueue_script('review');
    wp_localize_script( 'review', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    
    wp_register_script('recaptcha', 'https://www.google.com/recaptcha/api.js?render=' . get_field( 'recaptcha_site', 'overall_settings' ) );
    wp_enqueue_script('recaptcha');*/
    
    wp_enqueue_style( 'review-css' , plugin_dir_url( __FILE__ ) . 'css/gmg-reviews.css' );
}

add_action( 'admin_enqueue_scripts', 'register_reviews_admin_scripts' );
function register_reviews_admin_scripts() {
	wp_enqueue_style( 'review-admin-css' , plugin_dir_url( __FILE__ ) . 'css/gmg-reviews-admin.css' );
}

//This is the function that creates a review after Custom Form 7 
add_action( 'wpcf7_before_send_mail', 'gmg_review_page_autosave', 10, 1 );
function gmg_review_page_autosave( $contact_form ) {
    
    //get form ID
    $form_id = $_POST['_wpcf7'];
    
    error_log('Form ID is ' . $form_id );
 
    //Check to see if it's the one we want.
    if ($form_id == 31380 ){
        
        //get raw of the name
        $raw_name = explode( " " , $_POST['your-name'] );
        
        //Let's pass down Customer info into an array
        $cust_info = array(
            'fname'          => isset( $raw_name[0] ) ? $raw_name[0] : '',
            'lname'          => isset( $raw_name[1] ) ? $raw_name[1] : '' ,
            'email'          => $_POST['your-email']
        );

        //Then, let's see if the customer is already set.
        if( isset( $_POST['c_id'] ) ){

            $c_id = $_POST['c_id'];
            $customer = new Customer( $c_id );
            $customer->update_customer( $cust_info );
            $customer->update_title();

        } else {

            $c_id = gmg_process_customer( $cust_info );

        }

        //Now that we have our customer, we need to create the review.
        $customer = new Customer( $c_id );   

        //As before, throw it all in an array to send.
        $info_array = array(
            'reviewer_fname'    => $customer->get_fname(),
            'reviewer_lname'    => $customer->get_lname(),
            'reviewer_email'    => $customer->get_email(),
            'reviewer_town'     => $_POST['your-town'],
            'review'            => $_POST['your-review']
            );

        //Process the review and return the ID
//        $r_id = gmg_process_review( $info_array );
        
        //Initialize the Reviews class.
        $reviews = new Reviews();

        //Create the review.
        $r_id = $reviews->create_new_review( $info_array );

        //Initialize the Review Class
        $review = new Review( $r_id );

        //Set Stars
        $review->set_stars( $_POST['your-stars'] );

        //Set Subject
        $review->set_subject( $_POST['your-subject'] );

        //Update title
        $review->update_title();
    }
}

//This is the function called by the Review Page.
//add_action('wp_ajax_gmg_review_page_save', 'gmg_review_page_save');
//add_action('wp_ajax_nopriv_gmg_review_page_save', 'gmg_review_page_save');
function gmg_review_page_save() {
    
//    error_log( 'Inside Page Reviewer with something from Sidebar ' . $_POST['webdev_form_from'] );
    
    if( isset( $_POST['webdev_form_from'] ) ){
        if( $_POST['webdev_form_from'] == 'sidebar' ){
            
            $sidebar = true;
            
//            error_log( 'Sidebar is ' . $sidebar );
        }
    } else {
        
        $sidebar = false;
    }
    
//    $recaptcha_secret = '6LeL9JsUAAAAAH7uWMH1jxGmumwtVryUVX5BU5Ij';
    $recaptcha_secret = get_field( 'recaptcha_secret', 'overall_settings' );
    error_log( 'Secret is ' . $recaptcha_secret );
    $recaptcha_response = (isset($_POST['CaptureToken']) ) ? $_POST['CaptureToken'] : '';
    error_log( 'Response is ' . $recaptcha_response );
    
    $data = array(
            'secret' => $recaptcha_secret,
            'response' => $recaptcha_response
        );
    
    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($verify, CURLOPT_POST, true);
    curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($verify);
    curl_close($verify);
    $response = json_decode($response, true );
    error_log( 'Recaptcha Score is ' . $response["score"] );
    
    // Take action based on the score returned:
//    if ($response["score"] >= 0.5) {
    
        //Let's pass down Customer info into an array
        $cust_info = array(
            'fname'          => $_POST['fname'],
            'lname'          => $_POST['lname'],
            'email'          => $_POST['email']
        );

        //Then, let's see if the customer is already set.
        if( isset( $_POST['c_id'] ) ){

            $c_id = $_POST['c_id'];
            $customer = new Customer( $c_id );
            $customer->update_customer( $cust_info );
            $customer->update_title();

        } else {

            $c_id = gmg_process_customer( $cust_info );

        }

        //Now that we have our customer, we need to create the review.
        $customer = new Customer( $c_id );   

        //As before, throw it all in an array to send.
        $info_array = array(
            'reviewer_fname'    => $customer->get_fname(),
            'reviewer_lname'    => $customer->get_lname(),
            'reviewer_email'    => $customer->get_email(),
            'reviewer_town'     => $_POST['town'],
            'review'            => $_POST['review']
            );

        //Process the review and return the ID
        $r_id = gmg_process_review( $info_array );

        //Now send the email to the right person
        $return = gmg_send_review_email( $c_id, $r_id );
        
//    } else {
//        
//        $return['foo'] = 'There was a problem with authentication';
//    
//    }
    
    if( $sidebar ){
        
        $return['sidebar'] = $sidebar;
    }
    
    echo wp_send_json( $return );
    exit();
    
}

//This function processes customers as either new or updates established.
function gmg_process_customer( $info_array ){
    
    $customers = new Customers();
    
    //First, let's check if Customer exists    
    if( !$customers->check_if_customer_exists_by_email( $info_array['email'] ) ){
        
        $c_id = $customers->create_new_customer( $info_array );
    
    //If they don't, create a new one.
    } else {
        
        $c_id = $customers->get_customer_by_email( $info_array['email'] );
        $customer = new Customer( $c_id );
        $customer->update_customer( $info_array );
        $customer->update_title();
    }
    
    //Return the new ID
    return $c_id;
}

//This function processes new Reviews.
function gmg_process_review( $info_array ){
    
    //Initialize the Reviews class.
    $reviews = new Reviews();
    
    //Create the review.
    $r_id = $reviews->create_new_review( $info_array );
    
    //Initialize the Review Class
    $review = new Review( $r_id );
    
    //Set Stars
    $review->set_stars( $_POST['stars'] );
    
    //Set Subject
    $review->set_subject( $_POST['subject'] );
    
    //Update title
    $review->update_title();
    
    //Return the ID
    return $r_id;    
}


function gmg_send_review_email( $c_id, $r_id ){
    
    $customer = new Customer( $c_id );
    $review = new Review( $r_id );
    
    $recipient = get_field( 'reviewer_email_address' , 'rm_options' );
//    $recipient = 'ray@goodmarketinggroup.com';
    
    $link = get_edit_post_link( $r_id, '' );
    $reviewer_email = $review->get_email();
//    error_log( 'The edit post link is ' . $link );
    
    $email_message = "You got a new review from " . $customer->get_name() . ".\n\n" .
        "Their review was:" . "\n" .
        '"' . $review->get_review_text() . '"' . "\n\n" .
        "Please log in at " .
        $link .
        " to review and approve it.";

    $message = "FROM: " . $review->get_full_name() . "\n" .
        "EMAIL: " . $reviewer_email . "\n" .
        "MESSAGE: " . $email_message;

//        error_log( 'Ready to send Mail' );

    $site_name = get_bloginfo( 'name' );
    $site_email = get_bloginfo( 'admin_email' );

    $subject = "New Review on $site_name";
    
    $headers   = array();
    $headers[] = "MIME-Version: 1.0";
    $headers[] = "Content-type: text/plain; charset=iso-8859-1";
    $headers[] = "From: $site_name <$site_email>";
//        $headers[] = "CC: Ray <ray@goodmarketinggroup.com>";
    $headers[] = "Bcc: Forms <forms@goodmarketinggroup.com>";
    $headers[] = "Reply-To: { $reviewer_email }";
    $headers[] = "Subject: { $subject }";
    $headers[] = "X-Mailer: PHP/".phpversion();

    $answer = wp_mail($recipient, $subject, $message, implode("\r\n", $headers));
//        error_log( 'Answer in Page Review is ' . $answer );
    
    if ( $answer ){
        $return = array ( 'foo' => 'Review submitted!' );
        error_log( 'Success' );
    } else {
        $return = array ( 'foo' => 'There was an error sending the message.' );
        error_log( 'Failure' );
    }
    
    return $return;
    
}


add_filter('acf/load_field/key=field_5b87ffa895a6e', 'gmg_rm_update_copyright');
function gmg_rm_update_copyright( $field ){
    
//    error_log( 'Update Copyright!');
    
    $field['message'] = 'Copyright ' . date("Y") . ' Good Group LLC. All Rights Reserved';
    
    return $field;
}


add_filter('acf/load_field/key=field_5b80176e2246b', 'gmg_cm_reviews_lists_load');
function gmg_cm_reviews_lists_load( $field ){
    
//    error_log( 'Call Lists Load' );
    
    if( class_exists( 'ReviewCM' ) ){
    
        $bm = new ReviewCM();
        $list_array = $bm->get_clients_list();
        if( is_array( $list_array ) AND count( $list_array ) != 0) {
            foreach( $list_array as $list ) {

                $field['choices'][ $list['ListID'] ] = $list['Name'];
            }
        }
    }
    
    return $field;
}
