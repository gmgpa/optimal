<?php

//        error_log( 'Inside Inside Reviews' );

$revs = plugin_dir_path( __FILE__ );

//Let's bring in Class Review.
require_once($revs . '/includes/gmg-class-reviews.php');

//Let's bring in the custom post type Reviews, which is where we will store the info.
require_once($revs . '/includes/gmg-reviews-cpt.php');

//Let's also bring in the code to save the ACF fields.
require_once($revs . '/includes/gmg-reviews-acf.php');

//Let's also bring in options to be show in admin
require_once($revs . '/includes/gmg-reviews-admin.php');

//Let's brining in some shortcode to show the review form in a sidebar.
require_once($revs . '/includes/gmg-reviews-shortcode.php');

//Let's also bring in code to load the right page content.
require_once($revs . '/includes/gmg-reviews-loader.php');

//Let's also bring in code to create the right pages if they don't exist.
require_once($revs . '/includes/gmg-reviews-creator.php');

//Let's also bring in the dashboard widget
//require_once($revs . '/includes/gmg-reviews-dashboard.php');

//Let's also bring in the code to handle when a review is published
//require_once($revs . '/includes/gmg-reviews-published.php');

add_action( 'admin_menu', 'register_gmg_reviews_menu_page' );
