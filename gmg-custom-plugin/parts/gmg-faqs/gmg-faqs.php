<?php

//Add Custom Post Type For FAQ
function faq_init() {
	$labels = array(
		'name'               => _x( 'FAQ', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'FAQ', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'FAQ', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'FAQ', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'gallery', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New FAQ', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New FAQ', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit FAQ', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View FAQ', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All FAQs', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search FAQs', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent FAQ:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No FAQ found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No FAQ found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'public' => true,
		'labels'  => $labels,
		'has_archive' => true,
		'supports' => array('editor', 'title', 'genesis-cpt-archives-settings'),
        'rewrite'   => array( 'slug' => 'faqs' ),
        'taxonomies'    => array( 'category' ),
//		'menu_icon' => 'dashicons-images-alt',
		'show_ui' => true,
		'show_in_menu' => 'gmg-custom-plugin',
        'show_in_rest' => true,
		'query_var' => true,
		'publicly_queryable' => true,
		'pages' => true,
	);
	register_post_type( 'gmg-faq', $args );
}
add_action( 'init', 'faq_init' );

// Remove after entry widget
remove_action( 'genesis_after_entry', 'genesis_after_entry_widget_area', 10 );

// Add after entry widget to posts and pages
add_action( 'genesis_after_entry', 'gmg_after_entry_widget', 10 );

function gmg_after_entry_widget() {

   if ( ! is_singular( array( 'gmg-faq' )) )
        return;

        genesis_widget_area( 'after-entry', array(
            'before' => '<div class="after-entry widget-area">',
            'after'  => '</div>',
        ) );
}

class FAQs{
    
    public function __construct( ) {
            
        }
    
    public function get_all_faqs_sort_by_cats(){
        
        $faqs = array();
        $cats_array = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'gmg-faq' ),
            'posts_per_page'         => '-1',
            'post_status'            => 'publish',
            'orderby'                => 'menu_order',
            'order'                  => 'ASC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                
                $query->the_post();
                
//                error_log( 'FAQ with ID ' . get_the_ID() . ' is called ' . get_the_title() );
                
                //Get Category Object
                $cat = get_the_category()[0];
//                $cat = get_the_terms( get_the_ID(), 'category');
//                error_log( 'Cat is called ' . $cat->name );
                $current_cat_id = $cat->term_id;
                
                //See if Cats Array has anything in it.
                if( count( $cats_array ) > 0 ){
                    
                    //Create a stop variable.
                    $go = true;
                    
                    //Create an index
                    $index = -1;
                    
                    //Iterate through all the Cats
                    foreach( $cats_array as $each_cat ){
                        
                        //Iterate the index
                        $index++;
                        
                        //Get the Cat ID to be examined.
                        $each_cat_id = $each_cat['id'];
                        
                        //Check to see if Cat ID and Current Cat ID are the same
                        if( $each_cat_id == $current_cat_id ){
                            
                            //They are, so we need to just add that ID to the entry.
                            
                            //First, we'll tell the code not to go any further after this.
                            $go = false;
                            
                            //Let us get the IDs also
//                            $ids = $each_cat['ids'];
                            
                            //Second, let's call the function that sets the ids and set the new array.
                            $new_cat_array = set_the_ids( $each_cat['ids'], get_the_ID() );
                            
                            //Set the ID
                            $new_cat_array['id'] = $current_cat_id;
                            
                            //Set the new array
                            $cats_array[ $index ] = $new_cat_array;
//                            echo '<p>Found' . var_dump( $cats_array ) . '</p>';
                            
                        }
                    
                    }
                    
                    //It was never found so let's add.
                    if( $go ){
                        
                        //Set the array to be placed.
                        $new_cat_array = array();
                        
                        //Set the ID
                        $new_cat_array['id'] = $current_cat_id;
                        $new_cat_array['ids'] = get_the_ID();
                        
                        //Set the new array
                        $cats_array[] = $new_cat_array;
//                        echo '<p>Not Found' . var_dump( $cats_array ) . '</p>';
                        
                    }
                
                } else {

                    //It does not, so create it.

                    //Then we'll add the new ID and put it back inside.
                    $new_cat_array['id'] = $current_cat_id;

                    //There wasn't anything inside so let's just as this id.
                    $new_cat_array['ids'] = get_the_ID();

                    //Set the new array
                    $cats_array[] = $new_cat_array;
//                    echo '<p>Nothing:' . var_dump( $cats_array ) . '</p>';
                }
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $cats_array;       
        
    }
    
    public function get_all_faqs_sort_by_cats2(){
        
        $faqs = array();
        $cats_array = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'gmg-faq' ),
            'posts_per_page'         => '-1',
            'post_status'            => 'publish',
            'orderby'                => 'menu_order',
            'order'                  => 'ASC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                
                $query->the_post();
                
                error_log( 'FAQ with ID ' . get_the_ID() . ' is called ' . get_the_title() );
                
                //Get Category Object
                $cat = get_the_category()[0];
//                $cat = get_the_terms( get_the_ID(), 'category');
                error_log( 'Cat is called ' . $cat->name );
                $current_cat_id = $cat->term_id;
                
                //Cats Array has anything in it.
                if( count( $cats_array ) > 0 ){
                    
                    //Create a stop variable.
                    $go = true;
                    
                    //Create an index
                    $index = -1;
                    
                    //Iterate through all the Cats
                    foreach( $cats_array as $each_cat ){
                        
                        //Iterate the index
                        $index++;
                        
                        //Set the Cat ID to be examined.
                        $each_cat_id = $each_cat['id'];
                        
                        //Check to see if Cat ID is in the Array
                        if( $each_cat_id == $current_cat_id ){
                            
                            //It was found so no need to do anything.
                            $go = false;
                            $set_index = $index;
                            
                        }
                    
                    }
                    
                    //It was never found so let's add.
                    if( $go ){
                        
                        //Set the array to be placed.
                        $new_cat_array = array();
                        
                    if( isset( $cat->term_id ) ){
                        //Then we'll add the new ID and put it back inside.
                        $new_cat_array['id'] = $current_cat_id;
                    }
                        
                        //First, we'll grab the array.
                        
                        $old_cat_array = $cats_array[$set_index];
                        $ids = $old_cat_array[ 'ids' ];
                        
                        //Let's see if there are even any ids in there.
                        if( strlen( $ids ) > 0 ){
                        
                            //There are so let's check to see if there are multiple ids inside
                            if( strpos( $ids , ',' ) != false ){

                                //There were so we explode it into an array.
                                $ids_array = explode(',' , $ids );
                                
                                $new_cat_array['ids'] = implode( ',' , $ids_array );

                            } else {

                                //It wasn't multiple ids so we'll just add this one to whatever was in it.
                                $cats_array[ $cat->term_id ] = $ids . ',' . get_the_ID();
                                
                                $new_cat_array['ids'] = $ids . ',' . get_the_ID();

                            }
                        
                        } else {
                            
                            //There wasn't anything inside so let's just as this id.
                            $new_cat_array['ids'] = get_the_ID();
//                            $cats_array[ $cat->term_id ] = get_the_ID();
                        }
                        
                        //Set the new array
                        $cats_array[ $set_index ] = $new_cat_array;
                        
                    }             
                    
                    
                } else {
                    
                    //Set the array to be placed.
                    $new_cat_array = array();

                    //Then we'll add the new ID and put it back inside.
                    $new_cat_array['id'] = $current_cat_id;
                    
                    //There wasn't anything inside so let's just as this id.
                    $new_cat_array['ids'] = get_the_ID();
                    
                                            //Set the new array
                        $cats_array[] = $new_cat_array;
                    
                    //the Array was empty so add this Cat ID as well as the post ID.                    
//                    $cats_array[ $cat->term_id ] = get_the_ID();
                }
                
//                array_push( $faqs, 
//                           array(
//                               'title'  => get_the_title(),
//                               'link'   => get_permalink(),
//                               'cat'    => get_the_category(),
//                           )
//                          );                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $cats_array;       
        
    }
    
    public function get_all_faqs(){
        
        $faqs = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'gmg-faq' ),
            'posts_per_page'         => '-1',
            'post_status'            => 'publish',
            'orderby'                => 'menu_order',
            'order'                  => 'ASC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                
                $query->the_post();
                
                array_push( $faqs, 
                           array(
                               'title'  => get_the_title(),
                               'link'   => get_permalink(),
                               'cat'    => get_the_category(),
                           )
                          );                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $faqs;       
        
    }    
    
}

function set_the_ids( $ids, $id ){
    
    //Set the new array;
    $new_cat_array = array();

    //Let's see if there are even any ids in there.
    if( strlen( $ids ) > 0 ){

        //There are so let's check to see if there are multiple ids inside
        if( strpos( $ids , ',' ) != false ){

            //There were so we explode it into an array.
            $ids_array = explode(',' , $ids );
            
//            echo '<p>IDs:' . var_dump( $ids_array ) . '</p>';
            
            //Add the new ID
            array_push( $ids_array , $id );

            //Set it to the array
            $new_cat_array['ids'] = implode( ',' , $ids_array );

        } else {

            //It wasn't multiple ids so we'll just add this one to whatever was in it.
            $new_cat_array['ids'] = $ids . ',' . $id;

        }

    } else {

        //There wasn't anything inside so let's just as this id.
        $new_cat_array['ids'] = $id;
        
    }
    
    return $new_cat_array;
}
