<?php

//        error_log( 'Inside Inside Reviews' );
$revs = plugin_dir_path( __FILE__ );

//Let's bring in the ACF fields.
require_once($revs . '/includes/gmg-reviews-create-categories.php');

//Let's bring in the ACF fields.
require_once($revs . '/includes/gmg-reviews-acf.php');

//Let's bring in Class Review.
require_once($revs . '/includes/gmg-class-reviews.php');

//Let's bring in the custom post type Reviews, which is where we will store the info.
require_once($revs . '/includes/gmg-reviews-cpt.php');

//Let's brining in some shortcode to show the review form in a sidebar.
require_once($revs . '/includes/gmg-reviews-shortcode.php');

//Let's also bring in code to load the right page content.
require_once($revs . '/includes/gmg-reviews-loader.php');

//Let's also bring in code to create the right pages if they don't exist.
require_once($revs . '/includes/gmg-reviews-creator.php');

//Let's also bring in the code to save the ACF fields.
require_once($revs . '/includes/gmg-reviews-save.php');

//Let's also bring in the code to create a new review after form submission
require_once($revs . '/includes/gmg-reviews-after-review-form.php');

//Let's also bring in options to be show in admin
require_once($revs . '/includes/gmg-reviews-admin.php');

//Let's also bring in code to load the right categories on the Leave a Review Form.
require_once($revs . '/includes/gmg-reviews-form-categories.php');

//Bring in the reviews block
require_once($revs . '/reviews-block/gmg-reviews-block.php');


/*
OPTIONS BASED ON EMAIL MARKETING PLATFORMS
*/

$reviews = new Reviews();

//$platform = get_field( 'email_platform', 'overall_settings' );
//error_log( 'Email Platform is ' . $reviews->get_email_platform() );

if( $reviews->get_email_platform() != "none" ){

    //Let's also bring in the dashboard widget
    require_once($revs . '/includes/gmg-reviews-dashboard.php');

    //Let's also bring in the code to handle when a review is published
    require_once($revs . '/includes/gmg-reviews-published.php');
    
}

add_action( 'admin_menu', 'register_gmg_reviews_menu_page' );

add_action( 'wp_enqueue_scripts', 'register_reviews_slider_scripts' );
function register_reviews_slider_scripts(){
    
//    error_log( 'Calling scripts!');
    
    $revs = plugin_dir_path( __FILE__ );
    
    wp_enqueue_style( 'slick-css', plugin_dir_url( __FILE__ ) . 'slick/slick.css', [], false, 'all' );
    
    wp_enqueue_style( 'slick-theme-css', plugin_dir_url( __FILE__ ) . 'slick/slick-theme.css', ['slick-css'], false, 'all' );
    
    wp_enqueue_script( 'slick-js', plugin_dir_url( __FILE__ ) . 'slick/slick.min.js', array( 'jquery' ), null, true );
    
    wp_enqueue_script( 'review-slick-js', plugin_dir_url( __FILE__ ) . 'includes/js/gmg-reviews-block.js', array( 'jquery' ), null, true );
}