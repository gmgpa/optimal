<?php

/*
*
* Creates a review after Gravity Forms Leave A Review Submission
*
* Then sends them down to SharpSpring of Campaign Monitor
*
* Status: TESTING
*
*/

add_action( 'gform_after_submission_5', 'gmg_review_page_autosave', 10, 2 );
function gmg_review_page_autosave( $entry, $form ) {
    
    //As before, throw it all in an array to send.
    $info_array = array(
        'fname'    => rgar( $entry, '1.3' ),
        'lname'    => rgar( $entry, '1.6' ),
        'email'    => rgar( $entry, '4' ),
        'reviewer_town'     => rgar( $entry, '5' ),
        'review'            => rgar( $entry, '8' ),
        'stars'             => rgar( $entry, '9' ),
        'subject'           => rgar( $entry, '10' ),
        );
        
    //Initialize the Reviews class.
    $reviews = new Reviews();
    
//    error_log('Review submitted by ' . $info_array['fname']  );

    //Create the review.
    $r_id = $reviews->create_new_review( $info_array );
    
    //First, we only need to do something with this if it's Campaign Monitor
    if( $reviews->get_email_platform() == 'campaign' ){
        
        //If Campaign Monitor, then do this.
        $campaign = new ReviewCM();
        
        //Next, remove them from the Leave a Review list
        $campaign->gmg_cm_reviews_remove_list( $info_array );
        
        //Next, we do different things depending on stars
    
        //If 5 starts, also send a nudge
        if( $info_array['stars'] == 5 ){
            
            //Send Nudge
            $campaign->gmg_cm_reviews_send_nudge( $info_array );
        
        }        
    }
}