<?php

//add action that only fires when draft or pending is published
add_action( "pending_to_publish", 'gmg_review_approved', 10, 1 );
add_action( "draft_to_publish", 'gmg_review_approved', 10, 1 );
function gmg_review_approved( $post ) {
    
    if( $post->post_type == 'reviews' ){
        
        //Instantiate the Review Class
        $review = new Review( $post->ID );
        
        //Instantiate the Reviews Class
        $reviews = new Reviews();
        
        //check to see if the reviewer has been sent down already.
        if( $review->get_review_submitted() == 'no' || $review->get_review_submitted() == '' || $review->get_review_submitted() == null ){

            //If not, do so.

            //First, we need to figure out where we're sending it.
            if( $reviews->get_email_platform() == 'sharpspring' ){

                //If SharpSpring, then do this.
                $base = get_field( 'sharpspring_base', 'rm_options' );
                $end = get_field( 'sharpspring_sm_nudge_form_endpoint', 'rm_options' );

                //Instantiate the Review SharpSpring Class
                $sharpspring = new ReviewSS( $base, $end );                    

                //Get an array full of data
                $info_array = $review->get_review_info();
                
//                error_log( 'Draft to Publish this review to SharpSpring for ' . $info_array['fname'] );

                //send it down
                $sharpspring->gmg_review_manual_enter_sharpspring( $info_array );

            } else {
                
                //Get an array full of data
                $info_array = $review->get_review_info();
                
                error_log( 'Draft to Publish this review to Campaign Monitor for ' . $info_array['fname'] );

                //If Campaign Monitor, then do this.
                $campaign = new ReviewCM();
                
                //Remove from Leave a Review List
                //$result = $campaign->gmg_cm_reviews_remove_list( $info_array );

                //Send Nudge
                $campaign->gmg_cm_reviews_send_nudge( $info_array );

            }

            //update to say that it was submitted
            $review->set_review_submitted('yes');

        }
    }
}

/*
* 
* Add action that only fires when draft is trashed
*
* NOT NEEDED ANYMORE SINCE UPON LEAVE A REVIEW FORM SUBMISSION
* USER IS AUTOMATICALLY REMOVED.
*/

//add_action( "draft_to_trash", 'gmg_review_trashed', 10, 1 );
function gmg_review_trashed( $post ) {
    
//    error_log( 'The review was trashed!' );
    
    if( $post->post_type == 'reviews' ){

        //Instantiate the Review Class
        $review = new Review( $post->ID );
        
        //Instantiate the Reviews Class
        $reviews = new Reviews();

        //First, we need to figure out where we're sending it.
        if( $reviews->get_email_platform() != 'sharpspring' ){

            //If Campaign Monitor, then do this.
            $campaign = new ReviewCM();
            
            //Get an array full of data
            $info_array = $review->get_review_info();
            
//            error_log( 'Draft to Trash this review to Campaign Monitor for ' . $info_array['fname'] );           

            //Remove from Leave a Review List
            $result = $campaign->gmg_cm_reviews_remove_list( $info_array );
        }
    }
}