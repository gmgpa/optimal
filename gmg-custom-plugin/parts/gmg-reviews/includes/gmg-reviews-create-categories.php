<?php

/*
*
* This is a function that creates baseline categories for Reputation Management
*
* Status: TESTING
*/

function gmg_reminders_create_categories() {
    
    if( get_field( 'categories_created' , 'rm_options' ) != 'yes' ){
        
        $taxonomy = 'category';
        
        $reviews_term = term_exists('reviews', $taxonomy );
    
        if( $reviews_term == null ){
            
            error_log( 'No Reviews Category!');
            
            $term = 'Reviews';
            
            //Define the category
            $cat = array(
                'description' => 'A category for only reviews',
                'slug' => 'reviews',
                'parent' => ''
            );
            
            // Create the category 
            $reviews_term = wp_insert_term( $term, $taxonomy, $cat );
            
        }
        
        if( term_exists('sales', $taxonomy ) == null ){
            
            error_log( 'No Sales Category!');
            
            $term = 'Sales';
            
            //Define the category
            $cat = array(
                'description' => 'A category for only Sales reviews',
                'slug' => 'sales',
                'parent' => $reviews_term['term_id']
            );
            
            // Create the category 
            $sales_term = wp_insert_term( $term, $taxonomy, $cat );
            
        }
        
        if( term_exists( 'service' , $taxonomy ) == null ){
            
            error_log( 'No Service Category!');
            
            $term = 'Service';
            
            //Define the category
            $cat = array(
                'description' => 'A category for only Service reviews',
                'slug' => 'service',
                'parent' => $reviews_term['term_id']
            );
            
            // Create the category 
            $service_term = wp_insert_term( $term, $taxonomy, $cat );            
            
        }
        
        update_field( 'categories_created', 'yes', 'rm_options' );
    }
}