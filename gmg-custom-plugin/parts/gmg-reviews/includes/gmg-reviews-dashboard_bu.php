<?php

/*
* Add a widget to the dashboard.
*
* This function is hooked into the 'wp_dashboard_setup' action below.
*
* Status: SOLID
*
*/

add_action( 'wp_dashboard_setup', 'gmg_reviewer_add_dashboard_widgets' );
function gmg_reviewer_add_dashboard_widgets() {

	wp_add_dashboard_widget(
        'gmg_reviewer_dashboard_widget',         // Widget slug.
        'Add Reputation Management',         // Title.
        'gmg_reviewer_dashboard_widget_function' // Display function.   
        );	
}

/*
*
* Output the contents of our Dashboard Widget.
* 
* Status: SOLID
*/

function gmg_reviewer_dashboard_widget_function() {
    
    ?>
	<p><strong>Enter the name and email of customer.</strong></p>    

    <form name="gmg_review_form" method="post" action="" id="gmg_review_form">
        
        <label for="fieldFName">Name</label><br />
        <table>
            <tr>
                <td><input id="fieldFName" name="fieldFName" placeholder="First" type="text" required /> </td>
                <td><input id="fieldLName" name="fieldLName" placeholder="Last" type="text" required/></td>
            </tr>        
        </table>
        <p>
            <label for="fieldEmail">Email</label><br />
            <input id="fieldEmail" name="fieldEmail" type="email" required/>
        </p>
        <input type="hidden" name="gmg_review_action" value="gmg_review_submit" />
        <p>
            <input type="submit" name="gmg_reviewer_dashboard_button" value="Add Customer">
        </p>
    </form>

    <?php
}

/*
*
* Get info from Dashboard widget form and sends data 
* to SharpSpring RM Dashboard endpoint that launches contact 
* into Reputation Management Workflow
* 
* Status: SOLID
*
*/

add_action( 'init', 'func_gmg_reviews_send_contact' );
function func_gmg_reviews_send_contact() {
    
//    error_log( 'Called gmg review send' );
    
    //Make sure it's the right form sending stuff down.
	if( isset( $_POST['gmg_review_action'] ) ) {
        
//        error_log( 'Called gmg review send action' );
        
        //Throw it into an array
        $cust_info = array(
            'fname'     => $_POST['fieldFName'],
            'lname'     => $_POST['fieldLName'],
            'email'     => $_POST['fieldEmail']
            );
        
        //Instantiate the Reviews class
        $reviews = new Reviews();
        
//        error_log( 'Email platform that is chosen is ' . $reviews->get_email_platform() );
        
        //see which place we're sending things to
        if( $reviews->get_email_platform() == 'sharpspring' ){
        
            //If SharpSpring, then do this.
            $base = get_field( 'sharpspring_base', 'rm_options' );
            $end = get_field( 'sharpspring_dashboard_form_endpoint', 'rm_options' );

            $sharpspring = new ReviewSS( $base, $end );
            $sharpspring->gmg_review_dashboard_enter_sharpspring( $cust_info );
            
        } else {
            
            //Then Campaign Monitor
            $campaign = new ReviewCM();
            $result = $campaign->gmg_cm_reviews_add_customer( $cust_info );
//            $result = $campaign->add_reviewer( $cust_info , $c_id );

            if( $result->was_successful() ) {
                
//                error_log( "Subscribed with code ". $result->http_status_code);
                $reviews = new ReviewCustomers();
                $reviews->set_last_used();
                
//            } else {
                
//                error_log( 'Failed with code '. $result->http_status_code );
                
            }
        }
        
        wp_redirect( $_SERVER['HTTP_REFERER'] );
        exit();
    }
}