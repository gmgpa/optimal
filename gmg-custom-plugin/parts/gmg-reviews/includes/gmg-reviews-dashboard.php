<?php

/*
* Add a widget to the dashboard.
*
* This function is hooked into the 'wp_dashboard_setup' action below.
*
* Status: SOLID
*
*/

add_action( 'wp_dashboard_setup', 'gmg_reviewer_add_dashboard_widgets' );
function gmg_reviewer_add_dashboard_widgets() {

	wp_add_dashboard_widget(
        'gmg_reviewer_dashboard_widget',         // Widget slug.
        'Add Reputation Management',         // Title.
        'gmg_reviewer_dashboard_widget_function' // Display function.   
        );	
}

/*
*
* Output the contents of our Dashboard Widget.
* 
* Status: TESTING
*/

function gmg_reviewer_dashboard_widget_function() {
    
    ?>

	<p><strong>Click button below to go to enter customer.</strong></p>

    <a href="/ask-for-a-review/" class="button" target="_blank">Go Now</a>

    <?php
}

/*
*
* Record when Ask for a Review form is filled out to log date.
* 
* Status: TESTING
*
*/

add_action( 'gform_after_submission_8', 'gmg_review_log_ask', 10, 2 );
function gmg_review_log_ask() {
    
    $reviews = new Reviews();
    $reviews->set_last_used();
    
}