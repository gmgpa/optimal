jQuery(document).ready(function ($) {
    "use strict";
    
    if( $('#gmg_review_form') ){
        
//        var recaptcha = $("#CaptureRecaptcha").val();
//        
//        grecaptcha.ready(function() {
//                grecaptcha.execute( recaptcha,
//                                   { action: 'GMGReview' }
//                                  ).then( function ( token ) {
//                    $("#CaptureRecaptcha").attr('value', token );
//                }
//                                        );
//            }
//                            );
    
        var former = $('#gmg-review-form');

        former.on('submit', function (m) {
            m.preventDefault();

    //        alert('ALARM!');

            var form_fname = $("#webdev_form_fname").val();
            var form_lname = $("#webdev_form_lname").val();
            var form_email = $("#webdev_form_email").val();
            var form_town = $("#webdev_form_town").val();
    //        var form_subject = $("#webdev_form_category").val();
            var form_review = $("#webdev_form_review").val();
            var form_id = $("#webdev_form_p_id").val();

            var form_subject = $("#review-category:checked").val();
            var form_from = $("#webdev_form_from").val()

            var stars;

            $(".stars:checked").each(function () {
                stars = $(this).val();
            });
            
//            var form_token = $("#CaptureRecaptcha").val();

    //        console.log('First Name is ' + form_fname);
    //        console.log('Email is ' + form_email);
    //        console.log('Stars is ' + stars);

            $.ajax({
                url: ajax_object.ajaxurl,
                type: 'post',
                data: {
                    action: 'gmg_review_page_save',
                    fname: form_fname,
                    lname: form_lname,
                    email: form_email,
                    town: form_town,
                    subject: form_subject,
                    stars: stars,
                    review: form_review,
                    p_id: form_id,
                    webdev_form_from: form_from,
//                    CaptureToken: form_token,
                },
                success: function (response) {
                    if (response['foo']) {
    //                    alert( 'Success' + response['foo']);
    //                    console.log('Call response!' );
                        $('#webdev_form_response').html(response['foo']);
                        $('#webdev_form_response').css("visibility", "visible");
                        if( response['sidebar'] ){

                            console.log('Sidebar was it!' );
                            window.location.href = "/thank-you-review/";

                        } else{
                            window.location.href = "/thank-you-review/";
                        }
                    } else {
                        //                    alert( 'Problems' + response['errors']);
                        $('#webdev_form_response').html(response['errors']);
                        $('#webdev_form_response').css("visibility", "visible");
                    }

                },
                error: function (errorThrown) {
                    alert('Error' + errorThrown);
                }
            });
        });
     }
});
