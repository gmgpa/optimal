jQuery(document).ready(function ($) {
    "use strict";
    
    var slider = $('#gmg_review_block_slider');
    $('#gmg_review_block_slider').slick( {
        slidesToShow: 1,
        slidesToScroll: 1,
//        autoplay: true,
        autoplaySpeed: 3000,
        arrows: true,
    });
});
