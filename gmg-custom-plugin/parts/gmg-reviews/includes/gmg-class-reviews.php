<?php

/*
* 
* Class that handles a Review
*     
*/

class Review {
    
    private $fname;
    
    private $lname;
    
    private $email;
    
    private $review;
    
    private $id;
    
    private $subject;
    
    private $stars;
    
    public function __construct( $id ) {
        $this->id = $id;
        $this->fname = get_field( 'reviewer_fname' , $this->id );
        $this->lname = get_field( 'reviewer_lname' , $this->id );
        $this->email = get_field( 'reviewer_email' , $this->id );
    }
    
    //Print Review
    public function show_review(){
        
//        error_log('Show review! for ID ' . $this->id );
        
        $review_html = array();
        
        $reviews = new Reviews();
        
        array_push( $review_html, '<div id="review-' . $this->id . ' ' . $this->get_stars() . '" class="review-block" style="background-color: ' . $reviews->get_background_color() . '">' );
        
        $star_count = $this->get_stars();
        
        if( $star_count != null && $star_count != 0 ){
            
            array_push( $review_html, '<div class="star-box">' );
    
            for( $i = 1; $i <= $star_count; $i++ ){
                
                array_push( $review_html, '<i class="fa fa-star" aria-hidden="true"></i>' );
            
            }
            
            array_push( $review_html, '</div>' );
            
        }
        
        if( $this->get_review_text() != null ) {
        
            array_push( $review_html, '<p class="customer-quote" style="color: ' . $reviews->get_font_color() . '">' . $this->get_review_text() . '</p>' );
            
        }
//        array_push( $review_html, '<p class="customer-name" style="color: ' . $this->get_font_color() . '">- ' . $this->get_fname() . ' ' . substr( $this->get_lname(), 0 , 1) . '</p>' );
        
        $name_html = array();
        
        array_push( $name_html, '<p class="customer-name" style="color: ' . $reviews->get_font_color() . '">' . $this->get_fname() . ' ' . substr( $this->get_lname(), 0 , 1) );
        
        if( $this->get_town() != null ){
            
            array_push( $name_html, ' in ' . ucfirst( $this->get_town() ) );
        }
        
        array_push( $review_html, implode( $name_html ) . '</p>' );
        
        
        if( $this->get_town() != null ) {
            
            array_push( $review_html, '<p class="review-category" style="color: ' . $reviews->get_font_color() . '">' . $this->get_subject() . '</p>');            
        }
        
        if( $this->get_source() != null ) {
            
            array_push( $review_html, '<p class="review-source" style="color: ' . $reviews->get_font_color() . '">From ' . $this->get_source() . '</p>');            
        }
        
        array_push( $review_html, '</div>');
        
        return implode( $review_html );
        
    }
    
    
//    Create Review    
    public function update_review( $info_array ){
        
        foreach( $info_array as $key => $value ){
            update_field($key, $value, $this->id);
        }
    }
    
    //Update Title
    public function update_title(){
        
        $name = $this->get_full_name();

        $new_post = array(
            'ID'     => $this->id,
            'post_title'  => $name . ' Review',
            'post_name' => $name . ' Review'
        );

        wp_update_post( $new_post );        
    }
    
    //Update Title and Content
    public function update_title_and_content(){
        
        $name = $this->get_full_name();
        
        $review_html = array();
        
        array_push( $review_html, '<div id="review-' . $this->id . '" class="review-block slider">' );
        
        array_push( $review_html, '<div class="quote-box">' );
        
        $star_count = $this->get_stars();
        
//        error_log( 'Star cound is ' . $star_count );
        
        if( $star_count != null ){
            
            array_push( $review_html, '<div class="star-box">' );
    
            for( $i = 1; $i <= $star_count; $i++ ){
                
                array_push( $review_html, '<i class="fa fa-star" aria-hidden="true"></i>' );
            
            }
            
            array_push( $review_html, '</div>' );
            
        }
        
        array_push( $review_html, '<p class="customer-quote-slider">' . $this->get_review_text() . '</p>' );
        
        array_push( $review_html, '</div>' );
        
        array_push( $review_html, '<div class="customer-box">' );
        array_push( $review_html, '<p class="customer-name-slider">- ' . $this->get_fname() . ' ' . substr( $this->get_lname(), 0 , 1) . '</p>' );
        
        $reviewer_town = $this->get_town();
        if( $reviewer_town != null ){
            
            array_push( $review_html, '<p class="review-category-slider">' . $this->get_subject() . ' in ' . ucfirst( $reviewer_town ) . '</p>');
            
        } else {
            
            array_push( $review_html, '<p class="review-category-slider">' . $this->get_subject() . '</p>');
            
        }
        
        array_push( $review_html, '</div>' );
        
        array_push( $review_html, '</div>');

        $new_post = array(
            'ID'     => $this->id,
            'post_title'  => $name . ' Review',
            'post_name' => $name . ' Review',
            'post_content' => implode( $review_html )
        );

        wp_update_post( $new_post );        
    }
    
//    Get Review
    
    public function get_id(){
        return $this->id;
    }
    
    public function get_fname(){
        return get_field( 'reviewer_fname' , $this->id );
    }
    
    public function get_lname(){
        return get_field( 'reviewer_lname' , $this->id );
    }
    
    public function get_full_name(){
        
        error_log('Full name is ' . $this->fname . ' ' . $this->lname );
        
        return $this->fname . ' ' . $this->lname;
    }
    
    public function get_email(){
        return $this->email;
    }
    
    public function get_review(){
        return get_field( 'review' , $this->id );
    }
    
    public function get_review_text(){
        return get_field( 'review' , $this->id );
    }
    
    public function get_town(){
        return get_field( 'reviewer_town' , $this->id );
    }
    
    public function get_subject(){
        return get_field( 'subject' , $this->id );
    }
    
    public function get_source(){
        return get_field( 'reviewer_source' , $this->id );
    }

    public function get_stars(){
        return get_field( 'review_rating' , $this->id );
    }
    
    public function get_review_submitted(){
        return get_field( 'review_submitted' , $this->id );
    }
    
    public function get_review_info(){
        
        return array(
            'fname'             => $this->get_fname(),
            'lname'             => $this->get_lname(),
            'email'             => $this->get_email(),
            'reviewer_town'     => $this->get_town(),
            'review'            => $this->get_review_text(),
            'stars'             => $this->get_stars(),
            'subject'           => $this->get_subject(),
            );
        
    }
    
    
    //    Set Review
    
    public function set_fname( $fname ){
        update_field( 'reviewer_fname', $fname, $this->id );
    }
    
    public function set_lname( $lname ){
        update_field( 'reviewer_lname', $lname, $this->id );
    }
    
    public function set_email( $email ){
        update_field( 'reviewer_email', $email, $this->id );
    }
    
    public function set_town( $town ){
        update_field( 'reviewer_town', $town, $this->id );
    }
    
    public function set_review( $review ){
        update_field( 'review', $review, $this->id );
    }
    
    public function set_subject( $subject ){
        
        error_log( 'Subject will be set to ' . $subject );
        
        //check to see if it's text or a number
        if( is_numeric( $subject ) ){
            
            $cat = get_term_by( "id", $subject, 'category' );

        } else {
        
            $cat = get_term_by( "name", $subject, 'category' );
            
        }
        
//        error_log( 'Gotten Cat is ' . $cat->term_id );

        update_field( 'subject', array( $cat->name ), $this->id );
        
        wp_set_post_categories( $this->id, array( $cat->term_id ), true );
        
    }

    public function set_stars( $stars ){
        update_field( 'review_rating', array( $stars ), $this->id );
    }
    
    public function set_review_submitted( $status ){
        update_field( 'review_submitted', $status, $this->id );
    }
    
}

/*
* 
* Class that handles many Reviews
*     
*/

class Reviews {
    
    public function __construct( ) {}
    
    //Get Reputation Mangement Settings
    
    public function get_background_color(){
        return get_field( 'review_block_background_color' , 'rm_options' );
    }

    public function get_font_color(){
        return get_field( 'review_block_text_color' , 'rm_options' );
    }
    
    public function get_email_platform(){
        return get_field( 'rm_email_platform', 'rm_options' );
    }
    
    //get all subjects
    public function get_subjects(){
        
        $category_names = array();
        
        // Get the ID of the parent category, which is reviews.
        $category = get_category_by_slug( 'reviews' );

        // Then get it's children.
        $gmg_categories = get_term_children( $category->term_id, 'category' );

        // If there are categories (which there always should be), then do something.
        if( $gmg_categories ){

            foreach( $gmg_categories as $gmg_category ){
                
                $cat_array = array(
                    'cat_id' => $gmg_category,
                    'cat_name'  => get_term_by( 'id' , $gmg_category, 'category' )->name
                );
                
                array_push( $category_names, $cat_array );

            }
            
            return $category_names;
            
        } else{
            
            return false;
        }
    }
    
    //Get number of reviews
    public function get_number_of_reviews(){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reviews' ),
            'posts_per_page'         => '-1',
            'status'                => 'published' );
        
        return count( get_posts( $args ) );
        
    }
    
    //    Check Reviews    
    public function check_if_review_exists( $review ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reviews' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'review',
                    'value'   => $review,
                    'compare' => '=',
                ),
            ),
        );
        
        if( get_posts( $args ) ){
            return true;            
        } else{ return false; }
        
    }
    
    public function get_review( $review ){
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reviews' ),
            'post_status'            => array( 'publish' ),
            'meta_query'             => array(
                array(
                    'key'     => 'review',
                    'value'   => $review,
                    'compare' => '=',
                ),
            ),
        );
        
        return get_posts( $args )[0];        
    }
    
    
    public function create_new_review( $info_array ){ //SOLID!
        
        //set status as pending
        $status = 'pending';
        
        //If stars are 5, we're going to outright publish it
        if( $info_array['stars'] == '5' ){
            
            $status = 'publish';
            $info_array['review_submitted'] = 'yes';
            
        } else {
            
            $info_array['review_submitted'] = 'no';
            
            
        }
        
        //set arguments for the post
        $my_post = array(
            'post_title'        => $info_array['fname'] . ' ' . $info_array['lname'] . ' Review',
            'post_type'         => 'reviews',
            'post_status'       => $status
        );
        
        //create post
        $new_id = wp_insert_post( $my_post );
        
        //dump any meta into the post
        foreach( $info_array as $key => $value ){
            
            //these keys need to have an extra item added to them to get them into the right fields.
            if( in_array( $key, array('fname', 'lname', 'email' )  ) ){
                
                update_field( 'reviewer_' . $key, $value, $new_id );
                
            } else {
                
                //Otherwise, these are fine.
                update_field( $key, $value, $new_id );

            }
            
        }
        
        //Instantiate the Review Class
        $review = new Review( $new_id );
        
        //Set Stars
        $review->set_stars( $info_array['stars'] );

        //Set Subject
        $review->set_subject( $info_array['subject'] );
        
        //update content.
        $review->update_title_and_content();
        
        //return ID
        return $new_id;
    }
    
    
    public function get_all_reviews(){
         
        $reviews = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reviews' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'title',
            'order'                  => 'ASC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                $title = get_the_title();
                
                array_push( $reviews, 
                           array( $id  => $title )
                           );                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $reviews;        
    }    
    
    //Get review ids
    public function get_all_reviews_ids(){
         
        $reviews = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reviews' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'title',
            'order'                  => 'ASC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                $title = get_the_title();
                
                array_push( $reviews, $id  );                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $reviews;        
    }
    
    
    public function get_all_reviews_by_qty( $qty , $paged ){
         
        $reviews = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'reviews' ),
            'posts_per_page'         => $qty,
            'paged'                  => $paged,
            'orderby'                => 'date',
            'order'                  => 'DESC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                
                array_push( $reviews, $id  );                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $reviews;        
    }
    
    
    public function scrape_google_reviews( $start, $page ){
    
        //set the array to put everything into
        $answers = array();

        $gmg_web = new GMG_Review_Scraper();
        
        $dom = $gmg_web->gmg_review_scraper_call_new_page( $start );

//            echo var_dump( $dom->plaintext );

        if( $dom ) {

            $new_answers = $this->process_reviews_page( $dom );
            $answers = array_merge_recursive( $new_answers, $answers );
            
            //Get next link
            foreach( $dom->find('ul.pagination li.next a') as $next ){
                
//                echo '<p>' . $next->href .'</p>';
                
                if( $next->href ){
                    
                    $new_answers = $this->scrape_google_reviews( 'https://birdeye.com' . $next->href, $page++ );
                    $answers = array_merge_recursive( $new_answers, $answers );
                }
            }
        }
        
        usort($answers, function ($a, $b) {
            $dateA = DateTime::createFromFormat('Y-m-d H:i:s', $a['date']);
            $dateB = DateTime::createFromFormat('Y-m-d H:i:s', $b['date']);
            // ascending ordering, use `<=` for descending
            return $dateA >= $dateB;
        });
        
        return $answers;
    }
    
    
    public function process_reviews_page( $dom ){
        
        //set the array to put everything into
        $answers = array();

        //Get review blocks
        foreach( $dom->find('.Review__reviewCardWrapper__11smG') as $review_div ){
            
            //Start the array to put this product into
            $new_row = array();

//                    echo var_dump( $review_div );

            //Get review blocks
            foreach( $review_div->find('.Review__contentWrapper__2NQN3') as $review ){

//                        echo var_dump( $review->plaintext );

                $span_index = 0;

                //Get reviewer name
                foreach( $review->find('span') as $name ){

                    if( $span_index == 0 ){
                        
                        $name_parts = explode(" ", $name->plaintext );
                        $new_row["reviewer_fname"] = ucwords( $name_parts[0] );
                        $new_row["reviewer_lname"] = ucwords( $name_parts[1] );

                    }

                    $span_index++;

                }

                //Get the source
                foreach( $review->find('a') as $source ){
                    
                    if( strpos($source->plaintext, "on ") ){
                        
                        $source_plain = str_replace( "on ",  "", $source->plaintext );
                        $new_row["reviewer_source"] = trim( $source_plain );
                        
                    } else {

                        $new_row["reviewer_source"] = trim( $source->plaintext );
                        
                    }

                }

                //Get the number of stars
                $star_count = 0;

                foreach( $review->find('.RatingStar__be-c-star__24d1B') as $stars ){

                    $star_count++;

                }

                $new_row["review_rating"] = $star_count;

                //Get the date

                foreach( $review->find('.Review__dateWrapper__7sEKQ') as $raw_date ){

                    $date_math = $this->process_review_date( $raw_date->plaintext );

//                            echo var_dump( $date_math );

                    $dt = new DateTime();

                    $new_row["date"] = $dt->modify( $date_math )->format('Y-m-d H:i:s');

                }

                //Get the review
                $span_index = 0;

                //Get reviewer name
                foreach( $review->find('.Review__reviewPara__2qFYA') as $r_text ){

                    if( $span_index == 0 ){

//                                echo var_dump( $name->plaintext );

                        $new_row["review"] = $r_text->plaintext;

                    }

                    $span_index++;

                }


                //Put it all in an array
                array_push( $answers, $new_row );

            }

        }        
        return $answers;
    }
    
    
    public function process_review_date( $date_text ){
        
        $date_text_parts = explode(" ", $date_text );
        
        $number_part = '';
        $variable_part = '';
        
        foreach( $date_text_parts as $date_part ){
            
//            echo var_dump( $date_part );
            
            switch( $date_part ){
                    
                case "month":
//                    echo 'month';
                    $variable_part = "month";
                    break;
                    
                case "months":
//                    echo 'month';
                    $variable_part = "months";
                    break;
                    
                case "year":
//                    echo 'year';
                    $variable_part = "year";
                    break;
                    
                case "years":
//                    echo 'year';
                    $variable_part = "years";
                    break;
                    
                case "day":
//                    echo 'day';
                    $variable_part = "day";
                    break;
                
                case "days":
//                    echo 'day';
                    $variable_part = "days";
                    break;
                    
                case "a":
//                    echo 'a';
                    $number_part = "1";
                    break;
                    
                case is_numeric( $date_part ):
//                    echo 'is number';
                    $number_part = $date_part;
                    break;
                    
            }
        }
        
        return "-" . $number_part . " " . $variable_part;
    }
    
    
    public function insert_new_review( $review ){
        
        $my_post = array(
            'post_title'        => $review['reviewer_fname'] . ' ' . $review['reviewer_lname'] . ' Review',
            'post_type'         => 'reviews',
            'post_status'       => 'publish',
            'post_date'         => $review['date']            
        );
        
        $new_id = wp_insert_post( $my_post );
        
        foreach( $review as $key => $value ){
            
            update_field($key, $value, $new_id );
        }
        
        $review = new Review( $new_id );
        
        $review->update_title_and_content();
        
        return $new_id;      
    }
    
    public function get_last_used(){        
        $fields = get_fields('rm_options');
        if( $fields['last_used'] ){
            return $fields['last_used'];
        } else {
            return false;
        }
    }

    public function set_last_used(){        
        $today_date = new DateTime();
        $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
        update_field( 'last_used', $today_date->format('F j, Y'), 'rm_options' );
    }

    public function set_good_upload_file( $good ){        
        $file = fopen( plugin_dir_path( __FILE__ ) . 'goodUpload.txt', "w" );
        fwrite( $file, json_encode( $good, JSON_PRETTY_PRINT) );
        fclose($file);
    }

    public function set_bad_upload_file( $error ){
        $file = fopen( plugin_dir_path( __FILE__ ) . 'badUpload.txt', "w" );
        fwrite( $file, json_encode( $error, JSON_PRETTY_PRINT) );
        fclose($file);
    }

    public function check_upload_file( $file_name ){
        if( file_exists( plugin_dir_path( __FILE__ ) . $file_name ) ){
            return true;
        } else {
            return false;
        }
    }

    public function delete_upload_file( $file_name ){
        if( file_exists( plugin_dir_path( __FILE__ ) . $file_name ) ){
            unlink( plugin_dir_path( __FILE__ ) . $file_name );
        }
    }
    
}

/*
* 
* Extend CampaignMonitor functionality for Reviews if Campaign Monitor exists
*
* Status: TESTING
*     
*/

if( class_exists( 'CampaignMonitor' ) ){
    
    class ReviewCM extends CampaignMonitor{

        private $api_key;
        private $client_id;

        function __construct() {
            
            parent::__construct();

            //If you have it already saved, then create it.
            if( get_field('cm_client_id_key' , 'rm_options') != null ){
                
                //grab the client ID key
                $this->client_id = get_field('cm_client_id_key' , 'rm_options');
                
                //grab the API key
                $this->api_key = get_field('cm_client_api_key' , 'rm_options');
            }
        }
        
        /*
        *
        * Function for creating subscriber array
        *
        * Status: TESTING
        *
        */
        
        public function reviewCM_create_subscriber( $info, $custom_fields ){
            
            return array(
                'EmailAddress'  => $info['email'],
                'Name'          => $info['fname'] . ' ' . $info['lname'],
                'CustomFields'  => $custom_fields,
                'ConsentToTrack' => "Yes",
                'Resubscribe' => true,
                'RestartSubscriptionBasedAutoresponders' => true,
            );
            
        }

        /*
        *
        * Unsure what this is for
        *
        * Status: UNSURE
        *
        */
        
        public function gmg_cm_reviews_get_lists(){

            $this->read_client_file();

//            error_log( 'API key is ' . $this->api_key );

            $wrap = new CS_REST_Clients(
                
                $this->client_id,
                array( 'api_key' => $this->api_key )
            );
            
            $result = $wrap->get_lists();
    //        echo "Result of /api/v3.1/clients/{id}/lists\n<br />";
            
            if( $result->was_successful() ) {

                $list_array = array();
                $result_array = $result->response;
                foreach( $result_array as $result ){
                    
                    if( is_object( $result ) ){
                        
                        $list_info_array = json_decode( json_encode( $result ) );
                        
                        $list_array[ $list_info_array->ListID ] = $list_info_array->Name;
                    }

                }
                
                return $list_array;

            }
        }
        
        /*
        *
        * Parse lists of Lists for backend choices
        *
        * Status: TESTING
        *
        */
        
        public function gmg_cm_reviews_parse_list_choices( $field ){
            
            $list_array = $this->gmg_cm_reviews_get_lists();
            
            if( is_array( $list_array ) AND count( $list_array ) != 0) {
            
                $field['choices'] = $list_array;
            }
            
            return $field;
        }
        
        /*
        *
        * Send Customer down to CM and into Leave a Review List
        *
        * Status: SOLID
        *
        */
          
        public function gmg_cm_reviews_add_customer( $info ){

            //read in the subscriber PHP file to be able to call its functions
            $this->read_subscriber_file();
            
            //Get the id for the Leave a Review List
            $list_id = get_field( 'leave_a_review_list_id' , 'rm_options' );

//            error_log('Name is ' . $info['fname'] . ' ' . $info['lname'] . ', email is ' . $info['fname'] . ' ' . $info['email'] . ', List ID is ' . $list_id );
//            error_log('Client API Key is ' . $this->api_key );

            //create a wrapper to see if subscriber is in the list already.
            $wrap = new CS_REST_Subscribers(
                $list_id,
                array( 'api_key' => $this->api_key ) );

            //make the call
            $result = $wrap->get( $info['email'] , true );

            //see if you found the customer in the list
            if( $result->was_successful() ){

//                error_log('Update Subscriber to Leave a Review');
                
                $subscriber = $this->reviewCM_create_subscriber( $info, array() );
                
                $result = $wrap->update( $info['email'] , $subscriber );
                
                if($result->was_successful()) {
                    
                    error_log( "CM Dashboard Update Subscriber Leave A Review with code ". $result->http_status_code);
                    
                } else {
                    
                    error_log( 'CM Dashboard Update Failed Leave A Review with code '. $result->http_status_code );

                }
                
                return $result;
            
            } else {

//                error_log('Add Subscriber to Leave a Review');
                
                $subscriber = $this->reviewCM_create_subscriber( $info, array() );

                $result = $wrap->add( $subscriber );
                
                if( $result->was_successful() ) {
                    
                    error_log( "CM Dashboard Add Subscriber Leave A Review with code ". $result->http_status_code);
                    
                } else {
                    
                    error_log( 'CM Dashboard Add Failed Leave A Review  ith code '. $result->http_status_code );

                }
                
                return $result;
                
            }        
        }
        
        /*
        *
        * Remove Customer from Leave a Review List
        *
        * Status: SOLID
        *
        */
          
        public function gmg_cm_reviews_remove_list( $info ){

            //read in the subscriber PHP file to be able to call its functions
            $this->read_subscriber_file();
            
            //Get the ID for the Leave a Review List
            $lr_list_id = get_field( 'leave_a_review_list_id' , 'rm_options' );
            
//            error_log('Name is ' . $info['fname'] . ' ' . $info['lname'] . ', email is ' . $info['fname'] . ' ' . $info['email'] . ', possibly removing customer from List ' . $lr_list_id  );
//            error_log('Client API Key is ' . $this->api_key );
            
            
            /*
            Check whether subscriber is on the Leave a Review list already.
             */   

            //create a wrapper
            $lr_wrap = new CS_REST_Subscribers(
                $lr_list_id,
                array( 'api_key' => $this->api_key ) );
            
            //make the call
            $lr_result = $lr_wrap->get( $info['email'], true );
            
            //See if they were found.
            if( $lr_result->was_successful() ){
                
                //If so, then delete them from the list.

//                error_log('Deleting Subscriber from list');
                
                $result = $lr_wrap->delete( $info['email'] );
                
                if( $result->was_successful() ) {
                    
                    error_log( "CM Delete Subscriber from Leave A Review with code ". $result->http_status_code);
                    
                } else {
                    
                    error_log( 'CM Delete Subscriber from Leave a Review Failed with code '. $result->http_status_code );

                }
                
                return $result;
            
            } else {

                error_log('Not on Leave a Review list');
                
                return false;
            }
            
        }
        
        /*
        *
        * Send Customer down to CM and into Social Media Nudge
        *
        * Status: SOLID
        *
        */
          
        public function gmg_cm_reviews_send_nudge( $info ){

            //read in the subscriber PHP file to be able to call its functions
            $this->read_subscriber_file();
            
            //Also get the ID for the Send Social Media Nudge List
            $sn_list_id = get_field( 'social_media_nudge_list_id' , 'rm_options' );    
            
            /*
            Check whether subscriber is on the Send Nudge list already.
             */ 
            
            //create a wrapper.
            $sn_wrap = new CS_REST_Subscribers(
                $sn_list_id,
                array( 'api_key' => $this->api_key ) );
            
            //make the call
            $sn_result = $sn_wrap->get( $info['email'], true );
            
            /*
            See if we found the customer in Send Social Media Nudge List
            */
            
            $custom_fields = array();
            
            /*
            Let's add the Review Text if one exists.\
            *
            * array(
            *        'Key' => 'ReviewText',
            *        'Value' => $info['review'] 
            *        )
            *
            */
            
            if( $info['review'] != '' ){
                
                array_push( $custom_fields, array(
                    'Key' => 'ReviewText',
                    'Value' => $info['review'] )
                           );
            }
            
            //see if you found the customer in the list
            if( $sn_result->was_successful() ){

//                error_log('Update Subscriber on Social Media Nudge');
                
                $subscriber = $this->reviewCM_create_subscriber( $info, $custom_fields );

                $result = $sn_wrap->update( $info['email'] , $subscriber );
                
                if( $result->was_successful() ) {
                    
                    error_log( "CM SMN Update Subscriber with code ". $result->http_status_code);
                    
                } else {
                    
                    error_log( 'CM SMN Update Failed with code '. $result->http_status_code );

                }
                
                return $result;
            
            } else {

//                error_log('Add Subscriber to Social Media Nudge');
                
                $subscriber = $this->reviewCM_create_subscriber( $info, $custom_fields );

                $result = $sn_wrap->add( $subscriber );
                
                if( $result->was_successful() ) {
                    
                    error_log( "CM SMN Add Subscriber with code ". $result->http_status_code);
                    
                } else {
                    
                    error_log( 'CM SMN Add Failed with code '. $result->http_status_code );

                }
                
                return $result;
                
            }
        }
        
        
    }
}

/*
* 
* Class for scraping review data
* 
* Status: SOLID
*
*/

class GMG_Review_Scraper{
    
    public function __construct() { }

    public function gmg_review_scraper_go_curl( $base ){

        $response = false;

        // set up curl
        $curl = curl_init();

        // the url to request
        curl_setopt( $curl, CURLOPT_URL, $base );           

        // return to variable
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true ); 

        // decompress using GZIP
        curl_setopt( $curl, CURLOPT_ENCODING, '');

        // don't verify peer ssl cert
        curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );

        // fetch remote contents, check for errors
        if ( false === ( $response = curl_exec( $curl ) ) )
            $error = curl_error( $curl );

        // close the resource
        curl_close( $curl );

        if ( !$response ){

            return false;

        } else {

            return $response;
        }
    }
    
    public function gmg_review_scraper_encode( $text ){
        
        if( $text == "â€³" ){
            
            return str_replace( "â€³", '"', $text );
            
        }
        
        return $text;
        
    }
    
    public function gmg_review_scraper_call_new_page( $link ){
        
        $response = $this->gmg_review_scraper_go_curl( $link );
        
//        echo var_dump( $response );

        if( $response ){

            $html = new simple_html_dom();
            $dom = $html->load( $response );

            if( !empty($dom) ) {
                
                return $dom;
                
            }
        }
        
        else return false;
    }
    
}

/*
* 
* Class for sending data to SharpSpring
* 
* Status: SOLID
*
*/

if( class_exists( 'GMG_SharpSpring' ) ){
    
//    error_log('SharpSpring Class exists!' );

    class ReviewSS extends GMG_SharpSpring {

        private $baseURL;
        private $endPoint;

        //construct the class
        public function __construct( $base, $end ) {
            
            error_log( 'Base: ' . $base );
            error_log( 'Endpoint: ' . $end );
            
            $this->baseURL = $base;
            $this->endPoint = $end;
        }
        
        /*
        * 
        * Sends data to SharpSpring endpoint that launches contact into Reputation Management Workflow
        * 
        * Status: SOLID
        *
        */
        
        public function gmg_review_dashboard_enter_sharpspring( $info ){
            
            $sharpspring = new GMG_SharpSpring( $this->baseURL, $this->endPoint );
            $parameters = $sharpspring->gmg_parse_sharpspring( $info );
            $result = $sharpspring->gmg_sharpspring_curl( $parameters );
            
            if( $result ){
                
                error_log( "Sharpspring subscribed with code " . $result['http_code'] );
                $reviews = new Reviews();
                $reviews->set_last_used();
                
            } else {
                
                error_log( 'Sharpspring failed with code '. $result['http_code'] );
            }
        }
        
        /*
        *
        * Sends data to SharpSpring endpoint that launches contact into Start Social Media Nudge Workflow
        * 
        * Status: SOLID
        *
        */
        
        public function gmg_review_manual_enter_sharpspring( $info ){ //SOLID
            
            $sharpspring = new GMG_SharpSpring( $this->baseURL, $this->endPoint );
            $parameters = $sharpspring->gmg_parse_core_sharpspring( $info );
            $updated_parameters = $this->gmg_review_parse_extra_parameters( $info, explode( "&" , $parameters ) );            
            $result = $sharpspring->gmg_sharpspring_curl( $updated_parameters );
            
            if( is_array( $result ) ){
                
                error_log( "Sharpspring Manually entered with code " . $result['http_code'] );
                $reviews = new Reviews();
                $reviews->set_last_used();
                
            } else {
                
                error_log( 'Sharpspring Manual failed with code '. $result['http_code'] );
            }
            
        }
        
        /*
        *
        * Adds two parameters that Rep Management needs and any extra ones
        * that this client's version of Rep Management needs
        * 
        * Status: SOLID
        *
        */
        
        public function gmg_review_parse_extra_parameters( $info , $parameters ){
            
            //Always need these:
            
            if( isset( $info['stars'] ) ) {

                array_push( $parameters, "reviewStars=" . urlencode( $info['stars'] ) );
            }

            if( isset( $info['review'] ) ) {

                array_push( $parameters, "reviewText=" . urlencode( $info['review'] ) );
            }
            
            //Space for extra ones for client:
            
//            error_log( 'Parameters are: ' . implode( "&" , $parameters ) );

            return implode( "&" , $parameters );

        }
    
    }

} else {
    
//    error_log('SharpSpring Class does not exists!' );
    
    
}