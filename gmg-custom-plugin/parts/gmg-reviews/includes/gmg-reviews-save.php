<?php

/*
*
* Someone presses "update" or "publish" within a Review post.
*
* This processes the review, creating content from the elements of the post
* then updating the post.
*
* Then sends them down to SharSpring to enter Start Social Media Nudge
*
* Status: SOLID
*/

add_filter('acf/save_post' , 'gmg_review_backend_save', 10, 1 );
function gmg_review_backend_save( $post_id ) {
    
//    error_log( 'Back end save with a post that is ' . get_post_type( $post_id ) );
    
    if( !is_page() ){
    
        if( get_post_type( $post_id ) == 'reviews' ){
            
            //instantiate the Reviews Class
            $reviews = new Reviews();
            
            //instantiate the Review Class
            $review = new Review( $post_id );
            
            //Process Categories
            $review->set_subject( $review->get_subject() );
            
            //If calling wp_update_post, unhook this function so it doesn't loop infinitely
            remove_action('save_post', 'gmg_review_backend_save');

            //Update all the review content
            $review->update_title_and_content();

            // re-hook this function
            add_action('save_post', 'gmg_review_backend_save');
            
            if( $reviews->get_email_platform() != "none" && get_post_status( $post_id ) == 'publish' ){
            
                //check to see if the reviewer has been sent down already.
                if( $review->get_review_submitted() == 'no' || $review->get_review_submitted() == '' || $review->get_review_submitted() == null ){

                    //if not, do so.

                    //First, we need to figure out where we're sending it.
                    if( $reviews->get_email_platform() == 'sharpspring' ){

                        //If SharpSpring, then do this.
                        $base = get_field( 'sharpspring_base', 'rm_options' );
                        $end = get_field( 'sharpspring_sm_nudge_form_endpoint', 'rm_options' );

                        //Instantiate the Review SharpSpring Class
                        $sharpspring = new ReviewSS( $base, $end );                    

                        //Get an array full of data
                        $info_array = $review->get_review_info();

                        //send it down
                        $sharpspring->gmg_review_manual_enter_sharpspring( $info_array );

                    } else {

                        //If Campaign Monitor, then do this.
                        $campaign = new ReviewCM();

                        //Get an array full of data
                        $info_array = $review->get_review_info();

                        //Send Nudge
                        $campaign->gmg_cm_reviews_send_nudge( $info_array );

                    }

                    //update to say that it was submitted
                    $review->set_review_submitted('yes');

                }
            }
        }
    }
}