<?php

add_filter( 'gform_pre_render_5', 'gmg_reviews_populate_categories' );
add_filter( 'gform_pre_validation_5', 'gmg_reviews_populate_categories' );
add_filter( 'gform_pre_submission_filter_5', 'gmg_reviews_populate_categories' );
function gmg_reviews_populate_categories( $form ) {
    
//    error_log( 'Inside Pre Render with Form');
 
    foreach( $form['fields'] as &$field )  {
 
        //NOTE: replace 11 with your select field id
        $field_id = 10;
        if ( $field->id != $field_id ) {
            continue;
        }
        
        //Instantiate Reviews Class
        $reviews = new Reviews();

        //grab all subjects
        $review_cats = $reviews->get_subjects();
        
        //        echo var_dump( $review_cats );
        
        //Creating item array.
        $items = array();

        //Adding categories to select items
        foreach( $review_cats as $review_cat ){
            
            $items[] = array( 'value' => $review_cat['cat_name'], 'text' => $review_cat['cat_name'] );
        }

        //Adding items to field id
        foreach ( $form['fields'] as &$field ) {
            if ( $field->id == $field_id ) {
                $field->choices = $items;
            }
        }
        
        return $form;
    }
}