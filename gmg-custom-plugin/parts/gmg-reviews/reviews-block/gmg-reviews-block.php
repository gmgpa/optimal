<?php

add_action('acf/init', 'gmg_reviews_block_init');
function gmg_reviews_block_init() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// register a gmg-form block
		acf_register_block(array(
			'name'				=> 'gmg-reviews-block',
			'title'				=> __('GMG Reviews Block'),
			'description'		=> __('A block for showing Reviews in a Slick Slider.'),
			'render_callback'	=> 'gmg_acf_reviews_block_render_callback',
			'category'			=> 'design',
			'icon'				=> 'editor-table',
			'keywords'			=> array( 'GMG', 'Reviews' ),
		));
	}
}

function gmg_acf_reviews_block_render_callback( $block ) {
	
    // convert name ("acf/gmg-form") into path friendly slug ("gmg-form")
	$slug = str_replace('acf/', '', $block['name']);
	
	// include a template part from within the "template-parts/block" folder
	if( file_exists( plugin_dir_path( __FILE__ ) . "template-parts/block/content-{$slug}.php") ) {
		include( plugin_dir_path( __FILE__ ) . "template-parts/block/content-{$slug}.php" );
	}
}