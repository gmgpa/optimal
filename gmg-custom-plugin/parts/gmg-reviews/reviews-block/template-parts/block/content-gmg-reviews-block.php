<?php
/**
 * Block Name: GMG Reviews Block
 *
 * This is the template that displays the gmg-reviews-block block.
 */

$number = get_field( 'number_of_reviews' );

$html_array = array();

// WP_Query arguments
$args = array(
    'post_type'              => array( 'reviews' ),
    'posts_per_page'         => $number,
    'orderby'                => 'date',
    'order'                  => 'DESC' );
        
// The Query
$query = new WP_Query( $args );

//echo var_dump( $query );

// The Loop
if ( $query->have_posts() ) {
    
//    array_push( $html_array, '<div class="gmg-review-block-slider" id="gmg_review_block_slider"' );
//    echo '<div class="gmg-review-block-slider" id="gmg_review_block_slider">';
    
    ?>

    <div class="gmg-review-block-slider" id="gmg_review_block_slider">
        
    <?php


    while ( $query->have_posts() ) {
        
        $query->the_post();
        
        $content = apply_filters( 'the_content', get_the_content() );
        
//        array_push( $html_array, '<div>' . get_the_content() . '</div>' );
//        echo '<div>' . get_the_content() . '</div>';
        echo "<div>$content</div>";
    
    }
    
    // Restore original Post Data
    wp_reset_postdata();

}

//array_push( $html_array, '</div>' );
//echo '</div>';

//echo var_dump( $html_array );

//return implode('' , $html_array );

?>

</div>

