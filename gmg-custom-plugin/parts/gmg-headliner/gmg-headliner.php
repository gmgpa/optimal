<?php


//Modified genesis_do_post_title function that inputs our custom headline.
//add_action('genesis_entry_header','gmg_do_page_headline', 9);
function gmg_do_page_headline() {
    
    global $post;

    
    $title = get_post_meta( $post->ID, 'headline_text', true );
    
    error_log( 'Entry header is ' . $title );

	if( $title != '' ) {

	// Wrap in H1 on singular pages.
	$wrap = is_singular() ? 'h1' : 'h2';

	// Also, if HTML5 with semantic headings, wrap in H1.
	$wrap = genesis_html5() && genesis_get_seo_option( 'semantic_headings' ) ? 'h1' : $wrap;

	/**
	 * Entry title wrapping element
	 *
	 * The wrapping element for the entry title.
	 *
	 * @since 2.2.3
	 *
	 * @param string $wrap The wrapping element (h1, h2, p, etc.).
	 */
	$wrap = apply_filters( 'genesis_entry_title_wrap', $wrap );

	// Build the output.
	$output = genesis_markup( array(
		'open'    => "<{$wrap} %s>",
		'close'   => "</{$wrap}>",
		'content' => $title,
		'context' => 'entry-title',
		'params'  => array(
			'wrap'  => $wrap,
		),
		'echo'    => false,
	) );

	echo apply_filters( 'genesis_post_title_output', "$output \n", $wrap, $title );
        
    }

}