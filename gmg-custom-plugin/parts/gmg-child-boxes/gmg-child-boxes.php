<?php

/**
 * GMG Child Boxes
 *
 * @package           gmg-child-boxes
 * @author            Raymond M Rose
 * @license           GPL-2.0+
 * @link              http://goodgroupllc.com/
 * @copyright         2017 Good Marketing Group
 *
 * @wordpress-plugin
 * Plugin Name:       GMG Child Boxes
 * Plugin URI:        http://goodgroupllc.com/
 * Description:       For Parent Pages, creates category boxes to put at top of the page.
 * Version:           1.0.2
 * Author:            Raymond M Rose
 * Author URI:        http://goodgroupllc.com/
 * Text Domain:       gmg-child-boxes
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

$cb = plugin_dir_path( __FILE__ );

//Let's also bring in the meta boxes and all that jazz.
require_once($cb . '/includes/gmg-child-box-meta.php');

add_action('acf/init', 'my_acf_child_init');
function my_acf_child_init() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// register a gmg-form block
		acf_register_block(array(
			'name'				=> 'gmg-child',
			'title'				=> __('GMG Child'),
			'description'		=> __('A block for showing GMG Child Boxes.'),
			'render_callback'	=> 'gmg_child_acf_block_render_callback',
			'category'			=> 'formatting',
			'icon'				=> 'editor-table',
			'keywords'			=> array( 'GMG', 'Child Box' ),
		));
	}
}

function gmg_child_acf_block_render_callback( $block ) {
	
	// convert name ("acf/gmg-form") into path friendly slug ("gmg-form")
	$slug = str_replace('acf/', '', $block['name']);
	
	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/block/content-{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/block/content-{$slug}.php") );
	}
}