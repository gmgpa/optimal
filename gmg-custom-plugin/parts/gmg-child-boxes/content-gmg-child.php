<?php
/**
 * Block Name: GMG Form
 *
 * This is the template that displays the gmg-form block.
 */

//global
global $post;

// create id attribute for specific styling
$id = 'gmg_child_' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

$p_id = get_field( 'parent_page' );

//$continue = get_field('turn_on' );

if( $p_id ){

    $args = array(
        'post_parent' => $p_id,
        'post_type'   => 'page', 
        'numberposts' => -1,
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'order' => 'ASC'
    );

    if ( get_children( $args ) ) {

        $children = get_children( $args );
        
        $div_class = 'wp-block-columns has-2-columns child-box';

        ?>

            <div class="child-boxes">

            <?php foreach($children as $child): ?>

            <div style="height:30px" aria-hidden="true" class="wp-block-spacer"></div>

            <div class="<?php echo $div_class; ?> ">

                <div class="wp-block-column">
                    <figure class="wp-block-image">
                        <a href="<?php echo get_permalink( $child->ID ); ?>">
                        <img src="<?php echo get_the_post_thumbnail_url( $child->ID, 'full-size' ); ?>" />
                        </a>
                    </figure>
                </div>
                <div class="wp-block-column">
                    <a href="<?php echo get_permalink( $child->ID ); ?>">
                        <h4><?php echo $child->post_title; ?></h4>
                    </a>
                    <?php if( get_the_excerpt( $child->ID ) ): ;?>
                        <p><?php echo get_the_excerpt( $child->ID ); ?></p>
                    <?php endif; ?>
                    <a href="<?php echo get_permalink( $child->ID ); ?>" class="button">Learn More</a>
                </div>

            </div>
            <?php endforeach; ?>

            </div>
    <?php

    }
    
}
//}
