<?php

add_action( 'add_meta_boxes', 'GMG_Child_Box_add_metabox');

function GMG_Child_Box_add_metabox() {
        
        	add_meta_box(
                'child-box-meta',      // Unique ID
                esc_html__( 'Category Area', 'text_domain' ),    // Title
                'GMG_Child_Box_render_metabox',   // Callback function
                'page',         // Admin page (or post type)
                'normal',         // Context
                'high'         // Priority
            );
}

function GMG_Child_Box_render_metabox( $post ) {

		// Add nonce for security and authentication.
		wp_nonce_field( 'child_box_data', 'child_box_nonce' );

		// Retrieve an existing value from the database.
		$child_box_header = get_post_meta( $post->ID, 'child_box_header', true );
        $child_box_text = get_post_meta( $post->ID, 'child_box_text', true );
//        $child_box_footer = get_post_meta( $post->ID, 'child_box_footer', true );
        

		// Set default values.
		if( empty( $child_box_header ) ) $child_box_header = '';
        if( empty( $child_box_text ) ) $child_box_text = '';
//        if( empty( $child_box_footer ) ) $child_box_footer = '';

//    Form fields
        
       echo '<table class="form-table">';
        
        //Header
        echo '<tr>'; 
        echo '<th><label for="child_box_header" class="child_box_header_label">' . __( 'Category Header (Optional)', 'text_domain' ) . '</label></th>';     
        echo '<td>';
    echo '<input type="text"  id="child_box_header" name="child_box_header" style="width: 99%;" class="child_box_header_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $child_box_header ) . '">';
        
        echo '</td>';
        echo '</tr>';
        
        //Title
        echo '<tr>'; 
        echo '<th><label for="child_box_text" class="child_box_text_label">' . __( 'Category Text (Optional)', 'text_domain' ) . '</label></th>';     
        echo '<td>';
        echo '<textarea rows="2" cols="50" id="child_box_text" name="child_box_text" style="width: 99%;" class="child_box_text_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '">' . esc_textarea( $child_box_text ) . '</textarea>';
        echo '</td>';
        echo '</tr>';
        
        echo '</table>';
}

add_action( 'save_post', 'GMG_Child_Box_save_metabox', 10, 2 );

	function GMG_Child_Box_save_metabox( $post_id, $post ) {
        
        if( get_post_type( $post_id ) == 'page'){
        
            if( isset( $_POST['child_box_nonce'] ) ){

                // Add nonce for security and authentication.
                $cb_nonce_name   = $_POST['child_box_nonce'];
                $cb_nonce_action = 'child_box_data';

                // Check if a nonce is set.
                if ( ! isset( $cb_nonce_name ) )
                    return;

                // Check if a nonce is valid.
                if ( ! wp_verify_nonce( $cb_nonce_name, $cb_nonce_action ) )
                    return;

                // Check if the user has permissions to save data.
                if ( ! current_user_can( 'edit_post', $post_id ) )
                    return;

                // Check if it's not an autosave.
                if ( wp_is_post_autosave( $post_id ) )
                    return;

                // Check if it's not a revision.
                if ( wp_is_post_revision( $post_id ) )
                    return;

                // Sanitize user input.
        //        $child_box_new_address = isset( $_POST[ 'child_box_address' ] ) ? sanitize_text_field( $_POST[ 'child_box_address' ] ) : '';
                $child_box_new_header = isset( $_POST[ 'child_box_header' ] ) ? wp_kses_post( $_POST[ 'child_box_header' ] ) : '';
                $child_box_new_title = isset( $_POST[ 'child_box_text' ] ) ? wp_kses_post( $_POST[ 'child_box_text' ] ) : '';
        //		$child_box_new_footer = isset( $_POST[ 'child_box_footer' ] ) ? sanitize_text_field( $_POST[ 'child_box_footer' ] ) : '';

                // Update the meta field in the database.
                update_post_meta( $post_id, 'child_box_header', $child_box_new_header );
                update_post_meta( $post_id, 'child_box_text', $child_box_new_title );
        //        update_post_meta( $post_id, 'child_box_footer', $child_box_new_footer );
            }
        }
	}
