<?php
/**
 * Block Name: GMG Form
 *
 * This is the template that displays the gmg-form block.
 */

//global
global $post;

// create id attribute for specific styling
$id = 'gmg_form_' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

if( class_exists( 'GMG_Forms' ) ){
    
    $f_id = get_field('gmg_form' );
//    echo var_dump( $f_raw );
    $form = new GMG_Form( $f_id );
    echo $form->show_form( 'shop_contact' , 'contact-form', 'contact-form', $post->ID );
    
}