<?php

function slides_init() {
	$labels = array(
		'name'               => _x( 'Slides', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Slide', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Slides', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Slide', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'slide', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Slide', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Slide', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Slide', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Slide', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Slides', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Slides', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Slides:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No slides found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No slides found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
        'label'                 => __( 'Slides', 'your-plugin-textdomain' ),
		'description'           => __( 'Custom Post Type for Slides', 'your-plugin-textdomain' ),
		'labels'                => $labels,
		'supports'              => array('thumbnail', 'title', 'editor', 'page-attributes'),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => 'gmg-custom-plugin',
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-images-alt2',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'slides', $args );
}
add_action( 'init', 'slides_init' );

//Change the name of the featured image metabox to slide
add_action('do_meta_boxes', 'replace_featured_image_box');

function replace_featured_image_box()
{
	remove_meta_box( 'postimagediv', 'slides', 'side' );
	add_meta_box('postimagediv', __('Slide Image (Recommended Size 1800 x 1000)'), 'post_thumbnail_meta_box', 'slides', 'advanced', 'high');
}

// Move all "advanced" metaboxes above the default editor
add_action('edit_form_after_title', function() {
	global $post, $wp_meta_boxes;
	do_meta_boxes(get_current_screen(), 'advanced', $post);
	unset($wp_meta_boxes[get_post_type($post)]['advanced']);
});

add_action( 'load-post.php', 'init_slide_metabox' );
add_action( 'load-post-new.php', 'init_slide_metabox' );

function init_slide_metabox() {
    
    add_action( 'add_meta_boxes', 'gmg_slide_add_metabox' );
    add_action( 'save_post', 'gmg_slide_save_metabox', 10, 2 );
}

function gmg_slide_add_metabox() {
    
    add_meta_box(
        'slide-meta',      // Unique ID
        esc_html__( 'Slide Content', 'text_domain' ),    // Title
        'gmg_slide_render_metabox',   // Callback function
        'slides',         // Admin page (or post type)
        'advanced',         // Context
        'default'         // Priority
    );

}

function gmg_slide_render_metabox( $post ) {
    
// Add nonce for security and authentication.
    wp_nonce_field( 'slide_data', 'slide_nonce' );
        
    $slide_headline = get_post_meta( $post->ID, 'slide_headline', true );
    $slide_post_text = get_post_meta( $post->ID, 'slide_post_text', true );        
    $slide_link = get_post_meta( $post->ID, 'slide_link', true );
    
    echo '<ul>';

    //Slide Headline
    echo '<li><strong>Enter a Headline: </strong><input type="text" id="slide_headline" name="slide_headline" class="slide_headline_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr( $slide_headline ) . '"></li>';
    
//    echo $slide_post_text;

    //Slide Post Text    
    echo '<li><strong>Enter some post text. One or two short sentences: </strong><textarea id="slide_post_text" name="slide_post_text" rows="2" cols="100" class="slide_post_text_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '">' . esc_textarea( $slide_post_text ) . '</textarea></li>';

    //Slide Button
    echo '<li><strong>Enter link URL: </strong><input type="text" id="slide_link" name="slide_link" class="slide_link_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr( $slide_link) . '"></li>';
    
    echo '</ul>';
}

function gmg_slide_save_metabox( $post_id, $post ) {

    // Add nonce for security and authentication.
    $slide_nonce_name   = $_POST['slide_nonce'];
    $slide_nonce_action = 'slide_data';

    //Check it's not an auto save routine
     if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
          return;

    //Perform permission checks! For example:
    if ( !current_user_can('edit_post', $post_id) ) 
          return;
    
    // Check if a nonce is set.
    if ( ! isset( $slide_nonce_name ) )
        return;

    // Check if a nonce is valid.
    if ( ! wp_verify_nonce( $slide_nonce_name, $slide_nonce_action ) )
        return;

    // Check if it's not a revision.
    if ( wp_is_post_revision( $post_id ) )
        return;


    // Sanitize user input for slide contact name.
    $slide_new_headline = isset( $_POST[ 'slide_headline' ] ) ? sanitize_text_field( $_POST[ 'slide_headline' ] ) : '';
    $slide_new_post_text = isset( $_POST[ 'slide_post_text' ] ) ? htmlspecialchars( $_POST[ 'slide_post_text' ] ) : '';
    $slide_new_link = isset( $_POST[ 'slide_link' ] ) ? sanitize_text_field( $_POST[ 'slide_link' ] ) : '';

    // Update the meta field in the database.
    update_post_meta( $post_id, 'slide_headline', $slide_new_headline );
    update_post_meta( $post_id, 'slide_post_text', $slide_new_post_text );
    update_post_meta( $post_id, 'slide_link', $slide_new_link );
    
    $content = array();
    
    array_push( $content, '<div class="slider-box">' );
    array_push( $content, '<div class="slider-text-box">' );
    array_push( $content, '<h1>'. $slide_new_headline . '</h1>' );
    array_push( $content, '<p>'. $slide_new_post_text . '</p>' );
    array_push( $content, '</div>' );
    array_push( $content, '<div class="slider-circle-box">' );
    array_push( $content, '<a href="'. $slide_new_link . '" class="slider-circle">' );
    array_push( $content, '<span class="fa-stack fa-3x"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-arrow-down fa-stack-1x fa-inverse" aria-hidden="true"></i></span>' );
    array_push( $content, '</a>' );
    array_push( $content, '</div>' );
    array_push( $content, '</div>' );
    
    //If calling wp_update_post, unhook this function so it doesn't loop infinitely
    remove_action('save_post', 'gmg_slide_save_metabox');
    
    $new_post = array(
        'ID'     => $post_id,
        'post_title'    => $slide_new_headline,
        'post_content'  => implode($content)
    );
    
    wp_update_post( $new_post );
    
        // re-hook this function
    add_action('save_post', 'gmg_slide_save_metabox');
    
}


// add order column to admin table list of posts
function add_new_post_column($cpt_columns) {
	$cpt_columns['menu_order'] = "Order";
	return $cpt_columns;
}
add_action('manage_slides_posts_columns', 'add_new_post_column');

//Show custom order column values for slides
function show_order_column($name){
	global $post;

	switch ($name) {
		case 'menu_order':
			$order = $post->menu_order;
			echo $order;
			break;
		default:
			break;
	}
}
add_action('manage_slides_posts_custom_column','show_order_column');


//Make Column Sortable for slides
function order_column_register_sortable($columns){
	$columns['menu_order'] = 'menu_order';
	return $columns;
}
add_filter('manage_edit-slides_sortable_columns','order_column_register_sortable');

//Get Featured Image
function gmg_get_featured_image($post_ID) {
	$post_thumbnail_id = get_post_thumbnail_id($post_ID);
	if ($post_thumbnail_id) {
		$post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail');
		return $post_thumbnail_img[0];
	}
}
// Add Image Column for slides
function gmg_columns_head($defaults) {
	$defaults['featured_image'] = 'Slide Image';
	return $defaults;
}

// Populate Featured Image Column for slides
function gmg_columns_content($column_name, $post_ID) {
	if ($column_name == 'featured_image') {
		$post_featured_image = gmg_get_featured_image($post_ID);
		if ($post_featured_image) {
			echo '<img src="' . $post_featured_image . '" />';
		}
	}
}

add_filter('manage_slides_posts_columns', 'gmg_columns_head');
add_action('manage_slides_posts_custom_column', 'gmg_columns_content', 10, 2);

//Change the name of the featured image metabox to Gallery Thumbnail
add_action('do_meta_boxes', 'replace_featured_image_box_gallery');
function replace_featured_image_box_gallery()
{
	remove_meta_box( 'postimagediv', 'gallery', 'side' );
	add_meta_box('postimagediv', __('Gallery Thumbnail (Recommended Size 200x200 or larger)'), 'post_thumbnail_meta_box', 'gallery', 'advanced', 'high');
}
