<?php


// Move all "advanced" metaboxes above the default editor
add_action('edit_form_after_title', function() {
	global $post, $wp_meta_boxes;
	do_meta_boxes(get_current_screen(), 'advanced', $post);
	unset($wp_meta_boxes[get_post_type($post)]['advanced']);
});

//Add Custom Post Type For Recipients
function recipients_init() {
	$labels = array(
		'name'               => _x( 'Recipients', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Recipient', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Recipients', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Recipient', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'recipient', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Recipient', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Recipient', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Recipient', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Recipient', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Recipients', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Recipients', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Recipients:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No recipients found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No recipients found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'public' => true,
		'labels'  => $labels,
		'supports' => array('title', 'page-attributes'),
		'menu_icon' => 'dashicons-email',
		'taxonomies' => array( 'category' ),
	);
	register_post_type( 'recipients', $args );

}
add_action( 'init', 'recipients_init' );

// add order column to admin table list of posts
function gmg_recipients_add_new_post_column($cpt_columns) {
	$cpt_columns['menu_order'] = "Order";
	return $cpt_columns;
}
add_action('manage_recipients_posts_columns', 'gmg_recipients_add_new_post_column');

//Show custom order column values for Recipients
function gmg_recipients_show_order_column($name){
	global $post;

	switch ($name) {
		case 'menu_order':
			$order = $post->menu_order;
			echo $order;
			break;
		default:
			break;
	}
}
add_action('manage_recipients_posts_custom_column','gmg_recipients_show_order_column');


//Make Column Sortable for Recipients
function gmg_recipients_order_column_register_sortable($columns){
	$columns['menu_order'] = 'menu_order';
	return $columns;
}
add_filter('manage_edit-recipients_sortable_columns','gmg_recipients_order_column_register_sortable');


/* Fire our meta box setup function on the recipients editor screen. */
add_action( 'init', 'gmg_recipients_meta_boxes_setup' );

function gmg_recipients_meta_boxes_setup(){

	/* Add meta boxes on the 'add_meta_boxes' hook. */
	add_action( 'add_meta_boxes', 'gmg_recipients_add_recipient_meta_boxes' );
	/* Save post meta on the 'save_post' hook. */
	add_action( 'save_post', 'gmg_save_recipient_name_meta', 10, 2);
	add_action( 'save_post', 'gmg_save_recipient_email_meta', 10, 2);
}

/* Create Recipients Custom Meta Boxes */
function gmg_recipients_add_recipient_meta_boxes(){

	add_meta_box(
		'recipient-name',      // Unique ID
		esc_html__( 'Recipient Name', 'outreach' ),    // Title
		'gmg_recipient_name_meta_box',   // Callback function
		'Recipients',         // Admin page (or post type)
		'advanced',         // Context
		'default'         // Priority
	);

	add_meta_box(
		'recipient-email',      // Unique ID
		esc_html__( 'Recipient Email Address', 'outreach' ),    // Title
		'gmg_recipient_email_meta_box',   // Callback function
		'Recipients',         // Admin page (or post type)
		'advanced',         // Context
		'default'         // Priority
	);
}

/* Display the page recipient meta box. */
function gmg_recipient_name_meta_box( $object, $box ) { ?>

    <?php wp_nonce_field( basename( __FILE__ ), 'gmg_recipient_name_nonce' ); ?>

    <p>
        <label for="recipient-name"><?php _e( "Recipient Name:", 'Outreach Pro' ); ?></label>
        <br />
        <input class="widefat" type="text" name="recipient-name" id="recipient-name" value="<?php echo esc_attr( get_post_meta( $object->ID, 'recipient_name', true ) ); ?>" size="30" />
    </p>
    <?php }

/* Display the recipient name meta box. */
function gmg_recipient_email_meta_box( $object, $box ) { ?>

    <?php wp_nonce_field( basename( __FILE__ ), 'gmg_recipient_email_nonce' ); ?>

    <p>
        <label for="recipient-email"><?php _e( "Recipient Email:", 'Outreach Pro' ); ?></label>
        <br />
        <input class="widefat" type="text" name="recipient-email" id="recipient-email" value="<?php echo esc_attr( get_post_meta( $object->ID, 'recipient_email', true ) ); ?>" size="30" />
    </p>
    <?php }


/* Save the meta box's post metadata. */
function gmg_save_recipient_name_meta( $post_id, $post ) {
    
    	/* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );
    
    if( $post->post_type == 'recipients' ){

        /* Verify the nonce before proceeding. */
        if ( !isset( $_POST['gmg_recipient_name_nonce'] ) || !wp_verify_nonce( $_POST['gmg_recipient_name_nonce'], basename( __FILE__ ) ) )
            return $post_id;

        /* Check if the current user has permission to edit the post. */
        if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
            return $post_id;

        /* Get the posted data and sanitize it for use as an HTML class. */
        $new_meta_value = ( isset( $_POST['recipient-name'] ) ? sanitize_text_field( $_POST['recipient-name'] ) : '' );

        /* Get the meta key. */
        $meta_key = 'recipient_name';

        /* Get the meta value of the custom field key. */
        $meta_value = get_post_meta( $post_id, $meta_key, true );

        /* If a new meta value was added and there was no previous value, add it. */
        if ( $new_meta_value && '' == $meta_value )
            add_post_meta( $post_id, $meta_key, $new_meta_value, true );

        /* If the new meta value does not match the old value, update it. */
        elseif ( $new_meta_value && $new_meta_value != $meta_value )
            update_post_meta( $post_id, $meta_key, $new_meta_value );

        /* If there is no new meta value but an old value exists, delete it. */
        elseif ( '' == $new_meta_value && $meta_value )
            delete_post_meta( $post_id, $meta_key, $meta_value );
        
    }
}

/* Save the meta box's post metadata. */
function gmg_save_recipient_email_meta( $post_id, $post ) {
    
    /* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );
        
    if( $post->post_type == 'recipients' ){

        /* Verify the nonce before proceeding. */
        if ( !isset( $_POST['gmg_recipient_email_nonce'] ) || !wp_verify_nonce( $_POST['gmg_recipient_email_nonce'], basename( __FILE__ ) ) )
            return $post_id;

        /* Check if the current user has permission to edit the post. */
        if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
            return $post_id;

        /* Get the posted data and sanitize it for use as an HTML class. */
        $new_meta_value = ( isset( $_POST['recipient-email'] ) ? sanitize_text_field( $_POST['recipient-email'] ) : '' );

        /* Get the meta key. */
        $meta_key = 'recipient_email';

        /* Get the meta value of the custom field key. */
        $meta_value = get_post_meta( $post_id, $meta_key, true );

        /* If a new meta value was added and there was no previous value, add it. */
        if ( $new_meta_value && '' == $meta_value )
            add_post_meta( $post_id, $meta_key, $new_meta_value, true );

        /* If the new meta value does not match the old value, update it. */
        elseif ( $new_meta_value && $new_meta_value != $meta_value )
            update_post_meta( $post_id, $meta_key, $new_meta_value );

        /* If there is no new meta value but an old value exists, delete it. */
        elseif ( '' == $new_meta_value && $meta_value )
            delete_post_meta( $post_id, $meta_key, $meta_value );
        
    }
}

//Change category to Receives Mail From:
add_action('add_meta_boxes_recipients','add_recipients_meta');
function add_recipients_meta(){
		remove_meta_box( 'categorydiv', 'recipients', 'side' );
		add_meta_box('categorydiv', __('Receives Mail From: '), 'post_categories_meta_box', 'recipients', 'normal', 'high');
}