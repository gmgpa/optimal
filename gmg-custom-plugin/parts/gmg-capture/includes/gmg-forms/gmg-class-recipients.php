<?php

class Recipients{
    
    public function __construct() {
        
    }
    
    public function get_all_recipients(){
        
        $recipients = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'recipients' ),
            'posts_per_page'         => '-1',
            'post_status' => 'publish',
            'orderby' => 'menu_order',
            'order'                  => 'ASC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                
                array_push( $recipients, 
                           array( 'name'    => get_post_meta( $id , "recipient_name" , true),
                                 'email'    => get_post_meta( $id , "recipient_email" , true)
                                )
                           );
                
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $recipients;
    }
}