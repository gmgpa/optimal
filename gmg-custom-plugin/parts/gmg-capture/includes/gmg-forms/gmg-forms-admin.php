<?php

add_action('wp_ajax_gmg_contact_service_mail', 'gmg_capture_many_forms');
add_action('wp_ajax_nopriv_gmg_contact_service_mail', 'gmg_capture_many_forms');
function gmg_capture_many_forms() {
    
	if( isset( $_POST['CaptureHidden' ] ) ) {
        
        error_log( 'Contact Service Mail' );
    
        $errors = array();

        $capture_form_first_name = $_POST['CaptureFName'];
        $capture_form_last_name = $_POST['CaptureLName'];

        $capture_form_phone = $_POST['CapturePhone'];
        $capture_form_email = $_POST['CaptureEmail'];
        
        $capture_form_message = $_POST['CaptureMessage'];
        
        if( empty($capture_form_first_name) || empty($capture_form_email) || empty($capture_form_message) ){
            $errors[] = 'Name, email and message are required';
            error_log( 'No name' );
        }

//        $capture_form_street = $_POST['street'];
//        $capture_form_city = $_POST['city'];
//        $capture_form_state = $_POST['state'];
//        $capture_form_zip = $_POST['zip'];        

        $capture_form_addressee = $_POST['CaptureRecipient'];

//        $capture_form_response = $_POST['captcha'];
    
//        error_log( $capture_form_addressee );
    

//        elseif(empty($capture_form_response)){
//            $errors[] = 'ReCaptcha is not set.';
//            error_log( 'No recaptcha' );
//        }
//        elseif( filter_var($capture_form_email, FILTER_VALIDATE_EMAIL) === false ){
//            $errors[] = 'That\'s not a valid email address';
//            error_log( 'No email' );
//        }
    
//    If there are no errors, build the email from the form contents and then mail it
    if(empty($errors)){
        
//        $secret = '6LeuLn0UAAAAAIWXQDEF3GENO91sFHSPwAfDXAWe';
//        
//        $data = array(
//            'secret' => $secret,
//            'response' => $capture_form_response
//        );
//        
//        $verify = curl_init();
//        curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
//        curl_setopt($verify, CURLOPT_POST, true);
//        curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
//        curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
//        $response = curl_exec($verify);
//        $recaptcha = json_decode($response, true);
        
//        error_log( 'Response is ' . $response );
    
//        if ($recaptcha['success']){           
        
            $message = "FROM: " . $capture_form_first_name . " "  . $capture_form_last_name . "\n\n" .
//                "ADDRESS: " . $capture_form_street . "\n" .
//                $capture_form_city . ", " . $capture_form_state . " " . $capture_form_zip . "\n\n" .
                "PHONE NUMBER: " . $capture_form_phone . "\n\n" .
                "EMAIL: " . $capture_form_email . "\n\n" .
//                "PRODUCT: " . $capture_form_product . "\n\n" .
//                "DAYS AVAILABLE: " . $capture_form_days . "\n\n" .
//                "TIMES: " . $capture_form_times . "\n\n" .
                "MESSAGE: " . $capture_form_message;

//            error_log( 'Ready to send Mail' );

            $site_name = get_bloginfo( 'name' );
            $site_email = get_bloginfo( 'admin_email' );
//            $site_email = 'info@acmestove.com';
//            $site_email = 'ray@goodmarketinggroup.com';

            $success_message = '<p>Your message has been sent</p>';
            $subject = "New contact from on $site_name";
            $headers   = array();
            $headers[] = "MIME-Version: 1.0";
            $headers[] = "Content-type: text/plain; charset=iso-8859-1";
            $headers[] = "From: $site_name <$site_email>";
            $headers[] = "Bcc: Forms <forms@goodmarketinggroup.com>";
            $headers[] = "Reply-To: {$capture_form_email}";
            $headers[] = "Subject: {$subject}";
            $headers[] = "X-Mailer: PHP/".phpversion();

            $answer = wp_mail($capture_form_addressee, $subject, $message, implode("\r\n", $headers));

            error_log( 'Answer is ' . $answer );

            if ($answer){
                $return = array ( 'foo' => $success_message );
                error_log( 'Success' );
            } else {
                $return = array ( 'foo' => 'There was an error sending the message.' );
                error_log( 'Failure' );
            }

            echo wp_send_json( $return );
            error_log( 'Sent Json' );
        } else {
            error_log( 'Errors were found' );
            echo wp_send_json( array ( 'errors' => 'Recaptcha did not find you worthy.' ) );
        }
    }
    else {
        error_log( 'Errors were found' );
        echo wp_send_json( array ( 'errors' => $errors[0] ) );
        }
        exit();
//    }
}