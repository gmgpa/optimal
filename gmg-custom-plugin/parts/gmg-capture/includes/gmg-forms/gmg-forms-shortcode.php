<?php

add_shortcode('request-quote', 'gmg_write_request_quote');

function gmg_write_request_quote( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'id' => '',
		),
		$atts,
		'request-quote'
	);
    
    global $post;
    
    $forms = new GMG_Forms();
    $return_html = array();
    
    $ids = $forms->find_form_with_shortcode( 'request-quote' );
    error_log( 'ID is ' . $ids[0] . ' for ' . get_the_title( $ids[0] ) );
    
    if( $ids != false ){
        
        $form = new GMG_Form( $ids[0] );
        array_push( $return_html ,  $form->show_form( 'gmg_capture_form' , 'contact-form', 'shop_contact', $post->ID ) );
        return implode( $return_html );
        
    } else {
        
        array_push( $return_html, '<p>Form not created at this time.</p>'  );
    }
    
    return implode( $return_html );    
    
}

add_shortcode('service-forms', 'gmg_write_service_form');
function gmg_write_service_form(){
    
//    error_log( 'Called Service Form!');
    
    global $post;
    
    $forms = new GMG_Forms();
    $html_array = array();
    
    $ids = $forms->find_form_with_shortcode( 'service-forms' );
    error_log( 'ID is ' . $ids[0] . ' for ' . get_the_title( $ids[0] ) );
    
    if( $ids != false ){
        
//        error_log( 'IDs were not false!' );
        
//        $content = get_post_field('post_content', $ids[0]);
//        if( !empty( $content ) ){
//            array_push( $html_array , apply_filters( 'the_content', $content ) );
//        }
        
        $form = new GMG_Form( $ids[0] );
        array_push( $html_array ,  $form->show_form( 'service_form' , 'service-form', 'service-form', $post->ID ) );
        return implode( $html_array );
        
    } else {
        
        return '<p>No form was found.</p>';
    }
    
}

add_shortcode('contact-form', 'gmg_write_contact_form');
function gmg_write_contact_form(){
    
//    error_log( 'Called Contact Form!');
    
    $forms = new GMG_Forms();
    $html_array = array();
    
    $ids = $forms->find_form_with_shortcode( 'contact-form' );
    
    global $post;
//    error_log( 'ID is ' . $ids[0] . ' for ' . get_the_title( $ids[0] ) );
    
    if( $ids != false ){
        
        $content = get_post_field('post_content', $ids[0]);
        if( !empty( $content ) ){
            array_push( $html_array , apply_filters( 'the_content', $content ) );
        }
        
        $form = new GMG_Form( $ids[0] );
        array_push( $html_array ,  $form->show_form( 'contact_form' , 'contact-form', 'contact-form', $post->ID ) );
        return implode( $html_array ); 
    } else {
        
        return '<p>No form was found.</p>';
    }
    
}
    