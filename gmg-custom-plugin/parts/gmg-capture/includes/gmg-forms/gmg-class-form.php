<?php

class GMG_Form{
    
    private $id;
    private $functions = array(
        'show_gmg_form_name',
        'show_gmg_form_customer_type',
        'show_gmg_form_company_name',
        'show_gmg_form_email',
        'show_gmg_form_phone',
        'show_gmg_response_options',
        'show_gmg_form_street_address',
        'show_gmg_form_address_parts',
        'show_gmg_form_dates',
        'show_gmg_form_birthdate',
        'show_gmg_form_interests',
        'show_gmg_form_checkboxes',
        'show_gmg_form_custom_text',
        'show_gmg_form_question',
    );
    
    public function __construct( $id ) {
        $this->id = $id;
    }
    
    /*
    Update ____________________________________
    */
    
    public function update_title(){
        
        $form_name_array = explode( ' ', $this->get_gmg_form_name() );
        
        if( in_array( 'form' , $form_name_array ) ){
            
            $post_title = implode( ' ' , $form_name_array );
            
        } else {
            
            $post_title = implode( ' ' , $form_name_array ) . ' Form';
            
        }

        $update_post = array(
            'ID'     => $this->id,
            'post_title'  => $post_title,
            'post_name' => $post_title
        );

        wp_update_post( $update_post );        
    }
    
    /*
    Get _______________________________________
    */
    
    //Get Form Post Tags
    public function get_gmg_form_tags(){        
        return wp_get_post_tags( $this->id );        
    }
    
    //Get Form Opening HTML
    public function get_gmg_form_html_start($class , $form_id ){
        $form_name = str_replace( ' ' , '_', get_the_title( $this->id ) );
        return '<form name="' . $form_name . '" class="' . $class . '" method="post" action="" id="' . $form_id . '" >';
    }
    
    //Get Form Closing HTML
    public function get_gmg_form_html_close(){
        return '</form>';
    }
    
    //Get Form Waiting gif
    public function get_gmg_form_wait_gif(){
        return '<img id="sign-up-wait" src="https://goodmarketinggroup.com/wp-content/uploads/2018/03/Loading_icon.gif" width="100" style="display: none;" />';
    }
    
    //Get Form Name
    public function get_gmg_form_name(){
        return get_field( 'form_name', $this->id );
    }
    
    //Get Form Name Label
    public function get_gmg_form_name_label(){
        return get_field( 'name_label' , $this->id );
    }
    
    //Get Form Company Name Label
    public function get_gmg_form_company_name_label(){
        return get_field( 'company_name_label' , $this->id );
    }
    
    //Get Form Email Label
    public function get_gmg_form_email_label(){
        return get_field( 'email_label' , $this->id );
    }
    
    //Get Form Phone Number Label
    public function get_gmg_form_phone_number_label(){
        return get_field( 'phone_number_label' , $this->id );
    }
    
        //Redirect or Reset
    public function get_gmg_form_redirect_or_reset(){
        
//        error_log( 'The value of Yay or nay ' . get_field( 'yay_or_nay', $this->id ) );
//        error_log( 'The return url is ' . get_field( 'go_to_url', $this->id ) );
        
        if ( get_field( 'yay_or_nay', $this->id ) ){
            
            return $this->get_gmg_form_thank_you_page();
            
        } else {
            
            return get_the_permalink( $this->id );
             
        }
    }
    
    //Get Form Thank You Page
    public function get_gmg_form_thank_you_page(){
        
        $return = get_field( 'go_to_url' , $this->id );
//        error_log( 'Return in database is ' . $return );
        
        return ( $return != '' ) ? get_site_url() . $return : get_site_url() . '/thank-you';
        
    }
    
    //Check and Get Form List
    public function show_gmg_form_list(){
        
        if( get_field( 'form_campaign_monitor_list', $this->id ) ){
            
            return '<input type="hidden" name="CaptureFormList" id="CaptureFormList" value="' . get_field('form_campaign_monitor_list', $this->id ) . '" />';
            
        }
    }
    
    //Check and Show Recipients
    public function show_gmg_form_recipients(){
        
        if( get_field( 'show_recipients', $this->id ) ){
            
            $html_return = array();
            $recipient = new Recipients();
            $recipients = $recipient->get_all_recipients();
            
            array_push( $html_return, print_label( '*Message Subject:' )  );
            array_push( $html_return, '<select name="CaptureRecipients" id="CaptureRecipients" required>');
            
            //loop over each post
            foreach( $recipients as $recipient_post ){
                
                array_push( $html_return, '<option value="' . $recipient_post['email'] . '">' . $recipient_post['name'] . '</option>');
                
            }
            
            array_push( $html_return, '</select>' );
            
            return implode( $html_return );
            
        } elseif( get_field( 'form_recipient', $this->id ) ){
            
            return '<input type="hidden" name="CaptureRecipient" id="CaptureRecipient" value="' . get_field('form_recipient', $this->id ) . '" />';
            
        }
    }
    
    //Check and Get First Name and Placement
    public function show_gmg_form_name(){
        
        if( get_field( 'show_name', $this->id ) ){
            
            $html_return = array();
            $label = get_field( 'name_label', $this->id );
            
            if( get_field( 'name_required', $this->id ) ){
                $required = 'required';
                $title = $label . ' *';
            } else{
                $required = '';
                $title = $label;
            }
            
            array_push( $html_return , '<div class="form-names">' );
            array_push( $html_return , print_label( $title ) );
            array_push( $html_return , print_text_field( $required, 'CaptureFName', 'First', 'names' ) );
            array_push( $html_return , print_text_field( $required, 'CaptureLName', 'Last', 'names' ) );
            array_push( $html_return , '</div>' );
            
            return array( get_field( 'name_position', $this->id ),
                         implode( $html_return ) );
        }
    }
    
    //Check and Get Customer Type
    public function show_gmg_form_customer_type(){
        
        if( get_field( 'add_customer_type', $this->id ) ){
            
            $title = get_field( 'customer_type_label', $this->id );
            $html_return = array();
            array_push( $html_return , '<div class="label-input gmg-form-boxes">' );
            
            $types = get_field( 'customer_type_choices', $this->id );
            
            if( is_array( $types ) ){
                
                $count = count( $types ) + 1;
                $percent = 100/$count - 1;
                
                array_push( $html_return , '<div class="gmg-form-box radio label" style="width: ' . $percent . '%;">' . print_label( $title ) . '</div>' );
                
                foreach( $types as $type ){
                    
                    array_push( $html_return , '<div class="gmg-form-box radio" style="width: ' . $percent . '%;"><input type="radio" id="CaptureCustomer" name="CaptureCustomer" value="' . $type['types'] . '" /><strong>' . $type['types'] . '</strong></div>' );
                    
                }
                
            }else{
                
                array_push( $html_return , '<div class="gmg-form-box manual label">' . print_label( $title ) . '</div>' );
                
                array_push( $html_return , '<div class="gmg-form-box manual label"><input type="radio" id="CaptureCustomer" name="CaptureCustomer" value="' . $types['types'] . '" /><strong>' . $type['types'] . '</strong></div>' );
                
            }
            
            array_push( $html_return , '</div>' );
            
            return array( get_field( 'customer_type_position', $this->id ),
                         implode( $html_return ) );
        }
    }
    
    //Check and Get Response Options
    public function show_gmg_response_options(){
      
        if( get_field( 'show_response_options', $this->id ) ){
            
            $html_return = array();            
            $levels = get_field( 'response_options_levely', $this->id );
            $index = 0;
            
            if( is_array( $levels ) ){
                
                foreach( $levels as $level ){
                    
                    array_push( $html_return , '<div class="label-input gmg-form-boxes">' );                    
                    $choices = $level['level_choices'];
                    
                    if( is_array( $choices ) ){
                        
                        $count = count( $choices ) + 1;
                        $percent = 100/$count - 1;
                        
                        switch( $level['level_type']){
                            case 'options':
                                $selector = 'CaptureOptions';
                                break;
                            case 'times':
                                $selector = 'CaptureTimes';
                                break;
                            case 'days':
                                $selector = 'CaptureDays';
                                break;                                
                        }

                        array_push( $html_return , '<div class="gmg-form-box ' . $level['level_mode'] . ' label" style="width: ' . $percent . '%;">' . print_label( $level['level_label'] ) . '</div>' );

                        foreach( $choices as $choice ){
                            
                            array_push( $html_return , '<div class="gmg-form-box ' . $level['level_mode'] . '" style="width: ' . $percent . '%;"><input type="' . $level['level_mode'] . '" id="' . $selector . '" name="' . $selector . '" value="' . $choice['level_choice'] . '" /><strong>' . $choice['level_choice'] . '</strong></div>' );

                        }
                    }
                    
                    array_push( $html_return , '</div>' );
                }
            }
            
            return array( get_field( 'response_options_position', $this->id ),
                         implode( $html_return ) );
        }        
        
    }
    
    //Check and Get Response Options
    public function show_gmg_form_custom_text(){
      
        if( get_field( 'show_custom_text_inputs', $this->id ) ){
            
            $html_return = array();            
            $levels = get_field( 'custom_text_inputs_levels', $this->id );
            $index = 0;
            
            if( is_array( $levels ) ){
                
                foreach( $levels as $level ){
                    
                    $class = $level['level_width'];
                    
                    array_push( $html_return , '<div class="label-input custom-input ' . $class . '">' );                    
                    array_push( $html_return , '<label>' . $level['level_label'] );
                    
                    switch( $level['level_type']){
                            
                        case 'text':
                            $input = '<input type="text" name="CaptureInputs" id="CaptureInput_"' . $index . ' class="CaptureInputs" placeholder="' . $level['level_label'] . '" />';
//                            $input = '<input type="text" name="CaptureInputs" id="CaptureInputs" class="gmg-form-custom-text" placeholder="' . $level['level_label'] . '" />';
                            break;
                        case 'area':
                            $input = '<textarea name="CaptureInputs" id="CaptureInputs" placeholder="' . $level['level_label'] . '" class="CaptureInputs"></textarea>';
                            break;
                        default:
                            $input = '<input type="text" name=name="CaptureInputs" id="CaptureInputs" class="CaptureInputs" placeholder="' . $level['level_label'] . '" />';
                            break;
                    }
                    
                    array_push( $html_return , $input . '</label></div>' );                    
                    $index++;
                
                }
            
            }
            
            return array( get_field( 'custom_text_inputs_position', $this->id ), implode( $html_return ) );
        
        }        
        
    }
    
    
    //Check and Get Company Name
    public function show_gmg_form_company_name(){
        
        if( get_field( 'show_company_name', $this->id ) ){
            
            $label = get_field( 'company_name_label', $this->id );
            if( get_field( 'company_name_required', $this->id ) ){
                $required = 'required';
                $title = $label . ' *';
            }else{
                $required = '';
                $title = $label;
            }
            
            $html_return = array();
            
            array_push( $html_return , '<div class="label-input">' );
            array_push( $html_return , print_label( $title ) );
            array_push( $html_return , print_text_field( $required, 'CaptureCompanyName', $label , 'company' ) );
            array_push( $html_return , '</div>' );
            
            return array( get_field( 'company_name_position', $this->id ),
                         implode( $html_return ) );
        }
    }
    
    //Check and Get Email
    public function show_gmg_form_email(){
        
        if( get_field( 'show_email', $this->id ) ){
            
            $label = get_field( 'email_label', $this->id );
            if( get_field( 'email_required', $this->id ) ){
                
                $required = 'required';
                $title = $label . ' *';
            }else{
                $required = '';
                $title = $label;
            }
            
            $html_return = array();
            
            array_push( $html_return , '<div class="label-input">' );
            array_push( $html_return , print_label( $title ) );
            array_push( $html_return , print_text_field( $required, 'CaptureEmail', $label, 'email' ) );
            array_push( $html_return , '</div>' );
            
            return array( get_field( 'email_position', $this->id ),
                         implode( $html_return ) );
        }
    }
    
    //Check and Get Phone
    public function show_gmg_form_phone(){
        
        if( get_field( 'show_phone_number', $this->id ) ){
            
            $label = get_field( 'phone_number_label', $this->id );
            if( get_field( 'phone_number_required', $this->id ) ){
                
                $required = 'required';
                $title = $label . ' *';
            }else{
                $required = '';
                $title = $label;
            }
            
            $html_return = array();
            
            array_push( $html_return , '<div class="label-input">' );
            array_push( $html_return , print_label( $title ) );
            array_push( $html_return , print_text_field( $required, 'CapturePhone', $label, 'phone' ) );
            array_push( $html_return , '</div>' );
            
            return array( get_field( 'phone_number_position', $this->id ),
                         implode( $html_return ) );
        }
    }
    
    //Check and Get Dates
    public function show_gmg_form_dates(){
        
        if( get_field( 'add_date_picker', $this->id ) ){
            
            $label = get_field( 'date_picker_label', $this->id );
            if( get_field( 'date_picker_required', $this->id ) ){
                
                $required = 'required';
                $title = $label . ' *';
            }else{
                $required = '';
                $title = $label;
            }
            
            $html_return = array();
            $number = get_field( 'how_many_date_pickers', $this->id );
            
            $index = 0;
            while( $index < intval( $number) ){
                
                array_push( $html_return , '<div class="label-input">' );
            
                if( $index > 0 ){
                    
                    array_push( $html_return , print_label( 'Additional ' . $title ) );
                    
                } else {
                    
                    array_push( $html_return , print_label( $title ) );
                }
                
                array_push( $html_return , print_date_field( $required, 'CaptureAppt' . $index , 'appointment' ) );
                array_push( $html_return , '</div>' );
                
                $index++;
            }
            
            return array( get_field( 'date_picker_position', $this->id ),
                         implode( $html_return ) );
        }
    }
    
    //Check and Get Birthdate
    public function show_gmg_form_birthdate(){
        
        if( get_field( 'show_birthdate', $this->id ) ){
            
            $label = get_field( 'birthdate_label', $this->id );
            if( get_field( 'birthdate_required', $this->id ) ){
                
                $required = 'required';
                $title = $label . ' *';
            }else{
                $required = '';
                $title = $label;
            }
            
            $html_return = array();
            
            array_push( $html_return , '<div class="label-input">' );
            array_push( $html_return , print_label( $title ) );
            array_push( $html_return , print_date_field( $required, 'CaptureBirth' , 'birthdate' ) );
            array_push( $html_return , '</div>' );
            
            return array( get_field( 'birthdate_position', $this->id ),
                         implode( $html_return ) );
        }
    }
    
    //Check and Get Street Address
    public function show_gmg_form_street_address(){
        
        if( get_field( 'show_street_address', $this->id ) ){
            
            $label = get_field( 'street_address_label', $this->id );
            if( get_field( 'street_address_required', $this->id ) ){
                
                $required = 'required';
                $title = $label . ' *';
            }else{
                $required = '';
                $title = $label;
            }
            
            $html_return = array();
            
            array_push( $html_return , '<div class="label-input">' );
            array_push( $html_return , print_label( $title ) );
            array_push( $html_return , print_text_field( $required, 'CaptureStreetAddress', $label, 'street' ) );
            array_push( $html_return , '</div>' );
            
            return array( get_field( 'street_address_position', $this->id ),
                         implode( $html_return ) );
        }
    }
    
    //Check and Get Town, State, and Zip. This can be any or all.
    public function show_gmg_form_address_parts(){
        
//        error_log( 'Inside parts!');
        
        $count = 0;
        $go = false;
        
        $look_ups = array( 
            array( 'acf'        => 'show_town',
                  'acf2'        => 'town_required',
                  'placecard'   => 'Town',
                  'id'          => 'CaputureTown',
                  'type'        => 'text',
                  ),
            array( 'acf'        => 'show_state',
                  'acf2'        => 'state_required',
                  'placecard'   => 'State',
                  'id'          => 'CaputureState',
                  'type'        => 'select',
                  ),
            array( 'acf'        => 'show_zip_code',
                  'acf2'        => 'zip_code_required',
                  'placecard'   => 'Zip Code',
                  'id'          => 'CaputureZip',
                  'type'        => 'text',
                  ),
        );
        
        foreach( $look_ups as $look_up ){
            
            if( get_field( $look_up['acf'], $this->id ) ){
                
                $count++;
                $go = true;
//                error_log( 'Count is ' . $count );
            }
        }
        
        if( $go ){
            
            $html_return = array();
        
            array_push( $html_return , '<div class="label-input gmg-form-boxes">' );

            foreach( $look_ups as $look_up ){

                if( get_field( $look_up['acf'], $this->id ) ){

//                    error_log( 'Print is ' . $look_up['acf'] );

                    if( get_field( $look_up['acf2'], $this->id ) ){

                        $required = 'required';
                        $title = $look_up['placecard'] . ' *';

                    } else {

                        $required = 'required';
                        $title = $look_up['placecard'];

                    }
                    
                    $percent = 100/$count - 1;                    
                    array_push( $html_return , '<div class="gmg-form-box auto" style="width: ' . $percent . '%;">');

                    if( $count == 1 ){
                        
                        array_push( $html_return , print_label( $title ) );
                        
                    }
                    
                    if( $look_up['type'] == 'text' ){
                        
                        array_push( $html_return , print_text_field( $required, $look_up['id'], $look_up['placecard'], 'box' ) );
                    
                    } elseif( $look_up['type'] == 'select' ){
                            
                        $customers = new Customers();
                        array_push( $html_return , print_select_field( $required, $look_up['id'], $customers->get_states() ) );
                    
                    }
                    
                    array_push( $html_return , '</div>');
                }
            }
            
            array_push( $html_return , '</div>');
            
            return array( intval( get_field( 'street_address_position', $this->id ) ) + 1 ,
                         implode( $html_return ) );
        }
    } 
    
    //Check and show Interests
    public function show_gmg_form_interests(){
        
//        error_log( 'Show interests!' );
        
        if( get_field( 'add_interests', $this->id ) ){
            
            $html_return = array();
        
            $interests = get_field( 'other_interests', $this->id );
//            error_log( 'Interests are: ' . $interests );
//            error_log( 'Interests count: ' . count( $interests ) );
            if( is_array( $interests ) AND count( $interests ) != 0){
                
//                error_log( 'Got interests!' );
                
                $label = get_field( 'interests_label', $this->id );                
                array_push( $html_return , '<div class="label-input form-tags">' );
                array_push( $html_return , print_label( $label ) );
                array_push( $html_return , '<div class="checkboxes">' );
                
                foreach( $interests as $interest ){
                    
                    $term = $interest['interest_name'];
                    $name = ucwords( $term->name );
//                    error_log( 'Term:' . $term->name );

                    array_push( $html_return , '<div class="checkbox-unit">' );
                        array_push( $html_return , '<input id="CaptureInterests" name="' .  $name . '" type="checkbox" value="' .  $name . '">' );
                        array_push( $html_return , '<label for="' .  $name . '">' . $name . '</label>' );
                    array_push( $html_return , '</div>' );
                }
                
                if( get_field( 'add_other_interest', $this->id ) ){
                    array_push( $html_return , '<div class="checkbox-unit other">' );
                        array_push( $html_return , '<input id="CaptureInterestsOther" name="Other" type="text" class="other-input" value="">' );
                        array_push( $html_return , '<label for="Other">Other</label>' );
                    array_push( $html_return , '</div>' );
                }
                array_push( $html_return , '</div>' );
                array_push( $html_return , '</div>' );
                
                return array( get_field( 'interests_position', $this->id ),
                             implode( $html_return ) );
            }
        }
    }
    
    //Check and show Checkboxes
    public function show_gmg_form_checkboxes(){
        
        $checkboxes = get_field( 'check_boxes', $this->id );
        
        if( isset( $checkboxes ) && is_array( $checkboxes ) && count( $checkboxes ) > 0 ){
            
//            error_log( 'We are in on Checkboxes!' );
            
            $html_return = array();
            
            foreach( $checkboxes as $checkbox ){
                
                $text = $checkbox['checkbox_text'];
                
                $label = $checkbox['checkbox_label'];
                $id = str_replace( ' ' , '_' , $label );
            
                if( $checkbox['checkbox_required'] ){

                    $required = 'required';
                    $title = $label . ' *';

                }else{

                    $required = '';
                    $title = $label;

                }
                
                array_push( $html_return , '<div class="label-input one-checkbox" >' );
                array_push( $html_return , print_label( $title ) );
                array_push( $html_return , print_one_checkbox( $required, $id, $text ) );
                array_push( $html_return , '</div>' );
                
            }
            
            return array( get_field( 'check_boxes_position', $this->id ),
                             implode( $html_return ) );
        }
    }
    
    //Check and Get Messages
    public function show_gmg_form_question(){
        
        if( get_field( 'show_message_box', $this->id ) ){
            
            $label = get_field( 'message_box_label', $this->id ); 
            
            if( get_field( 'message_box_required', $this->id ) ){
                
                $required = 'required';
                $title = $label . ' *';
                
            }else{
                $required = '';
                $title = $label;
            }
            
            $html_return = array();
            
            array_push( $html_return , '<div class="label-input">' );
            array_push( $html_return , print_label( $title ) );
            array_push( $html_return , '<textarea name="CaptureMessage" id="CaptureMessage" placeholder="Leave a Message or Ask a question" ' . $required . '></textarea>');
            array_push( $html_return , '</div>' );
            
            return array( get_field( 'message_box_position', $this->id ),
                             implode( $html_return ) );
        }
    }
    
    //Show the Response Form
    public function show_gmg_form_response(){
        return '<p class="form-control" name="CaptureResponse" id="CaptureResponse" style="visibility: hidden"></p>';
    }
    
    //Show the Form ID Hidden
    public function show_gmg_form_id_hidden(){
        $form_id = get_post( $this->id )->ID;
//        error_log( 'Server Side: Form ID is ' . $form_id );
        return '<input id="CaptureFormID" name="CaptureFormID" type="hidden" style="height: 5px; padding: 0;" value="' . $form_id . '" />';
    }
    
    //Show the Any ID Hidden
    public function show_gmg_form_any_id_hidden( $id ){
        return '<input id="CaptureID" name="CaptureID" type="hidden" style="height: 5px; padding: 0;" value="' . $id . '" />';
    }
    
    //Show the Recaptcha Form
    public function show_gmg_form_recaptcha(){
        return '<input type="hidden" name="g-recaptcha-response" id="CaptureRecaptcha" value="' . get_field( 'recaptcha_site', 'overall_settings' ) . '">';
    }
    
    //Show the Form Intro
    public function show_gmg_form_opening_text(){        
        $content = get_field( 'form_intro', $this->id );
//        error_log( 'The content is ' . $content );
        return '<div class="form-intro-text">' . apply_filters( 'acf_the_content', $content ) . '</div>';
    }
    
    //Show the Submit Button
    public function show_gmg_form_button(){
        return '<div class="sign-up-submit"><img id="sign-up-wait" src="https://goodmarketinggroup.com/wp-content/uploads/2018/03/Loading_icon.gif" width="100" style="display: none;" /><button id="CaptureSubmit" class="btn btn-primary" type="submit" value="submit">Submit</button></div>';
    }
    
    //Check if Form is using shortcode
    public function get_gmg_form_shortcode(){
//        $shortcode = get_field( 'form_shortcode', $this->id );
        return preg_replace('/[^a-zA-Z\-]/', '', get_field( 'form_shortcode', $this->id ) );
//        return $shortcode;
    }
    
    //Show Opening Div
    public function get_gmg_form_div_start( $class , $form_id ){
        return '<div class="' . $class . '" id="' . $form_id . '" >';
    }
    
    //Show Closing Div
    public function get_gmg_form_div_close(){
        return '</div>';
    }
    
    //Function to process and show form
    public function show_form( $form_id , $form_name, $class, $id ){
        
        $return_html = array();
        
        array_push( $return_html, $this->get_gmg_form_html_start( $class, $form_id ) );
//        echo $this->show_gmg_form_opening_text();
        array_push( $return_html, $this->show_gmg_form_opening_text() );
        array_push( $return_html, $this->show_gmg_form_recipients() );
        
        if( isset( $id ) ){
            array_push( $return_html, $this->show_gmg_form_any_id_hidden( $id ) );
        }
        
        $form_content = array();
        $index = 55;
        foreach( $this->functions as $function ){
            $content = $this->$function();
            if( $content ){
                
                if( is_array( $content ) ){
                    
                    $form_content[ $content[0] ] = $content[1];
//                    error_log( 'Putting at position ' . $content[0] );
                    
                }else{
                    
                    $form_content[ $index ] = $content;
                    
                }
            }
            $index++;
        }
        
        ksort( $form_content );
        array_push( $return_html, implode( $form_content ) );

        array_push( $return_html, $this->show_gmg_form_id_hidden() );
        array_push( $return_html, $this->show_gmg_form_list() );
        array_push( $return_html, $this->show_gmg_form_response()  );
//        array_push( $return_html, $this->show_gmg_form_recaptcha() );
        array_push( $return_html, $this->show_gmg_form_button()  );
        
        array_push( $return_html, $this->get_gmg_form_html_close() );
        
        return implode( $return_html );
    }    
}

function print_label( $text ){
    
    return '<label>' . $text . '</label>';
}

function print_text_field( $required, $id, $placecard, $class ){
    
    return '<input type="text" name="' . $id . '" id="' . $id . '" class="' . $class . '"  placeholder="' . $placecard . '"' . $required . ' />';
}

function print_date_field( $required, $id, $class ){
    
    $today_date = new DateTime();
    $today_date->setTimezone( new DateTimeZone( 'America/New_York' ) );
    $today = $today_date->format('m-d-Y');
        
    if( $class == 'birthdate' ){

        $min = "01-01-1918";
        $max = $today;

    } else {

        $min = $today;
        $max = $today_date->add( new DateInterval('P21D') )->format('m-d-Y');
    }

    return '<input name="' . $id . '" id="' . $id . '" class="' . $class . '" type="date" value=""
        min="' . $min . '" max="' . $max . '" ' . $required . ' />';
}

function print_select_field( $required, $id, $choices ){
    
    $select_array = array();
    
    array_push( $select_array , '<select name="' . $id . '" id="' . $id . '" ' . $required . '>');
    foreach( $choices as $choice ){
        array_push( $select_array , '<option value="' . $choice . '">' . $choice . '</option>');
    }
    array_push( $select_array , '</select>');
    return implode( $select_array );
}

function print_one_checkbox( $required, $id, $text ){
    
//    error_log( 'Print Checkbox!');
    
    $checkbox_array = array();
    
    array_push( $checkbox_array , '<div class="checkbox-unit">' );
    
    array_push( $checkbox_array , '<input id="' . $id . '" name="' . $id . '" type="checkbox" value="yes" ' . $required . '> ' . $text );
    array_push( $checkbox_array , '</div>' );
    
    return implode( $checkbox_array );
}

class GMG_Forms{
    
    public function __construct() {
        
    }
    
    public function find_form_with_shortcode( $shortcode ){
        
        $forms = array();

        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'gmg_form' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'title',
            'order'                  => 'ASC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                $form = new GMG_Form( $id );
                $found_shortcode = $form->get_gmg_form_shortcode();
//                error_log( 'Found Shortcode is ' . $found_shortcode );
//                error_log( 'Shortcode is ' . $shortcode );
                if( $found_shortcode != 'none' ){
                    
                    if( strcasecmp( $found_shortcode , $shortcode ) == 0 ){
                        
                        array_push( $forms, $id );
                    }
                    
                }
            }
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        if( !empty( $forms ) ){
            
            return $forms;
            
        } else {
            
            return false;
        }

    }
    
    public function get_form_by_title( $title ){
        
        $forms = array();

        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'gmg_form' ),
            'name'                   => $title, 
            'posts_per_page'         => '-1',
            'orderby'                => 'title',
            'order'                  => 'ASC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $id = get_the_ID();
                array_push( $forms, $id );
                    
                }
            }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        if( !empty( $forms ) ){
            
            return $forms;
            
        } else {
            
            return false;
        }
        
    }
    
}