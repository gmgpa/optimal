<?php

// This file handles single entries, but only exists for the sake of child theme forward compatibility.

remove_action ('genesis_loop', 'genesis_do_loop'); // Remove the standard loop
add_action( 'genesis_loop', 'gmg_form_loop', 10 , 2 ); // Add custom loop

function gmg_form_loop( $id ){
    
//    echo 'I am a form!';
    
    if ( have_posts() ){
        
        do_action( 'genesis_before_while' );
        while ( have_posts() ){
            
            the_post();

            do_action( 'genesis_before_entry' );
            
            printf( '<article %s>', genesis_attr( 'entry' ) );
            
//            do_action( 'genesis_entry_header' );
            printf( '<header %s>', genesis_attr( 'entry-header' ) );
            echo '<h1 class="entry-title" itemprop="headline">' . get_the_title( $id ) . '</h1>';
            echo '</header>';
            
            do_action( 'genesis_before_entry_content' );
            
            printf( '<div %s>', genesis_attr( 'entry-content' ) );
            
            do_action( 'genesis_entry_content' );
            
            $form = new GMG_Form( $id );
            echo $form->show_form( 'contact_form' , 'contact-form', 'gmg-form', '' );

            echo '</div>';
            
            do_action( 'genesis_after_entry_content' );

            echo '</article>';

            do_action( 'genesis_after_entry' );
        
        } // End of one post.
        
        do_action( 'genesis_after_endwhile' );
    
    }
}

// Run the Genesis loop.
genesis();