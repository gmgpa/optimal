jQuery(document).ready(function ($) {
    "use strict";
    
    var ids = ["#step_1",
               "#step_2",
               "#step_3",
               "#step_4",
               "#step_5"];
    
    ids.forEach( clickActive );
    
    function clickActive(item, index) {
        $(item).click( function () {
            $(item).removeClass('passive');
            $(item).addClass('active');
            
            //copy ids array
            var new_ids = ids.slice(0);
            
            //take out current id
            new_ids.splice(index, 1 );
            
            console.log( new_ids );
            
            new_ids.forEach( makePassive );
            
            
        });
    }
    
    function makePassive(item, index) {
        $(item).removeClass('active');
        $(item).addClass('passive');
        
    }
    
});