jQuery(document).ready(function ($) {
    "use strict";

    $("#order_form_button").click(function () {
        $(".shop_contact").slideToggle( "slow" );
    });

    if ($('#gmg_capture_form')){
        
//        var recaptcha = $("#CaptureRecaptcha").val();
//        
//        grecaptcha.ready(function() {
//                grecaptcha.execute( recaptcha,
//                                   { action: 'GMGCapture' }
//                                  ).then( function ( token ) {
//                    $("#CaptureRecaptcha").attr('value', token );
//                }
//                                        );
//            }
//                            );

        var form = $('#gmg_capture_form');

        form.on('submit', function (f) {
//                    alert( 'Selected!' );
            f.preventDefault();

            var img = $("#sign-up-wait");
            img.css("display", "inherit");

            var form_contact_Fname = $("#CaptureFName").val();
            var form_contact_Lname = $("#CaptureLName").val();
            var form_email = $("#CaptureEmail").val();
            var form_phone = $("#CapturePhone").val();
            var form_message = $("#CaptureMessage").val();

            var form_page_id = $("#CaptureID").val();
            
            var form_street = $("#CaptureStreetAddress").val();
            var form_town = $("#CaputureTown").val();
            var form_state = $("#CaptureState option:selected").val();
            var form_zip = $("#CaputureZip").val();

            var form_company_name = $("#CaptureCompanyName").val();
//            alert( 'Company name: ' + form_company_name );
//            console.log( 'Company: ' + form_company_name );

            var form_list = $("#CaptureFormList").val();
            var form_subject,
                form_addressee,
                form_id;

            if ($("#CaptureRecipients").val()) {

                form_addressee = $("#CaptureRecipients").val();
                form_subject = $("#CaptureRecipients option:selected").text();
                
            } else {
                
                form_addressee = $("#CaptureRecipient").val();
                
            }
            
            form_id = $("#CaptureFormID").val();
            
            var selectedinterests = [];
            var interests_selected;
            
            var form_customer_type = $("#CaptureCustomer:checked").val();
            
            //Get the options            
//            var form_response_option = $("#CaptureOptions:checked").val();
//            var form_response_time = $("#CaptureTimes:checked").val();
//            var form_response_day = $("#CaptureDays:checked").val();
            
            var form_response_option,
                form_response_time,
                form_response_day;            
            
            //Get Response Options
            if ( $("#CaptureOptions").is( "radio" ) ) {
                
                form_response_option = $("#CaptureOptions:checked").val();
                
            } else {
                
                var response_options = [];

                $("#CaptureOptions:checked").each(function () {
                    
                    response_options.push($(this).val());
                    
                });

                if ( response_options ) {
                    
                    form_response_option = response_options.join(',');
                }
            }
            
            
            //Get Response Times            
            if ( $("#CaptureTimes").is( "radio" ) ) {
                
                form_response_time = $("#CaptureTimes:checked").val();
                
            } else {
                
                var response_times = [];

                $("#CaptureTimes:checked").each(function () {
                    
                    response_times.push($(this).val());
                    
                });

                if ( response_times ) {
                    
                    form_response_time = response_times.join(',');
                }
            }
            
            
            //Get Response Days            
            if ( $("#CaptureDays").is( "radio" ) ) {
                
                form_response_day = $("#CaptureDays:checked").val();
                
            } else {
                
                var response_days = [];

                $("#CaptureDays:checked").each(function () {
                    
                    response_days.push($(this).val());
                    
                });

                if ( response_days ) {
                    
                    form_response_day = response_days.join(',');
                }
            }
            

            $("#CaptureInterests:checked").each(function () {
                selectedinterests.push($(this).val());
            });

            if (selectedinterests) {
                interests_selected = selectedinterests.join(',');
            }

            var form_other = $('#CaptureInterestsOther').val();
            
            if( $("#CaptureAppt0").val() ){
                
                var selecteddates = [],
                    dates_selected,
                    index = 0;
                
                while ( $("#CaptureAppt" + index ).val() ){
                    console.log( $("#CaptureAppt" + index).val() );
                    selecteddates.push( $("#CaptureAppt" + index).val() );
                    index++;
                }

                if (selecteddates) {
                    dates_selected = selecteddates.join(',');
                }
            }
            
            var selectedinputs = [];
            var inputs_selected;
            
            $('.CaptureInputs').each( function () {
                
//                console.log("Input: " + $(this).attr('placeholder') + ' : ' + $(this).val() );
                selectedinputs.push( $(this).attr('placeholder') + ' : ' + $(this).val() );
                
            });

            if ( selectedinputs ) {
                
                inputs_selected = selectedinputs.join(',');
                
            }
            
            var form_birthdate = $("#CaptureBirth").val();
            
//            var form_token = $("#CaptureRecaptcha").val();

            $.ajax({
                url: ajax_object.ajaxurl,
                type: 'post',
                data: {
                    action: 'gmg_capture_form_process',
                    CaptureFName: form_contact_Fname,
                    CaptureLName: form_contact_Lname,
                    CaptureCompanyName: form_company_name,
                    CaptureFormList: form_list,
                    CaptureEmail: form_email,
                    CapturePhone: form_phone,
                    CaptureFormID: form_id,
                    CaptureID: form_page_id,
                    CaptureStreet: form_street,
                    CaptureTown: form_town,
                    CaptureState: form_state,
                    CaptureZip: form_zip,
                    CaptureType : form_customer_type,
                    CaptureOptions : form_response_option,
                    CaptureTimes : form_response_time,
                    CaptureDays : form_response_day,
                    CaptureMessage: form_message,
                    CaptureRecipient: form_addressee,
                    CaptureSubject: form_subject,
//                    CaptureToken: form_token,
                    CaptureOther: form_other,
                    CaptureInterests: interests_selected,
                    CaptureInputs: inputs_selected,
                    CaptureDates: dates_selected,
                    CaptureBirth: form_birthdate
                },
                success: function (response) {
                    if (response['foo']) {
//                                            alert( 'Success' + response['foo']);
                        $('#CaptureResponse').html(response['foo']);
                        $('#CaptureResponse').css("visibility", "visible");
                        img.css("display", "none");
                        //                    form_contact_name.reset();
                        //                    formy[0].reset();
                        //                    window.location.href = "/thank-you/"

                        window.setTimeout(function () {
                            window.location.href = "/thank-you/"
                        }, 3000);

                    } else {
                        //                    alert( 'Problems' + response['errors']);
                        $('#webdev_form_response').html(response['errors']);
                        $('#webdev_form_response').css("visibility", "visible");
                    }

                },
                error: function (errorThrown) {
                    alert('Error' + errorThrown);
                }
            });
        });
    }


});
