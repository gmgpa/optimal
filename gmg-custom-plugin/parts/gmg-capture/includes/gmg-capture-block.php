<?php

add_action('acf/init', 'my_acf_init');
function my_acf_init() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// register a gmg-form block
		acf_register_block(array(
			'name'				=> 'gmg-form',
			'title'				=> __('GMG Form'),
			'description'		=> __('A block for showing GMG Forms.'),
			'render_callback'	=> 'gmg_form_acf_block_render_callback',
			'category'			=> 'formatting',
			'icon'				=> 'clipboard',
			'keywords'			=> array( 'GMG', 'form' ),
		));
	}
}

function gmg_form_acf_block_render_callback( $block ) {
	
	// convert name ("acf/gmg-form") into path friendly slug ("gmg-form")
	$slug = str_replace('acf/', '', $block['name']);
	
	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/block/content-{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/block/content-{$slug}.php") );
	}
}