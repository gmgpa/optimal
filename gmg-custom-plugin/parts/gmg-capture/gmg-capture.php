<?php

/**
 * This is the start of the Capture and Connect part of Contact 1:1.
 *
 * If this has been checked in the overall settings, then this program will not load.
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

//error_log( 'Inside Outer Capture!');

//$field = get_field('capture_&_connect', 'overall_settings');
////echo var_dump( $field );
//
//if( !empty( $field ) ){
//    if( $field[0] == 'yes' ){
        
//        error_log( 'Inside Capture!');
        
        $cap = plugin_dir_path( __FILE__ );
        
        require_once $cap . 'includes/gmg-capture-classes.php';
        require_once $cap . 'includes/gmg-capture-admin.php';
        require_once $cap . 'includes/gmg-capture-block.php';
        
        require_once $cap . 'includes/gmg-forms.php';
        
        add_action( 'wp_enqueue_scripts', 'gmg_capture_script_loader' );
        
//    }
//}


function gmg_capture_script_loader(){
    
//    error_log( 'Inside Capture Script Loader!');
    
    wp_register_script('recaptcha', 'https://www.google.com/recaptcha/api.js?render=' . get_field( 'recaptcha_site', 'overall_settings' ) );
    wp_enqueue_script('recaptcha');
    
    
    wp_register_script( 'clicker', plugin_dir_url( __FILE__ ) . 'includes/js/gmg-front-clicker.js', false , NULL, 'all' );
    wp_enqueue_script('clicker');
    
    wp_register_script('capture', plugin_dir_url( __FILE__ ) . 'includes/js/gmg-capture-forms.js', array(
        'jquery'), null, true );
    wp_enqueue_script('capture');
    wp_localize_script( 'capture', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    
    wp_register_style( 'form-style', plugin_dir_url( __FILE__ ) . 'includes/css/gmg-capture.css', false );
    wp_enqueue_style( 'form-style' );
    
}