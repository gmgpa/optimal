<?php

/**
 * GMG Brands
 *
 * @package           gmg-Brands
 * @author            Good Marketing Group
 * @license           GPL-2.0+
 * @link              http://goodgroupllc.com/
 * @copyright         Good Marketing Group
 *
 * @wordpress-plugin
 * Plugin Name:       GMG Brands
 * Plugin URI:        http://goodgroupllc.com/
 * Description:       Creates a Brand Custom Post Type and allows that to be connected to products.
 * Version:           1.0.0
 * Author:            Good Marketing Group
 * Author URI:        http://goodgroupllc.com/
 * Text Domain:       gmg-Brands
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

function gmg_start_brands_plugin(){
    
    $bro = plugin_dir_path( __FILE__ );
    
    require_once($bro . '/includes/gmg-brands-cpt.php');
    require_once($bro . '/includes/gmg-brands-export.php');
    require_once($bro . '/includes/gmg-class-brands.php');
}

function gmg_brand_style() {
//    error_log('Put up some style!');
    $plugin_url = plugin_dir_url( __FILE__ );
    
    wp_register_style( 'brand', $plugin_url . '/css/gmg-brand-style.css', false );
    wp_enqueue_style( 'brand' );
}

add_action('wp_enqueue_scripts', 'gmg_brand_style');

gmg_start_brands_plugin();
