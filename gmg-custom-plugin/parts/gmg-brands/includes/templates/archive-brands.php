<?php

// Remove standard post content output.
remove_action( 'genesis_post_content', 'genesis_do_post_content' );
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );
remove_action( 'genesis_entry_content', 'genesis_page_archive_entry_content', 10);
remove_action ('genesis_loop', 'genesis_do_loop'); // Remove the standard loop
add_action( 'genesis_loop', 'gmg_brands_archive_loop' ); // Add custom loop

//add_action( 'genesis_entry_content', 'genesis_page_archive_entry_content' );
//add_action( 'genesis_post_content', 'genesis_page_archive_entry_content' );

//remove_action( 'genesis_before_content_sidebar_wrap', 'studio_page_header', 10);
//add_action( 'genesis_before_content_sidebar_wrap', 'gmg_brands_header', 10);
//remove_action( 'studio_page_header', 'studio_page_excerpt', 20 );

function gmg_brands_header() {
    
    ?>
    
    <section id="page-header" class="brand-page-header" role="banner"><div class="wrap">
		<?php
    
    add_action( 'genesis_entry_header', 'genesis_do_post_title', 2 );
    
//    $cate = get_queried_object();
    
    genesis_markup( array(
			'open'    => '<h1 %s>',
			'close'   => '</h1>',
			'content' => 'Our Brands',
			'context' => 'archive-title',
		) );
    
    ?>
    
		</div></section>
		<?php

}

// Force full width content layout.
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

// Remove breadcrumbs.
//remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );


/**
 * This function outputs sitemap-esque columns displaying all pages,
 * categories, authors, monthly archives, and recent posts.
 *
 * @since 1.6
 */

function gmg_brands_archive_loop() {

//	$heading = ( genesis_a11y( 'headings' ) ? 'h2' : 'h4' );
//
//	genesis_sitemap( $heading );
    
//    echo '<div class="woocommerce woocommerce-page archive-description taxonomy-archive-description taxonomy-description">';
    echo '<div class="woocommerce woocommerce-page">';
    
//    echo '<div class="brands-search">';
//        echo '<label for="searchName">Search</label><input id="searchName" name="searchName" type="text" />';
//    echo '</div>';
    
    ?>

<form role="search" method="get" id="searchyform" class="searchyform" action="<?php echo get_site_url() . '/brands'; ?>">
<div class="brands-search">
<input type="text" value="" placeholder="Search" name="brand" id="brand" /> <input type="submit" id="searchsubmit" value="Search" />
</div>
</form>

<?php
    
//    echo '<div id="brand-return">';
    
    echo '<ul class="products">';
    
    if( isset( $_GET['brand'] ) ){
        
//        echo '<h2>' . $_GET['brand'] . '</h2>';
        
//        $args = array(
////            'post_type'              => 'brands',
//            'post_type'              => array( 'brands' ),
//            'posts_per_page'         => '-1',
////            's'                      => $_GET['brand'],
//            'title'                  => $_GET['brand'],
//            'orderby'                => 'post_title',
//            'order'                  => 'ASC',
////            'meta_query' => array(
////                array(
////                    'key' => 'post_title',
////                    'value' => $_GET['brand'],
////                    'compare' => '='
////                )
////            )
//        );
        
        $brands = new Brands();
        $brands->show_brands_by_keyword( $_GET['brand'] );
        
        
    } else {
        
        
        $brands = new Brands();
        $brands->show_all_brands();

        // WP_Query arguments
//        $args = array(
//            'post_type'              => array( 'brands' ),
//            'posts_per_page'         => '-1',
//            'orderby'                => 'title',
//            'order'                  => 'ASC',
//        );
        
    }
    
    echo '</ul>';
    
//    echo '</div>';
    
    echo '</div>';

}

genesis();
