<?php

//Force full width content layout.
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

/** Remove WooCommerce breadcrumbs */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

// This file handles single entries, but only exists for the sake of child theme forward compatibility.

remove_action ('genesis_loop', 'genesis_do_loop'); // Remove the standard loop
add_action( 'genesis_loop', 'gmg_brand_loop' ); // Add custom loop

//remove_action( 'genesis_before_content_sidebar_wrap', 'studio_page_header', 10);
//add_action( 'genesis_before_content_sidebar_wrap', 'gmg_brand_header', 10);
//remove_action( 'studio_page_header', 'studio_page_excerpt', 20 );

function gmg_brand_header() {
    
    ?>
    
    <section id="page-header" class="brand-page-header" role="banner"><div class="wrap">
		<?php
    
    add_action( 'genesis_entry_header', 'genesis_do_post_title', 2 );
    
//    $cate = get_queried_object();
    
    genesis_markup( array(
			'open'    => '<h1 %s>',
			'close'   => '</h1>',
			'content' => get_the_title(),
			'context' => 'archive-title',
		) );
    
    ?>
    
		</div></section>
		<?php
    
}


add_action( 'woocommerce_before_main_content', 'gmg_woocommerce_catalog_ordering', 30 );

function gmg_brand_loop() {
    
    echo '<div class="woocommerce woocommerce-page brand">';
    
    global $post;
    
    $brand_name = get_the_title();
    gmg_brand_section( $brand_name, get_the_id() );
    
    if( isset( $_GET['orderby'] ) ){
        
//        echo $_GET['orderby'];
        
        switch( $_GET['orderby'] ){
            case 'product_cat':
//                echo 'Cat!';               
                
                $args = array(
                    'post_type'              => array( 'product' ),
                    'posts_per_page'         => '-1',
                    'meta_query'             => array(
                        array(
                            'key'     => '_brand',
                            'value'   => $brand_name,
                            'compare' => '=',
                            'type'    => 'CHAR',
                        ),
                    ),
                );
                
                $cat_array = array();                
                
                // The Query
                $query = new WP_Query( $args );
                
                if ( $query->have_posts() ) {
                    while ( $query->have_posts() ) {
                        $query->the_post();
                        
                        $terms = get_the_terms( get_the_ID(), 'product_cat' );
//                        foreach ($terms as $term) {
                            if( !in_array( $terms[0]->term_id, $cat_array ) ){
                                array_push( $cat_array, $terms[0]->term_id );
                            }
//                        }
                    }
                }
                
                if( count( $cat_array) > 0 ){
                    
                    do_action( 'woocommerce_before_main_content' );
                
                    foreach( $cat_array as $cat){
                        $term = get_term_by( 'id', $cat, 'product_cat');
                        
                        $args = array(
                            'post_type'              => array( 'product' ),
                            'posts_per_page'         => '-1',
                            'order'                  => 'DESC',
                            'orderby'                => 'name',
                            'meta_query'             => array(
                                array(
                                    'key'     => '_brand',
                                    'value'   => $brand_name,
                                    'compare' => '=',
                                    'type'    => 'CHAR',
                                ),
                            ),
                            'tax_query'             => array(
                                array(
                                    'taxonomy'     => 'product_cat',
                                    'terms'   => $cat,
                                    'field' => 'id',
                                ),
                            ),
                        );
                        
                        // The Query
                        $query = new WP_Query( $args );  

                    //    echo var_dump( $query );


                        if ( $query->have_posts() ) {

                            do_action( 'woocommerce_before_shop_loop' );
                            woocommerce_product_loop_start();

                            while ( $query->have_posts() ) {

                                $query->the_post();
                                /**
                                 * Hook: woocommerce_shop_loop.
                                 *
                                 * @hooked WC_Structured_Data::generate_product_data() - 10
                                 */
                                do_action( 'woocommerce_shop_loop' );

                                wc_get_template_part( 'content', 'product' );
                            }
                            woocommerce_product_loop_end();
                            /**
                             * Hook: woocommerce_after_shop_loop.
                             *
                             * @hooked woocommerce_pagination - 10
                             */
                            do_action( 'woocommerce_after_shop_loop' );
                        } else {
                            /**
                             * Hook: woocommerce_no_products_found.
                             *
                             * @hooked wc_no_products_found - 10
                             */
                            do_action( 'woocommerce_no_products_found' );
                        }

                    }
                    
                    /**
                     * Hook: woocommerce_after_main_content.
                     *
                     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                     */
                    do_action( 'woocommerce_after_main_content' );

                    echo '</div>';                    
                    
                }        
                break;
                
            case 'date':
//                echo 'Date!';
                $args = array(
                    'post_type'              => array( 'product' ),
                    'posts_per_page'         => '-1',
                    'order'                  => 'DESC',
                    'orderby'                => 'date',
                    'meta_query'             => array(
                        array(
                            'key'     => '_brand',
                            'value'   => $brand_name,
                            'compare' => '=',
                            'type'    => 'CHAR',
                        ),
                    ),
                );
                
                do_action( 'woocommerce_before_main_content' );

                $theID = get_the_ID();                
//                gmg_brand_section( $brand_name, $theID );

                    // The Query
                $query = new WP_Query( $args );  

            //    echo var_dump( $query );


                if ( $query->have_posts() ) {

                    do_action( 'woocommerce_before_shop_loop' );
                    woocommerce_product_loop_start();

                    while ( $query->have_posts() ) {

                        $query->the_post();
                        /**
                         * Hook: woocommerce_shop_loop.
                         *
                         * @hooked WC_Structured_Data::generate_product_data() - 10
                         */
                        do_action( 'woocommerce_shop_loop' );

                        wc_get_template_part( 'content', 'product' );
                    }
                    woocommerce_product_loop_end();
                    /**
                     * Hook: woocommerce_after_shop_loop.
                     *
                     * @hooked woocommerce_pagination - 10
                     */
                    do_action( 'woocommerce_after_shop_loop' );
                } else {
                    /**
                     * Hook: woocommerce_no_products_found.
                     *
                     * @hooked wc_no_products_found - 10
                     */
                    do_action( 'woocommerce_no_products_found' );
                }
                /**
                 * Hook: woocommerce_after_main_content.
                 *
                 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                 */
                do_action( 'woocommerce_after_main_content' );

                echo '</div>';
                break;
            case 'menu_order':
                
                $args = array(
                    'post_type'              => array( 'product' ),
                    'posts_per_page'         => '-1',
                    'order'                  => 'DESC',
                    'orderby'                => 'menu_order',
                    'meta_query'             => array(
                        array(
                            'key'     => '_brand',
                            'value'   => $brand_name,
                            'compare' => '=',
                            'type'    => 'CHAR',
                        ),
                    ),
                );
                
                do_action( 'woocommerce_before_main_content' );
                
                $theID = get_the_ID();
//                gmg_brand_section( $brand_name, $theID );              

                    // The Query
                $query = new WP_Query( $args );  

            //    echo var_dump( $query );


                if ( $query->have_posts() ) {

                    do_action( 'woocommerce_before_shop_loop' );
                    woocommerce_product_loop_start();

                    while ( $query->have_posts() ) {

                        $query->the_post();
                        /**
                         * Hook: woocommerce_shop_loop.
                         *
                         * @hooked WC_Structured_Data::generate_product_data() - 10
                         */
                        do_action( 'woocommerce_shop_loop' );

                        wc_get_template_part( 'content', 'product' );
                    }
                    woocommerce_product_loop_end();
                    /**
                     * Hook: woocommerce_after_shop_loop.
                     *
                     * @hooked woocommerce_pagination - 10
                     */
                    do_action( 'woocommerce_after_shop_loop' );
                } else {
                    /**
                     * Hook: woocommerce_no_products_found.
                     *
                     * @hooked wc_no_products_found - 10
                     */
                    do_action( 'woocommerce_no_products_found' );
                }
                /**
                 * Hook: woocommerce_after_main_content.
                 *
                 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                 */
                do_action( 'woocommerce_after_main_content' );

                echo '</div>';
                
                break;
        }
                
    } else {

        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'product' ),
            'posts_per_page'         => '-1',
            'meta_query'             => array(
                array(
                    'key'     => '_brand',
                    'value'   => $brand_name,
                    'compare' => '=',
                    'type'    => 'CHAR',
                ),
            ),
        );
        
        do_action( 'woocommerce_before_main_content' );
        
        $theID = get_the_ID();
//        gmg_brand_section( $brand_name, $theID );

            // The Query
        $query = new WP_Query( $args );  

    //    echo var_dump( $query );


        if ( $query->have_posts() ) {

            do_action( 'woocommerce_before_shop_loop' );
            woocommerce_product_loop_start();

            while ( $query->have_posts() ) {

                $query->the_post();
                /**
                 * Hook: woocommerce_shop_loop.
                 *
                 * @hooked WC_Structured_Data::generate_product_data() - 10
                 */
                do_action( 'woocommerce_shop_loop' );

                wc_get_template_part( 'content', 'product' );
            }
            woocommerce_product_loop_end();
            /**
             * Hook: woocommerce_after_shop_loop.
             *
             * @hooked woocommerce_pagination - 10
             */
            do_action( 'woocommerce_after_shop_loop' );
        } else {
            /**
             * Hook: woocommerce_no_products_found.
             *
             * @hooked wc_no_products_found - 10
             */
            do_action( 'woocommerce_no_products_found' );
        }
        /**
         * Hook: woocommerce_after_main_content.
         *
         * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
         */
        do_action( 'woocommerce_after_main_content' );

        echo '</div>';

    }
    
}

// Run the Genesis loop.
genesis();

function gmg_brand_section( $name, $id ){
    
    echo '<div class="brand-header">';
    echo '<h2 class="woocommerce-products-header__title page-title">' . $name . '</h2>';
    $image_url = get_the_post_thumbnail_url( $id, 'thumbnail');
    echo '<img src="' . $image_url . '" class="brand-image" alt="' . $name .'" >';
    
    if( class_exists( 'Promos') ){
        error_log( 'Promos' );
        $promos = new Promos();
        if( $promos->get_promo_by_brand( $name ) != false ){
            
            $promo_array = $promos->get_promo_by_brand( $name );
            echo '<h2 class="brand-section-title">Promotions</h2>';
            foreach( $promo_array as $each_promo ){
                $promo = new Promo( $each_promo );
                $promo->show_promo( 'grade-2' );
            }
        }
    }
    echo '<h2 class="brand-section-title">Products</h2>';
    echo '</div>';
    
}

function gmg_brand_cat_section( $name ){
    
    echo '<div class="brand-header">';
    echo '<h2 class="woocommerce-products-header__title page-title">' . $name . '</h2>';
    echo '</div>';
    
}

function gmg_woocommerce_catalog_ordering() {
    global $wp_query;

    $orderby = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) ); 
    $show_default_orderby = 'menu_order' === apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) ); 
    $catalog_orderby_options = apply_filters( 'woocommerce_catalog_orderby', array( 
        'menu_order' => __( 'Default sorting', 'woocommerce' ),  
        'date' => __( 'Sort by newness', 'woocommerce' ), ) 
                                            );

    wc_get_template( 'loop/orderby.php', array( 'catalog_orderby_options' => $catalog_orderby_options, 'orderby' => $orderby, 'show_default_orderby' => $show_default_orderby ) ); 
} 