<?php

/**
 * GMG Brands
 *
 * @package           gmg-Brands
 * @author            Good Marketing Group
 * @license           GPL-2.0+
 * @link              http://goodgroupllc.com/
 * @copyright         Good Marketing Group
 *
 * @wordpress-plugin
 * Plugin Name:       GMG Brands
 * Plugin URI:        http://goodgroupllc.com/
 * Description:       Creates a Brand Custom Post Type and allows that to be connected to products.
 * Version:           1.0.0
 * Author:            Good Marketing Group
 * Author URI:        http://goodgroupllc.com/
 * Text Domain:       gmg-Brands
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

// Register Custom Post Type
function gmg_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Brands', 'Post Type General Name', 'wellness_pro' ),
		'singular_name'         => _x( 'Brand', 'Post Type Singular Name', 'wellness_pro' ),
		'menu_name'             => __( 'Brands', 'wellness_pro' ),
		'name_admin_bar'        => __( 'Brands', 'wellness_pro' ),
		'archives'              => __( 'Brand Archives', 'wellness_pro' ),
		'attributes'            => __( 'Brand Attributes', 'wellness_pro' ),
		'parent_item_colon'     => __( 'Parent Brand:', 'wellness_pro' ),
		'all_items'             => __( 'All Brands', 'wellness_pro' ),
		'add_new_item'          => __( 'Add New Brand', 'wellness_pro' ),
		'add_new'               => __( 'Add New', 'wellness_pro' ),
		'new_item'              => __( 'New Brand', 'wellness_pro' ),
		'edit_item'             => __( 'Edit Brand', 'wellness_pro' ),
		'update_item'           => __( 'Update Brand', 'wellness_pro' ),
		'view_item'             => __( 'View Brand', 'wellness_pro' ),
		'view_items'            => __( 'View Brands', 'wellness_pro' ),
		'search_items'          => __( 'Search Brand', 'wellness_pro' ),
		'not_found'             => __( 'Not found', 'wellness_pro' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wellness_pro' ),
		'featured_image'        => __( 'Featured Image', 'wellness_pro' ),
		'set_featured_image'    => __( 'Set featured image', 'wellness_pro' ),
		'remove_featured_image' => __( 'Remove featured image', 'wellness_pro' ),
		'use_featured_image'    => __( 'Use as featured image', 'wellness_pro' ),
		'insert_into_item'      => __( 'Insert into Brand', 'wellness_pro' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Brand', 'wellness_pro' ),
		'items_list'            => __( 'Brands list', 'wellness_pro' ),
		'items_list_navigation' => __( 'Brands list navigation', 'wellness_pro' ),
		'filter_items_list'     => __( 'Filter brands list', 'wellness_pro' ),
	);
    
	$args = array(
		'label'                 => __( 'Brand', 'wellness_pro' ),
		'description'           => __( 'Custom Post Type for Manufacturers', 'wellness_pro' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', ),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => 'gmg-custom-plugin',
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-awards',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'Brands', $args );

}
add_action( 'init', 'gmg_custom_post_type', 0 );

//[brands cat="wood-fireplace-brands"]

function gmg_cat_brands( $atts, $content = null ){
    
    extract(
        shortcode_atts(
            array ( 'cat' => null, ),
            $atts
        )
    );
    
//    error_log( 'The passed variable is ' . $cat );
    
    // WP_Query arguments
    $args = array(
        'post_type'              => array( 'Brands' ),
        'post_status'            => array( 'publish' ),
        'tax_query'              => array(
            array(
                'taxonomy'         => 'category',
                'terms'            => $cat,
                'field'            => 'slug',
            ),
        ),
    );

    // The Query
    $query = new WP_Query( $args );

    // The Loop
    if ( $query->have_posts() ) {
        
        $return_html = array();
        array_push( $return_html , '<ul class="brands">' );
        
        while ( $query->have_posts() ) {
            $query->the_post();            
            
            array_push( $return_html , '<li class="brand">');
            array_push( $return_html , '<a href="' . get_post_meta( get_the_ID() , '_brand_url', true ) . '" target="_blank">');
            array_push( $return_html , '<div id="brand-' . get_the_ID() . '" class="brand-card">');
            array_push( $return_html , '<img src="' . get_the_post_thumbnail_url( get_the_ID(), 'medium') . '" class="brand-image" >');
            
            array_push( $return_html , '</div>');
            array_push( $return_html , '</a>');
            array_push( $return_html , '</li>');
            
            // do something
        }
    } else {
        // no posts found
    }

    // Restore original Post Data
    wp_reset_postdata();
    
    array_push( $return_html , '</ul>' );
    
    return implode( $return_html );
}

add_shortcode('brands', 'gmg_cat_brands' );

/* Fire our meta box setup function on the brands editor screen. */
add_action( 'init', 'gmg_brands_meta_boxes_setup' );

function gmg_brands_meta_boxes_setup(){
    
//    error_log( 'Brands setup!' );

	/* Add meta boxes on the 'add_meta_boxes' hook. */
	add_action( 'add_meta_boxes', 'gmg_brands_add_url_meta_boxes' );
	/* Save post meta on the 'save_post' hook. */
	add_action( 'save_post', 'gmg_save_brand_url_meta', 10, 2);
}

/* Create brands Custom Meta Boxes */
function gmg_brands_add_url_meta_boxes(){

	add_meta_box(
		'brand-url',      // Unique ID
		esc_html__( 'Brand Url', 'text-domain' ),    // Title
		'gmg_brand_url_meta_box',   // Callback function
		'brands',         // Admin page (or post type)
		'advanced',         // Context
		'default'         // Priority
	);
}

/* Display the Brand url meta box. */
function gmg_brand_url_meta_box( $object, $box ) { ?>

    <?php wp_nonce_field( basename( __FILE__ ), 'gmg_brand_url_nonce' ); ?>

    <p>
        <label for="brand-url"><?php _e( "Brand Url:", 'Outreach Pro' ); ?></label>
        <br />
        <input class="widefat" type="text" name="brand-url" id="brand-url" value="<?php echo esc_attr( get_post_meta( $object->ID, '_brand_url', true ) ); ?>" size="30" />
    </p>
    <?php }


/* Save the meta box's post metadata. */
function gmg_save_brand_url_meta( $post_id, $post ) {
    
//    error_log( 'Save Brand Url with post type ' . get_post_type( $post_id ) );
    
    if( get_post_type( $post_id ) == 'brands'){
    
//        error_log( 'Save Brand Url with Brand ' . $post->post_name );

        /* Verify the nonce before proceeding. */
//        if ( !isset( $_POST['gmg_brand_url_nonce'] ) || !wp_verify_nonce( $_POST['gmg_brand_url_nonce'], basename( __FILE__ ) ) )
//            return $post_id;

        /* Get the post type object. */
//        $post_type = get_post_type_object( $post->post_type );
//
//        /* Check if the current user has permission to edit the post. */
//        if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
//            return $post_id;

        /* Get the posted data and sanitize it for use as an HTML class. */
        $new_meta_value = ( isset( $_POST['brand-url'] ) ? sanitize_text_field( $_POST['brand-url'] ) : '' );

        /* Get the meta key. */
        $meta_key = '_brand_url';
        
        update_post_meta( $post_id, $meta_key, $new_meta_value );

        /* Get the meta value of the custom field key. */
        $meta_value = get_post_meta( $post_id, $meta_key, true );

//        /* If a new meta value was added and there was no previous value, add it. */
//        if ( $new_meta_value && '' == $meta_value )
//            add_post_meta( $post_id, $meta_key, $new_meta_value, true );
//
//        /* If the new meta value does not match the old value, update it. */
//        elseif ( $new_meta_value && $new_meta_value != $meta_value )
//            update_post_meta( $post_id, $meta_key, $new_meta_value );
//
//        /* If there is no new meta value but an old value exists, delete it. */
//        elseif ( '' == $new_meta_value && $meta_value )
//            delete_post_meta( $post_id, $meta_key, $meta_value );
        
    }
}