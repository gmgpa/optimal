<?php

class Brand{
    
    private $id;
    
    public function __construct( $id ) {
        $this->id = $id;
    }
    
    
    //Get Promo
    
    public function get_brand_id(){
        return $this->id;
    }
    
    public function get_brand_name(){
        return get_the_title( $this->id );
    }
    
    public function get_brand_url(){
        return get_post_meta( $this->id, '_brand_url', true );
    }
    
    //Show
    
    public function show_brand(){
        
        echo '<li class="product brands">';
        $title = get_the_title( $this->id ); 
        echo '<a href="' . get_post_permalink( $this->id ) . '" alt="' . $title . '">';
        echo '<div id="brand-' . $this->id . '" class="brand-card">';
        $image_url = get_the_post_thumbnail_url( $this->id, 'medium');
        echo '<img src="' . $image_url . '" class="brand-image" alt="' . $title .'" >';
        echo '</div>';
        echo '</a>';
        echo '</li>';    
        
    }
    
}

class Brands {
    
    public function __construct() { }
    
    
    public function get_all_brands(){
         
        $brands = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'brands' ),
            'posts_per_page'         => '-1',
            'orderby'                => 'title',
            'order'                  => 'ASC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                array_push( $brands, get_the_ID() );
            }
                
        } else {
            // no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        if( count( $brands ) > 0 ){
            
            return $brands;
            
        } else{
            
            return false;
        }
        
    }
    
    public function get_products_brands ( $cat_id ){
        
//        error_log( 'Product Brands with Category ' . $cat_id );
        
        $brands_array = array();
        
        $args = array(
	       'post_type' => 'product',
	       'orderby'   => 'title',
	       'tax_query' => array(
               array(
                    'taxonomy'  => 'product_cat',
                    'field'     => 'id',
                    'terms'     => $cat_id
                ),
           ),
            'posts_per_page' => -1
        );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
//                error_log( get_the_title() . ' has the Brand of ' . get_post_meta( get_the_id(), '_brand' )[0] );
                
                if( null !== get_post_meta( get_the_id(), '_brand' ) ){
                    
//                    error_log( 'Go get the Brand!');
                    
                    $brand = get_post_meta( get_the_id(), '_brand' );
                    
//                    echo var_dump( $brand );
                
                    if( !in_array( $brand[0], $brands_array ) ){
                        
//                        error_log( 'Got the Brand with ' . $brand[0] );

                        array_push( $brands_array, $brand[0] );
                    }                    
                    
                }            
            }
        }  else {
    	// no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        return $brands_array;
    }
    
    public function get_brand_by_name( $name ){        
        
        $args = array(
            'post_type'              => array( 'brands' ),
            'posts_per_page'         => '-1',
            'title'                  => $name,
            'orderby'                => 'title',
            'order'                  => 'ASC',
        );
        
                // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                array_push( $brands, get_the_ID() );
            }
                
        } else {
            // no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
        if( count( $brands ) > 0 ){
            
            return $brands;
            
        } else{
            
            return false;
        }
        
    }
    
    
    //Show
    public function show_brands_by_keyword( $keyword ){
        
        $args = array(
            'post_type'              => array( 'brands' ),
            'post_status'               => 'publish',
            'posts_per_page'         => '-1',
        );
        
        $showed = false;
        
        // The Query
        $query = new WP_Query( $args );

        // The Loop
        if ( $query->have_posts() ) {
            echo '<p>Showing Search Results for "' . $keyword . '":</p>';
            while ( $query->have_posts() ) {

    //            echo 'The post!';
                $query->the_post();
                
                $title = get_the_title();
                if( strstr( $title , $keyword ) ){
                    
                    $showed = true;
                    
                    $brand = new Brand( get_the_id() );
                    $brand->show_brand();
                    
                }
                
            }

        }
        
        if( !$showed ) {
            echo '<h2>No Brands found with keyword ' . $_GET['brand'] . '</h2>';
        }
    }
    
    public function show_all_brands(){
         
        $brands = array();
        
        // WP_Query arguments
        $args = array(
            'post_type'              => array( 'brands' ),
            'posts_per_page'         => '-1',
            'post_status'               => 'publish',
            'orderby'                => 'title',
            'order'                  => 'ASC' );
        
        // The Query
        $query = new WP_Query( $args );
        
        // The Loop
        if ( $query->have_posts() ) {
            
            while ( $query->have_posts() ) {
                $query->the_post();
                
                $brand = new Brand( get_the_id() );
                $brand->show_brand();
            }
                
        } else {
            // no posts found
        }
    
        // Restore original Post Data
        wp_reset_postdata();
        
    }
    
    
    
//    public function get_all_brands_by_category( $category_name ){
//        
//    }
    
}
