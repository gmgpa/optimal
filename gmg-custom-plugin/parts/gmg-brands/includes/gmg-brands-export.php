<?php

add_action( 'restrict_manage_posts', 'gmg_brands_add_export_button' );

function gmg_brands_add_export_button() {
    
//    error_log( 'Inside Export brands');
    
    $screen = get_current_screen();
    
    if (isset($screen->parent_file) && ('edit.php?post_type=brands' == $screen->parent_file)) {
            
//            error_log( 'The screen parent file is ' . $screen->parent_file );
            ?>
    <input type="submit" name="export_brands_posts" id="export_brands_posts" class="button button-primary" value="Export All brands">
    <script type="text/javascript">
        jQuery(function($) {
            $('#export_brands_posts').insertAfter('#post-query-submit');
        });

    </script>

    <?php
    }
}

//add_action('wp_ajax_gmg_export_all_brands', 'gmg_export_all_brands');
////add_action('wp_ajax_nopriv_gmg_contact_mail', 'gmg_contact_mail');
add_action( 'init', 'func_gmg_brands_export_all_brands' );
function func_gmg_brands_export_all_brands() {
    
	if(isset($_GET['export_brands_posts'])) {
        
		$arg = array(
				'post_type' => 'brands',
				'post_status' => 'publish',
				'posts_per_page' => -1,
			);

		global $post;
		$arr_post = get_posts($arg);
		if ($arr_post) {

			header('Content-type: text/csv');
			header('Content-Disposition: attachment; filename="brands.csv"');
			header('Pragma: no-cache');
			header('Expires: 0');

			$file = fopen('php://output', 'w');

			fputcsv($file, array('Name', 'Categories', 'URL'));

			foreach ($arr_post as $post) {
                                
				setup_postdata($post);
                $cats = get_the_category();
                $cat_name = array();
                
                if( is_array( $cats) ){                    
                    if( count( $cats ) > 1 ){                        
                        foreach( $cats as $cat ){
                            array_push( $cat_name , html_entity_decode( $cat->name ) );
                        }                        
                    } else {
                        array_push( $cat_name , html_entity_decode( $cats[0]->name ) );
                    }
                }
                
				fputcsv( $file, 
                        array(
                            html_entity_decode( get_the_title() ),
                            implode( ', ' , $cat_name ),
                            get_post_meta( get_the_ID() , '_brand_url', true )
                        )
                       );
			}

			exit();
		}
	}
}
