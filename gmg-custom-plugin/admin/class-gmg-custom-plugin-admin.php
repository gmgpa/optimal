<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://goodmarketinggroup.com/
 * @since      1.0.0
 *
 * @package    Gmg_Custom_Plugin
 * @subpackage Gmg_Custom_Plugin/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gmg_Custom_Plugin
 * @subpackage Gmg_Custom_Plugin/admin
 * @author     Good Group LLC <info@goodmarketinggroup.com>
 */
class Gmg_Custom_Plugin_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gmg_Custom_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gmg_Custom_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gmg-custom-plugin-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gmg_Custom_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gmg_Custom_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gmg-custom-plugin-admin.js', array( 'jquery' ), $this->version, false );

	}
    
    /**
     * Registers and Defines the necessary fields we need.
     *
     */
    public function register_gmg_contact_121_menu_pages(){
        
//        error_log( 'Add menu pages!' );
        
        add_menu_page(
            __( 'GMG Contact 1:1', $this->plugin_name ),              // page title
            __( 'Contact 1:1', $this->plugin_name ),                  // menu title
            'publish_posts',                                               // capability
            $this->plugin_name,                                             // menu_slug
            array( $this, 'display_gmg_custom_121_plugin_page' ),       // callable function
            'dashicons-chart-area',
            4       // Menu Position
        );
        
        acf_add_options_sub_page(array(
            'parent_slug' 	=> $this->plugin_name,
            'page_title' 	=> 'GMG Contact 1:1 Overall Settings ',
            'menu_title' 	=> 'Overall Settings ',
            'post_id'            => 'overall_settings',
            'capability' 	=> 'manage_options'
        ));
        
    }
    
    public function display_gmg_custom_121_plugin_page(){
        
        echo '<h2>GMG Custom Plugin</h2>';
        echo '<p>Copyright ' . date("Y") . ' Good Marketing Group</p>';
        
    }

}
